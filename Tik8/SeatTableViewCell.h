//
//  SeatTableViewCell.h
//  Tik8
//
//  Created by Pooya on 5/4/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeatTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet YekanLabel *lbPrice;
@property (strong, nonatomic) IBOutlet YekanLabel *lbVenue;
@property (strong, nonatomic) IBOutlet YekanLabel *lbRow;
@property (strong, nonatomic) IBOutlet YekanLabel *lbSeatNi;

@end
