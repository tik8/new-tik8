//
//  SideViewController.m
//  Tik8
//
//  Created by Pooya on 2/4/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "SideViewController.h"
#import "MenuTableViewCell.h"
#import "MenuUserTableViewCell.h"
#import "JASidePanelController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "SubcribeViewController.h"

@interface SideViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadMenu) name:@"LOAD_MENU" object:nil];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)reloadMenu{
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0)return 1;
    else if([Tik8DataProvider sharedTik8DataProvider].userInfo){
        return 8;
    }else{
        return 6;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0 )return 100;
    else return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        MenuUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell" forIndexPath:indexPath];
        if([Tik8DataProvider sharedTik8DataProvider].userInfo){
            cell.lbName.text = [NSString stringWithFormat:@"%@ %@",[Tik8DataProvider sharedTik8DataProvider].userInfo.firstname,[Tik8DataProvider sharedTik8DataProvider].userInfo.lastname];
            cell.lbDetail.text = [NSString stringWithFormat:@"میزان اعتبار:‌ %@ تومان",[Tik8DataProvider sharedTik8DataProvider].userInfo.cashBalance];
        }else{
            cell.lbName.text = @"کاربر مهمان";
            cell.lbDetail.text = @"ورود / ثبت‌نام";
        }
        [cell.imgUser sd_setImageWithURL:[NSURL URLWithString:[Tik8DataProvider sharedTik8DataProvider].userInfo.profile_image] placeholderImage:[UIImage imageNamed:@"place-holder_user"]];
        
        return cell;
    }else{
        MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell" forIndexPath:indexPath];
        if([Tik8DataProvider sharedTik8DataProvider].userInfo){
            switch (indexPath.row) {
                case 0:
                    cell.lbTitle.text = @"صفحه‌ی اصلی";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-home"];
                    
                    break;
                case 1:
                    cell.lbTitle.text = @"بلیط‌های من";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-myTik8"];
                    
                    break;
                case 2:
                    cell.lbTitle.text = @"رویداد‌های روی نقشه";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-map"];
                    
                    break;
                case 3:
                    cell.lbTitle.text = @"رویداد‌های نزدیک من";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-nearby"];
                    
                    break;
                case 4:
                    cell.lbTitle.text = @"اشتراک در خبرنامه";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-subscribe"];

                    break;
                case 5:
                    cell.lbTitle.text = @"درباره اپلیکیشن";
                    cell.imgIcon.image = [UIImage imageNamed:@"Infoicon"];
                    
                    break;
                case 6:
                    cell.lbTitle.text = @"ارتباط با ما";
                    cell.imgIcon.image = [UIImage imageNamed:@"IconFeedback"];
                    
                    break;
                case 7:
                    cell.lbTitle.text = @"خروج";
                    cell.imgIcon.image = [UIImage imageNamed:@"iconExit"];
                    
                    break;
                default:
                    break;
            }
        }else{
            switch (indexPath.row) {
                case 0:
                    cell.lbTitle.text = @"صفحه‌ی اصلی";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-home"];
                    break;
                case 1:
                    cell.lbTitle.text = @"رویداد‌های روی نقشه";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-map"];
                    break;
                case 2:
                    cell.lbTitle.text = @"رویداد‌های نزدیک من";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-nearby"];
                    break;
                case 3:
                    cell.lbTitle.text = @"اشتراک در خبرنامه";
                    cell.imgIcon.image = [UIImage imageNamed:@"icon-menu-subscribe"];
                    break;
                case 4:
                    cell.lbTitle.text = @"درباره اپلیکیشن";
                    cell.imgIcon.image = [UIImage imageNamed:@"Infoicon"];
                    break;
                case 5:
                    cell.lbTitle.text = @"ارتباط با ما";
                    cell.imgIcon.image = [UIImage imageNamed:@"IconFeedback"];
                    
                    break;
                default:
                    break;
            }
        }

        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        [self userPressed];
    }else{
        self.sidePanelController.allowRightSwipe = true;
        if([Tik8DataProvider sharedTik8DataProvider].userInfo){
            switch (indexPath.row) {
                case 0:
                    [self homePressed];
                    break;
                case 1:
                    [self myTicketPressed];
                    
                    break;
                case 2:
                    [self eventMapPressed];
                    
                    break;
                case 3:
                    [self nearbyPressed];
                    break;
                case 4:
                    [self subscribePressed];
                    break;
                case 5:
                    [self aboutPressed];
                    break;
                case 6:
                    [self feedBackPressed];
                    break;
                case 7:
                    [self signoutPressed];
                    break;
                    
                default:
                    break;
            }
        }else{
            switch (indexPath.row) {
                case 0:
                    [self homePressed];
                    
                    break;
                case 1:
                    [self eventMapPressed];
                    break;
                case 2:
                    [self nearbyPressed];
                    break;
                case 3:
                    [self subscribePressed];
                    break;
                case 4:
                    [self aboutPressed];
                    break;
                case 5:
                    [self feedBackPressed];
                    break;
                default:
                    break;
            }
        }
        
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 1){
        
        [self homePressed];
        [[Tik8DataProvider sharedTik8DataProvider]userSignOut];
        [self.tableView reloadData];
    }
}

-(void)userPressed{
    if ([Tik8DataProvider sharedTik8DataProvider].userInfo) {
        self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileVC"];
    }else{
        [self presentViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"loginVC"] animated:YES completion:nil];
    }
}

-(void)signoutPressed{
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"آیا مایل به خروج از اکانت خود هستید؟" message:nil delegate:self cancelButtonTitle:@"خیر" otherButtonTitles:@"بلی", nil];
    [alertView show];
}

-(void)homePressed{
    self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeNav"];
    
}

-(void)eventMapPressed{
    self.sidePanelController.allowRightSwipe = false;
    self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"EventMapVC"];
    
}

-(void)nearbyPressed{
    self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"nearVC"];

}

-(void)myTicketPressed{
    self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"myTicketsNav"];
    
}

-(void)subscribePressed{
    SubcribeViewController *subsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"subscribeVC"];
    [self presentViewController:[[UINavigationController alloc]initWithRootViewController:subsVC] animated:YES completion:nil];
    dispatch_async(dispatch_get_main_queue(), ^{});
}

-(void)aboutPressed{
    self.sidePanelController.centerPanel = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutVC"];

}

-(void)feedBackPressed{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        // We must always check whether the current device is configured for sending emails
        if ([mailClass canSendMail])
        {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            NSString *mailSubject = @"ارتباط با تیکت";
            NSString *mailBody = @"";
            NSString *mailReciver = @"feedback@tik8.com";
            picker.mailComposeDelegate = self;
            NSArray *mailArray = [[NSArray alloc]initWithObjects:mailReciver, nil];
            [picker setToRecipients:mailArray];
            [picker setSubject:mailSubject];
            [picker setMessageBody:mailBody isHTML:NO];
            [self presentViewController:picker animated:YES completion:^{

            }];
        }
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
