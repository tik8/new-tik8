//
//  ArtistLocationDetailViewController.h
//  Tik8
//
//  Created by Pooya on 1/31/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistLocationDetailViewController : UIViewController

@property (nonatomic,strong) NSString *eventTypeId;
@property (nonatomic,strong) JSLocation *locationInfo;
@property (nonatomic,strong) JSCelebrity *celebInfo;

@end
