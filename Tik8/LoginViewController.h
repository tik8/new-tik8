//
//  LoginViewController.h
//  Netbarg
//
//  Created by Pooya on 1/24/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *textFieldEmailAddress;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPassword;
- (IBAction)loginPressed;
- (IBAction)signupPressed;
- (IBAction)forgotPassPressed;
@property (strong, nonatomic) IBOutlet YekanButton *butLogin;
@property (strong, nonatomic) IBOutlet YekanButton *butSignUp;
@property (strong, nonatomic) IBOutlet YekanButton *butForgotpass;

@end
