//
//  EventListViewController.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListViewController : UIViewController

@property (nonatomic,strong) NSString *eventTypeId;

@end
