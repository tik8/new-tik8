//
//  MyTicketTableViewCell.h
//  Tik8
//
//  Created by Pooya on 2/25/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTicketTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet YekanLabel *lbName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbLocation;
@property (strong, nonatomic) IBOutlet YekanLabel *lbCount;
@property (strong, nonatomic) IBOutlet UIImageView *imgTail;

@property (strong, nonatomic) IBOutlet YekanLabel *lbDayInWeek;
@property (strong, nonatomic) IBOutlet YekanLabel *lbDay;
@property (strong, nonatomic) IBOutlet YekanLabel *lbMonth;
@property (strong, nonatomic) IBOutlet YekanLabel *lbHour;

@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@end
