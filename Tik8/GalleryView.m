//
//  GalleryView.m
//  Tik8
//
//  Created by Pooya on 5/25/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "GalleryView.h"

@implementation GalleryView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"GalleryView" owner:self options:nil];
        self = [nib objectAtIndex:0];
        self.frame = frame;
        
    }
    return self;
}

@end
