//
//  EventLocation2TableViewCell.h
//  Tik8
//
//  Created by Sina KH on 7/19/16.
//  Copyright © 2016 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventLocation2TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet YekanLabel *lblTitle;
@property (weak, nonatomic) IBOutlet YekanLabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblViewIntervals;

@end
