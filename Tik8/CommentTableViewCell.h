//
//  CommentTableViewCell.h
//  Tik8
//
//  Created by Pooya on 2/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet YekanLabel *lbUserName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbUserComment;
@property (strong, nonatomic) IBOutlet UIView *viewUser;
@property (strong, nonatomic) IBOutlet UIView *viewSeprator;

@property (strong, nonatomic) IBOutlet UIImageView *imgAnswer;
@property (strong, nonatomic) IBOutlet YekanLabel *lbAnswerName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbAnswerComment;
@property (strong, nonatomic) IBOutlet UIView *viewAnswer;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnsQuestionToBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cnsAnswerToBottom;
@end
