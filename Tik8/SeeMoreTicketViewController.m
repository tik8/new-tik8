//
//  SeeMoreTicketViewController.m
//  Tik8
//
//  Created by Pooya on 2/18/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "SeeMoreTicketViewController.h"
#import "EventTimeCollectionViewCell.h"
#import "VenueViewController.h"
#import "TicketCatsViewController.h"
#import "ArtistLocationDetailViewController.h"
#import "MapViewController.h"

#define kIntervalHeight 60

@interface SeeMoreTicketViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsTitleTop;
@property (weak, nonatomic) IBOutlet UIView *viewLocation;
@property (nonatomic,strong) NSArray *intervals;
@end

@implementation SeeMoreTicketViewController

- (void)viewWillAppear:(BOOL)animated {
    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_eventTypeId intValue]]];
    _eventDetail.type_id = _eventTypeId;
    [self setNavbarAttr];
}
-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.transform = CGAffineTransformMakeScale(-1, 1);
    
    self.intervals =  self.eventDetail.intervals;
    if (_eventDetail.locations.count > 0) {
        _lblTitle.text = _eventDetail.location.title;
        _lblAddress.text = [Utils persianDigits: [NSString stringWithFormat: @"آدرس: %@\nشماره تماس: %@", _eventDetail.location.address, _eventDetail.location.tel]];
    } else {
        _cnsTitleTop.constant = -82;
        _viewLocation.hidden = true;
    }
    [self updateItems];
    if(self.intervals.count ==0){
        if([[MyReachability reachabilityForInternetConnection] isReachable]){
            [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSIntervalsDetail *result = [ServerRequest getEventIntervals:self.eventDetail.id];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if([result.status integerValue] == 1){
                        self.intervals = result.result;
                        [self updateItems];
                    }else{
                        [TSMessage showNotificationInViewController:self.parentViewController title:result.message subtitle:nil type:TSMessageNotificationTypeWarning];
                    }
                });
            });
        }else{
            [TSMessage showNotificationInViewController:self title:nil subtitle:@"اینترنت در دسترس نمی باشد" type:TSMessageNotificationTypeWarning];
        }
    }
    // Do any additional setup after loading the view.
}

-(void) updateItems {
    self.collectionView.scrollEnabled = [self.collectionView.collectionViewLayout collectionViewContentSize].height > self.collectionView.frame.size.height;
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView datasource delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.intervals.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EventTimeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TimeCollectionCell" forIndexPath:indexPath];

    JSInterval *interval = [self.intervals objectAtIndex:indexPath.row];
    cell.viewHeader.backgroundColor =[Tik8DataProvider sharedTik8DataProvider].eventColor;
    cell.lbDay.text = interval.day;
    cell.lbMonth.text = interval.date;
    cell.lbHour.text = interval.time;
    cell.viewContainer.layer.cornerRadius = 4.f;
    cell.viewContainer.clipsToBounds = YES;

    cell.transform = CGAffineTransformMakeScale(-1, 1);
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    JSInterval *interval = [self.intervals objectAtIndex:indexPath.row];
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        if([Tik8DataProvider sharedTik8DataProvider].userInfo){
            if(self.eventDetail.has_seat_chart) {
                [self performSegueWithIdentifier:@"go to venue" sender:interval];
            } else {
                [self performSegueWithIdentifier:@"go to ticket cats" sender:interval];
            }
        }else{
            //[TSMessage showNotificationInViewController:self title:nil subtitle:@"لطفا ابتدا لاگین کنید" type:TSMessageNotificationTypeWarning];
            [self presentViewController:[[self storyboard] instantiateViewControllerWithIdentifier:@"loginVC"] animated:true completion:nil];
        }
    }else{
        [TSMessage showNotificationInViewController:self title:nil subtitle:@"اینترنت در دسترس نمی باشد" type:TSMessageNotificationTypeWarning];
        
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.collectionView.frame.size.width/2-12, kIntervalHeight);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
  if([segue.identifier isEqualToString:@"go to venue"])
    {
        VenueViewController *venue = segue.destinationViewController;
        venue.interval = sender;
        venue.eventInfo = self.eventDetail;
    }
  else if([segue.identifier isEqualToString:@"go to ticket cats"])
  {
      TicketCatsViewController *tcvc = segue.destinationViewController;
      tcvc.interval = sender;
      tcvc.eventInfo = self.eventDetail;
  }
}
- (IBAction)showOnMap:(id)sender {
    // [START] Show event's location on map
    MapViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"mapVC"];
    vc.locationInfo = _eventDetail.location;
    [self.navigationController pushViewController: vc animated:YES];
    // [END]
}
- (IBAction)showDetails:(id)sender {
    // [START] Show location info
    ArtistLocationDetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"artistLocationVC"];
    vc.eventTypeId = _eventDetail.type_id;
    vc.locationInfo = _eventDetail.location;
    [self.navigationController pushViewController: vc animated:YES];
    // [END] Showing location info
}



@end
