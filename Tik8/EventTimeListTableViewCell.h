//
//  EventTimeListTableViewCell.h
//  Tik8
//
//  Created by Pooya on 1/26/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTimeListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIView *viewCircel;
@property (strong, nonatomic) IBOutlet YekanButton *butSeemore;
@property (strong, nonatomic) IBOutlet UIImageView *imgBullet;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDelegate,UICollectionViewDataSource>)dataSourceDelegate index:(NSInteger)index;
@end
