//
//  EventDetailViewController.m
//  Tik8
//
//  Created by Pooya on 1/26/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "EventDetailViewController.h"
#import "EventTitleTableViewCell.h"
#import "EventTimeListTableViewCell.h"
#import "EventTimeCollectionViewCell.h"
#import "EventLocationTableViewCell.h"
#import "ArtistLocationDetailViewController.h"
#import "DescpTableViewCell.h"
#import "CommentViewController.h"
#import "VenueViewController.h"
#import "CelebListViewController.h"
#import "GalleryTableViewCell.h"
#import "IDMPhotoBrowser.h"
#import "TicketCatsViewController.h"
#import "EventLocationsTableViewCell.h"
#import "EventLocation2TableViewCell.h"

#define kTimeTwoCell 135
#define KTimeFourCell 205
#define kGalleryCell 145

#define kGalleryImageCell 90

#define kLBCloseHeight 150

#define kIntervalHeight 60

#define kDescTextMarginFromScreenSides 20

@interface EventDetailViewController ()<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,IDMPhotoBrowserDelegate,MDHTMLLabelDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) JSEvent *eventDetail;
@property (nonatomic) BOOL lbDescriptionOpen;
@property (nonatomic) BOOL lbRollOpen;

@end

@implementation EventDetailViewController

// event type 1 sort
int posHeader = 0;
int posIntervals = 1;
int posAbout = 2;
int posGallery = 3;
int posLocation = 4;
int posRules = 5;
int posFooter = 6;
// event type 2:
int posLocations = -1;

// used for about and rules texts
UIFont *descFont;

- (void)viewWillAppear:(BOOL)animated {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"رویداد %@: %@",self.event.id,self.event.title]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    if(_event.type_id == nil) {
        _event.type_id = _event.type;
    }
    if(_event.type_id.length>0) {
        [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_event.type_id intValue]]];
    }
    [self setNavbarAttr];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    posHeader = 0;
    descFont = [UIFont fontWithName:FontName size:12];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 200;
    [self.tableView reloadData];
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSEventDetail *eventDetail = [ServerRequest getTicketDetail:self.event.id];
            dispatch_async(dispatch_get_main_queue(), ^{
                if([eventDetail.status integerValue] == 1){
                    self.eventDetail = eventDetail.result;
                    if(self.event.image_url.length == 0)self.event.image_url = self.eventDetail.image_url;
                    if (self.eventDetail.locations != nil) {
                        // sort if event is type 2
                        posIntervals = -1;
                        posLocation = -1;
                        posLocations = 1;
                        posAbout = 2;
                        posGallery = 3;
                        posRules = 4;
                        posFooter = 5;
                    }
                    [self.tableView reloadData];
                }else{
                    [TSMessage showNotificationInViewController:self title:eventDetail.message subtitle:nil type:TSMessageNotificationTypeError];
                }
            });
        });
    }else{
        [TSMessage showNotificationInViewController:self title:nil subtitle:@"اینترنت در دسترس نمی باشد" type:TSMessageNotificationTypeWarning];
    }
    [self.navigationController setNavigationBarHidden:false animated:NO];
    // Do any additional setup after loading the view.
}

-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

-(CGFloat)heightOfDescpCell{
    if(self.lbDescriptionOpen){
        CGSize size = CGSizeMake([[UIScreen mainScreen] bounds].size.width - kDescTextMarginFromScreenSides, [[UIScreen mainScreen] bounds].size.height);
        return [MDHTMLLabel sizeThatFitsHTMLString:self.eventDetail.Description withFont:descFont constraints: size limitedToNumberOfLines:0 autoDetectUrls:true] + 66;
    }else{
        return kLBCloseHeight;
    }
}

-(CGFloat)heightOfRullsCell{
    if(self.lbRollOpen){
        CGSize size = CGSizeMake([[UIScreen mainScreen] bounds].size.width - kDescTextMarginFromScreenSides, [[UIScreen mainScreen] bounds].size.height);
        return [MDHTMLLabel sizeThatFitsHTMLString:self.eventDetail.rules withFont:descFont constraints: size limitedToNumberOfLines:0 autoDetectUrls:true] + 66;
    }else{
        return kLBCloseHeight;
    }
}

#pragma mark - TableView datasource delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.eventDetail)
        if (self.eventDetail.locations != nil) {
            return 6;
        } else {
            return 7;
        }
    else
        return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.eventDetail){
        // we can't use switch case here!
        if(indexPath.row == posHeader) {
                return (self.view.frame.size.width * 0.4 * 1.41)+20;
        } else if(indexPath.row == posIntervals) {
                if(self.eventDetail.intervals.count > 2)
                    return KTimeFourCell;
                else
                    return kTimeTwoCell;
        } else if (indexPath.row == posAbout) {
                return [self heightOfDescpCell];
        } else if (indexPath.row == posGallery) {
                return (self.eventDetail.galleries.count > 0) ? kGalleryCell : 0 ;
        } else if (indexPath.row == posFooter) {
                return 90;
        } else if (indexPath.row == posLocation) {
                return UITableViewAutomaticDimension;
        } else if (indexPath.row == posRules) {
                return [self heightOfRullsCell];
        } else if (indexPath.row == posLocations) {
            return 92 * _eventDetail.locations.count + 41;
        }
    } else {
        switch (indexPath.row) {
            case 0:
                return (self.view.frame.size.width * 0.4 * 1.41)+20;
                break;
            case 1:
                return 40;
                break;
            default:
                break;
        }
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(!_eventDetail && indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[cell viewWithTag:100];
        [indicator startAnimating];
        return cell;
    }
    if(indexPath.row == posHeader){
        EventTitleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TitleCell" forIndexPath:indexPath];
        [cell.imgMain sd_setImageWithURL:[NSURL URLWithString:self.event.image_url != nil ? self.event.image_url : self.eventDetail.image_url] placeholderImage:[UIImage imageNamed:@"place-holder_event"]];
        cell.lbName.text = self.event.title != nil ? self.event.title : self.eventDetail.title;
        if(_event.interval != nil) {
            cell.lbTime.text = [NSString stringWithFormat:@"از %@",_event.interval != nil ? _event.interval : @" "];
        } else {
            cell.viewTime.hidden = true;
        }
        NSString *price = self.eventDetail.min_price;
        price = [Utils formatedNumber:[price doubleValue]];
        cell.lbPrice.text = (self.eventDetail)?[NSString stringWithFormat:@"از %@ تومان",price]:@" ";
        if (self.event.location_title != nil) {
            cell.lbLocation.text = self.eventDetail.location.title != nil ? self.eventDetail.location.title : @" ";
            cell.cnsTimeBottom.constant = 0;
            cell.viewLocation.hidden = false;
        } else {
            cell.cnsTimeBottom.constant = -29;
            cell.viewLocation.hidden = true;
        }
        [cell.butCeleb addTarget:self action:@selector(celebPressed) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } else if (indexPath.row == posIntervals) {
        if(self.eventDetail){
            EventTimeListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimeCell" forIndexPath:indexPath];
            cell.viewCircel.backgroundColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
            [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
            cell.collectionView.transform = CGAffineTransformMakeScale(-1, 1);

            if(self.eventDetail.intervals.count > 4){
                cell.butSeemore.hidden = NO;
                [cell.butSeemore addTarget:self action:@selector(seemorePressed) forControlEvents:UIControlEventTouchUpInside];
            }else{
                cell.butSeemore.hidden = YES;
            }

            cell.imgBullet.image = [[UIImage imageNamed:@"Tiket"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            [cell.imgBullet setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
            return cell;
        }
    }else if (indexPath.row == posAbout){
        DescpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DescpCell" forIndexPath:indexPath];
        cell.lbDescp.htmlText = self.eventDetail.Description;
        cell.lbDescp.delegate = self;
        cell.lbTitle.text = @"درباره رویداد";
        cell.lbDescp.font = descFont;
        if(self.lbDescriptionOpen){
            cell.lbMore.text = @"بستن توضیحات ▴";
        }else{
            cell.lbMore.text = @"ادامه توضیحات ▾";
        }
        cell.imgBullet.image = [[UIImage imageNamed:@"About"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.imgBullet setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
        return cell;
    }else if (indexPath.row == posGallery){
        GalleryTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"GalleryCell" forIndexPath:indexPath];
        cell.circelView.backgroundColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
        [cell setCollectionViewDataSourceDelegate:self index:indexPath.row];
        if(self.eventDetail.galleries.count > 0){
            cell.viewLine.hidden = NO;
            cell.lbTitle.hidden = NO;
            cell.circelView.hidden = NO;
        }else{
            cell.viewLine.hidden = YES;
            cell.lbTitle.hidden = YES;
            cell.circelView.hidden = YES;
        }
        cell.imgBullet.image = [[UIImage imageNamed:@"Image"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.imgBullet setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
        return cell;
    }else if (indexPath.row == posRules){
        DescpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DescpCell" forIndexPath:indexPath];
        cell.lbDescp.htmlText = self.eventDetail.rules;
        cell.lbDescp.delegate = self;
        cell.lbDescp.font = descFont;
        [cell.lbDescp setTextAlignment: NSTextAlignmentRight];
        cell.lbTitle.text = @"قوانین و مقررات";
        if(self.lbRollOpen){
            cell.lbMore.text = @"بستن قوانین و مقررات ▴";
        }else{
            cell.lbMore.text = @"ادامه قوانین و مقررات ▾";
        }
        cell.imgBullet.image = [[UIImage imageNamed:@"Rule"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.imgBullet setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];

        return cell;
    }else if (indexPath.row == posFooter){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CelebCell" forIndexPath:indexPath];
        UIButton *viewComment = (UIButton *)[cell.contentView viewWithTag:100];
        UIImageView *imgComment = (UIImageView *)[cell.contentView viewWithTag:103];
        UIButton *viewShare = (UIButton *)[cell.contentView viewWithTag:200];
        UIImageView *imgShare = (UIImageView *)[cell.contentView viewWithTag:203];
        UIButton *viewProfile = (UIButton *)[cell.contentView viewWithTag:300];
        UIImageView *imgProfile = (UIImageView *)[cell.contentView viewWithTag:303];

        UITapGestureRecognizer *commentTGR =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(butCommentPressed:)];
        [viewComment addGestureRecognizer:commentTGR];
        UITapGestureRecognizer *shareTGR =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(butSharePressed:)];
        [viewShare addGestureRecognizer:shareTGR];
        UITapGestureRecognizer *profileTGR =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(butProfilePressed:)];
        [viewProfile addGestureRecognizer:profileTGR];

        [imgShare setImage:[[UIImage imageNamed:@"event-share"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [imgShare setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
        [imgComment setImage:[[UIImage imageNamed:@"event-comment"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [imgComment setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
        [imgProfile setImage:[[UIImage imageNamed:@"event-profile"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [imgProfile setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];

        return cell;
    }else if (indexPath.row == posLocation){
        EventLocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationCell" forIndexPath:indexPath];
        cell.locationInfo = self.eventDetail.location;
        cell.lbAddress.text = [NSString stringWithFormat:@"%@\n%@", [Utils persianDigits: self.eventDetail.location.address], [Utils persianDigits: [NSString stringWithFormat:@"تلفن: %@",self.eventDetail.location.tel]]];
       // [cell.butSeeLocation setTitleColor:[Tik8DataProvider sharedTik8DataProvider].eventColor forState:UIControlStateNormal];
        cell.viewCircel.backgroundColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
        [cell.butSeeLocation addTarget:self action:@selector(seeLocationPressed) forControlEvents:UIControlEventTouchUpInside];
        [cell.butMap addTarget:self action:@selector(mapPressed) forControlEvents:UIControlEventTouchUpInside];
        
        cell.imgBullet.image = [[UIImage imageNamed:@"Location"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.imgBullet setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];

        return cell;
    } else if (indexPath.row == posLocations) {
        EventLocationsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationsCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.locations = _eventDetail.locations;
        [cell.tableView reloadData];
        // ui customizations
        cell.imgBullet.image = [[UIImage imageNamed:@"Location"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.imgBullet setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == posAbout){ // lb descp
        self.lbDescriptionOpen = !self.lbDescriptionOpen;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }else if(indexPath.row == posRules){ // lb descp
        self.lbRollOpen = !self.lbRollOpen;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - CollectionView datasource delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(collectionView.tag == 1){
        if(self.eventDetail.intervals.count > 4) return 4;
        else return self.eventDetail.intervals.count;
    }else if (collectionView.tag == 3){
        return self.eventDetail.galleries.count;
    }
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView.tag == 1){
        EventTimeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TimeCollectionCell" forIndexPath:indexPath];
        
        JSInterval *interval = [self.eventDetail.intervals objectAtIndex:indexPath.row];
        cell.viewHeader.backgroundColor =[Tik8DataProvider sharedTik8DataProvider].eventColor;
        cell.lbDay.text = interval.day;
        cell.lbMonth.text = interval.date;
        cell.lbHour.text = interval.time;
        cell.viewContainer.layer.cornerRadius = 4.f;
        cell.viewContainer.clipsToBounds = YES;
        cell.transform = CGAffineTransformMakeScale(-1, 1);
        return cell;
    }else if (collectionView.tag == 3){
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GalleryCollectionCell" forIndexPath:indexPath];
        UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:100];
        NSString *url = [self.eventDetail.galleries objectAtIndex:indexPath.row];
        [imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"place-holder_back"]];
        return cell;
    }
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView.tag == 1){
        JSInterval *interval = [self.eventDetail.intervals objectAtIndex:indexPath.row];
        if([[MyReachability reachabilityForInternetConnection] isReachable]){
            if([Tik8DataProvider sharedTik8DataProvider].userInfo){
                if(self.eventDetail.has_seat_chart) {
                    [self performSegueWithIdentifier:@"go to venue" sender:interval];
                } else {
                    [self performSegueWithIdentifier:@"go to ticket cats" sender:interval];
                }
            }else{
               // [TSMessage showNotificationInViewController:self title:nil subtitle:@"لطفا ابتدا لاگین کنید" type:TSMessageNotificationTypeWarning];
                [self presentViewController:[[self storyboard] instantiateViewControllerWithIdentifier:@"loginVC"] animated:true completion:nil];
            }
            
        }else{
            [TSMessage showNotificationInViewController:self title:nil subtitle:@"اینترنت در دسترس نمی باشد" type:TSMessageNotificationTypeWarning];
            
        }
    }else if (collectionView.tag == 3){
        IDMPhotoBrowser *photoBrowser = [[IDMPhotoBrowser alloc]initWithPhotoURLs:self.eventDetail.galleries];
        photoBrowser.delegate = self;
        [photoBrowser setInitialPageIndex:indexPath.row];
        [self presentViewController:photoBrowser animated:YES completion:^{
            
        }];
        
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView.tag==1) {
        return CGSizeMake(collectionView.frame.size.width/2-4, kIntervalHeight);
    } else {
        return CGSizeMake(kGalleryImageCell, kGalleryImageCell);
    }
}

-(void)seeLocationPressed{
    [self performSegueWithIdentifier:@"go to location detail" sender:self.eventDetail.location];
}

-(void)celebPressed{
    //[self performSegueWithIdentifier:@"go to celeb detail" sender:[self.eventDetail.celebrities firstObject]];
}

-(void)seemorePressed{
    [self performSegueWithIdentifier:@"go to see more" sender:self.eventDetail];
}

-(void)mapPressed{
    [self performSegueWithIdentifier:@"go to map" sender:self.eventDetail.location];
}

-(void)butSharePressed:(UITapGestureRecognizer *)recognizer{
    NSString *shareString = self.eventDetail.title;
    NSString *encodeUrl = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self.eventDetail.link, NULL, CFSTR(" "), kCFStringEncodingUTF8));
    NSURL *shareUrl = [NSURL URLWithString:encodeUrl];
    UIImageView *image = [[UIImageView alloc]init];
    [image sd_setImageWithURL:[NSURL URLWithString:self.event.image_url]];
    
    NSArray *activityItems = [NSArray arrayWithObjects:shareString,image.image ,shareUrl, nil];
    
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [self presentViewController:activityViewController animated:YES completion:nil];
    
}
-(void)butCommentPressed:(UITapGestureRecognizer *)recognizer{
    [self performSegueWithIdentifier:@"go to comment" sender:self.event];
    
}
-(void)butProfilePressed:(UITapGestureRecognizer *)recognizer{
    if(self.eventDetail.celebrities.count > 1){
        [self performSegueWithIdentifier:@"go to celeblist" sender:self.eventDetail.celebrities];
    }else{
        JSCelebrity *celeb =[self.eventDetail.celebrities firstObject];
        if([celeb.status boolValue]){
            [self performSegueWithIdentifier:@"go to celeb detail" sender:celeb];
        }else{
            [TSMessage showNotificationInViewController:self title:nil subtitle:@"پروفایل هنرمند موجود نمی‌باشد" type:TSMessageNotificationTypeWarning];
            
        }
        
    }
}

-(void) locationSelected: (int) itemIndex {
    _eventDetail.intervals = ((JSLocation *)[_eventDetail.locations objectAtIndex:itemIndex]).intervals;
    _eventDetail.location = ((JSLocation *)[_eventDetail.locations objectAtIndex:itemIndex]);
    [self performSegueWithIdentifier:@"go to see more" sender:_eventDetail];
}

- (void)HTMLLabel:(MDHTMLLabel *)label didSelectLinkWithURL:(NSURL*)URL{
    [[UIApplication sharedApplication]openURL:URL];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"go to location detail"])
    {
        [segue.destinationViewController setEventTypeId: self.event.type_id];
        [segue.destinationViewController setLocationInfo:sender];
    }
    else if([segue.identifier isEqualToString:@"go to celeb detail"])
    {
        [segue.destinationViewController setEventTypeId: self.event.type_id];
        [segue.destinationViewController setCelebInfo:sender];
    }
    else if([segue.identifier isEqualToString:@"go to see more"])
    {
        [segue.destinationViewController setEventTypeId: self.event.type_id];
        [segue.destinationViewController setEventDetail:sender];
    }
    else if([segue.identifier isEqualToString:@"go to map"])
    {
        [segue.destinationViewController setLocationInfo:sender];
    }
    else if([segue.identifier isEqualToString:@"go to comment"])
    {
        [segue.destinationViewController setEventInfo:sender];
    }
    else if([segue.identifier isEqualToString:@"go to celeblist"])
    {
        [segue.destinationViewController setEventTypeId: self.event.type_id];
        [segue.destinationViewController setCelebList:sender];
    }
    else if([segue.identifier isEqualToString:@"go to venue"])
    {
        VenueViewController *venue = segue.destinationViewController;
        venue.interval = sender;
        venue.eventInfo = self.event;
    }
    else if([segue.identifier isEqualToString:@"go to ticket cats"])
    {
        TicketCatsViewController *tcvc = segue.destinationViewController;
        tcvc.interval = sender;
        tcvc.eventInfo = self.event;
    }
}

@end
