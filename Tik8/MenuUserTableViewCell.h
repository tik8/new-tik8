//
//  MenuUserTableViewCell.h
//  Tik8
//
//  Created by Pooya on 2/2/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircelImageView.h"
@interface MenuUserTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet CircelImageView *imgUser;
@property (strong, nonatomic) IBOutlet YekanLabel *lbName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbDetail;

@end
