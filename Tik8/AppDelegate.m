//
//  AppDelegate.m
//  Tik8
//
//  Created by Pooya on 1/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import <SafariServices/SafariServices.h>

#import "AppboyKit.h"
#import "EventDetailViewController.h"
#import "AddBalanceViewController.h"

#import <Google/Analytics.h>

@interface AppDelegate ()
@property (retain, nonatomic) CLLocationManager *locationManager;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [GMSServices provideAPIKey:@"AIzaSyBLLRjGukiykNN7v_AgrzhQKHc-EcOKzKo"];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];

    [[UINavigationBar appearance]setTintColor:[UIColor colorWithWhite:1.000 alpha:1.000]];
    [[UINavigationBar appearance] setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:FontName size:20]}];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor colorWithWhite:1.000 alpha:1.000],
       NSFontAttributeName:[UIFont fontWithName:FontName size:15]
       }forState:UIControlStateNormal];
    
    [SVProgressHUD setFont:[UIFont fontWithName:FontName size:17]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    self.viewController = [[JASidePanelController alloc] init];
    self.viewController.shouldDelegateAutorotateToVisiblePanel = NO;

    UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    self.viewController.centerPanel = [mystoryboard instantiateViewControllerWithIdentifier:@"HomeNav"];
    self.viewController.rightFixedWidth = 278.0f;
    self.viewController.leftFixedWidth = 0;
    self.viewController.rightPanel = [mystoryboard instantiateViewControllerWithIdentifier:@"menu"];
    self.viewController.panningLimitedToTopViewController = YES;
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSData *encodedObject = [defaults objectForKey:@"gateways"];
            [Tik8DataProvider sharedTik8DataProvider].gateways = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
            [ServerRequest updateGatwaysAvaliable];
            [ServerRequest updateUserInfo];
            [ServerRequest updateCity];
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
        });
    }else{
        
    }

    // google analytics implementation
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);

    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    // end of google analytics implementation

    // app boy implementation
    [Appboy startWithApiKey:@"89a5f5a6-3259-42cd-9e4f-363bbd2a782e"
              inApplication:application
          withLaunchOptions:launchOptions
          withAppboyOptions:@{ABKRequestProcessingPolicyOptionKey: @(ABKAutomaticRequestProcessing)}];
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeAlert |
          UIRemoteNotificationTypeBadge |
          UIRemoteNotificationTypeSound)];
    } else {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeAlert | UIUserNotificationTypeSound) categories:nil];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    // end of app boy implementation

    [self startLocationUpdates]; // location updates

    // [START] Load cached event types
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"eventTypes"];
    NSMutableArray<JSEventType *>* allEventTypes = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    [[Tik8DataProvider sharedTik8DataProvider] setEventTypes:allEventTypes];
    
    if(allEventTypes.count>0) {
        if ([Tik8DataProvider sharedTik8DataProvider].eventType == nil) {
            for(int i=0; i<allEventTypes.count; i++) {
                if(allEventTypes[i].status) {
                    [Tik8DataProvider sharedTik8DataProvider].eventType = allEventTypes[i];
                    break;
                }
            }
        }
    }
    // [END]

    
    
    // [START] QuickActions Implementation
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_8_4) {
        if ([launchOptions[UIApplicationLaunchOptionsShortcutItemKey] isKindOfClass: UIApplicationShortcutItem.class]) {
            [self scheduleShortcut: launchOptions[UIApplicationLaunchOptionsShortcutItemKey]];
            return false;
        }
    }
    [self updateQuickActionItems];
    // [END]

    // [START] Check for update
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        int isUpdateAvailable = [ServerRequest isUpdateAvailable];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isUpdateAvailable == 2) {
                [self showUpdateDialog: YES]; // force update availabe!
            }
            if(![Tik8DataProvider sharedTik8DataProvider].checkedForUpdate && isUpdateAvailable == 1) {
                [Tik8DataProvider sharedTik8DataProvider].checkedForUpdate = true;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self showUpdateDialog: NO]; // update available
                });
            }
        });
    });
    // [END]
    return YES;
}

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"In application:didRegisterForRemoteNotificationsWithDeviceToken, token is %@", [NSString stringWithFormat:@"%@", deviceToken]);
    [[Appboy sharedInstance] registerPushToken:[NSString stringWithFormat:@"%@", deviceToken]];
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    [[Appboy sharedInstance] registerApplication:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:completionHandler];
    NSLog(@"Application delegate method didReceiveRemoteNotification:fetchCompletionHandler: is called with user info: %@", userInfo);
    if (application.applicationState == UIApplicationStateActive ) {
        // showing local notifications if needed.....
        // not needed yet
    }
}

- (void) applicationDidBecomeActive:(UIApplication *)application {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

// Handling DeepLinks
- (BOOL)application:(UIApplication*)application
            openURL:(NSURL*)url
  sourceApplication:(NSString*)sourceApplication
         annotation:(id)annotation
{
    NSString *path  = [url path];
    NSString *query = [url query];

    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    NSArray *urlComponents = [query componentsSeparatedByString:@"&"];
    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
        NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
        
        [queryStringDictionary setObject:value forKey:key];
    }

    if ([path isEqual:@"/event"]) {

        NSString *typeColor = [queryStringDictionary objectForKey:@"color"];
        if(typeColor!=nil) {
            [Tik8DataProvider sharedTik8DataProvider].eventType = [[JSEventType alloc] init];
            [Tik8DataProvider sharedTik8DataProvider].eventType.color = [NSString stringWithFormat:@"#%@", typeColor];
        } else {
            [Tik8DataProvider sharedTik8DataProvider].eventType = [[Tik8DataProvider sharedTik8DataProvider] getEventType:0];
        }

        NSString *id = [queryStringDictionary objectForKey:@"id"];

        UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        EventDetailViewController *evc = [mystoryboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
        evc.event = [JSEvent alloc];
        evc.event.id = id;
        [(UINavigationController*)self.viewController.centerPanel pushViewController:evc animated:YES];
    }
    
    // buy successful, or failed
    NSString *host = [url host];
    if ([host isEqualToString:@"buy.success"]) {
        if ([[self topViewController] isKindOfClass:[SFSafariViewController class]]) {
            [[self topViewController] dismissViewControllerAnimated:true completion:nil];
        }
        UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        UIViewController *vc = [mystoryboard instantiateViewControllerWithIdentifier:@"myTicketsVC"];
        self.viewController.centerPanel = [[UINavigationController alloc] initWithRootViewController:vc];
        [TSMessage showNotificationInViewController:vc title:@"خرید با موفقت انجام شد" subtitle:nil type:TSMessageNotificationTypeSuccess];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [ServerRequest updateUserInfo];
        });
    }
    if ([host isEqualToString:@"buy.failed"]) {
        if ([[self topViewController] isKindOfClass:[SFSafariViewController class]]) {
            [[self topViewController] dismissViewControllerAnimated:true completion:^{
                [TSMessage showNotificationInViewController:[self topViewController] title:@"خرید ناموفق" subtitle:nil type:TSMessageNotificationTypeError];
            }];
        } else {
            [TSMessage showNotificationInViewController:[self topViewController] title:@"خرید ناموفق" subtitle:nil type:TSMessageNotificationTypeError];
        }
    }
    // increase successful, or failed
    if ([host isEqualToString:@"increase.success"]) {
        if ([[self topViewController] isKindOfClass:[SFSafariViewController class]]) {
            [[self topViewController] dismissViewControllerAnimated:true completion:^{
                [self increaseSuccessful];
            }];
        } else {
            [self increaseSuccessful];
        }
    }
    if ([host isEqualToString:@"increase.failed"]) {
        if ([[self topViewController] isKindOfClass:[SFSafariViewController class]]) {
            [[self topViewController] dismissViewControllerAnimated:true completion:^{
                [TSMessage showNotificationInViewController:[self topViewController] title:@"افزایش اعتبار ناموفق" subtitle:nil type:TSMessageNotificationTypeError];
            }];
        } else {
            [TSMessage showNotificationInViewController:[self topViewController] title:@"افزایش اعتبار ناموفق" subtitle:nil type:TSMessageNotificationTypeError];
        }
    }

    return YES;
}

- (void) increaseSuccessful {
    [SVProgressHUD showWithStatus:@"به هنگام سازی موجودی ..." ];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ServerRequest updateUserInfo];
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            // if we are in add balance vc, then go back!!
            if ([[self topViewController] isKindOfClass:[JASidePanelController class]] &&
                    [[(JASidePanelController *)[self topViewController] centerPanel] isKindOfClass:[UINavigationController class]] &&
                    [[(UINavigationController *) [(JASidePanelController *)[self topViewController] centerPanel] visibleViewController] isKindOfClass:[AddBalanceViewController class]]) {
                [(UINavigationController *) [(JASidePanelController *)[self topViewController] centerPanel] popViewControllerAnimated:true];
                [TSMessage showNotificationInViewController:[self topViewController] title:@"افزایش اعتبار با موفقت انجام شد." subtitle:nil type:TSMessageNotificationTypeSuccess];
            } else {
                [TSMessage showNotificationInViewController:[self topViewController] title:@"افزایش اعتبار با موفقت انجام شد." subtitle:nil type:TSMessageNotificationTypeSuccess];
            }
        });
    });
}

// Location Tracking (from appboy documentation)
- (void)startLocationUpdates {
    // Create the location manager if this object does not
    // already have one.
    if (self.locationManager == nil) {
        CLLocationManager *locationManager = [[CLLocationManager alloc] init];
        self.locationManager = locationManager;
        // [locationManager release]; // is not needed because of arc
    }
    
    //self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    // Set a movement threshold for new events.
    self.locationManager.distanceFilter = 1; // meters

    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    /* When you want to request authorization even when the app is in the background, use requestAlwaysAuthorization.
     if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
     [self.locationManager requestAlwaysAuthorization];
     } */
    [self.locationManager startUpdatingLocation];
}

#pragma location manager delegate method
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    // test that the horizontal accuracy does not indicate an invalid measurement
    if (newLocation.horizontalAccuracy < 0) return;
    // test the age of the location measurement to determine if the measurement is cached
    // in most cases you will not want to rely on cached measurements
    NSTimeInterval locationAge = -[newLocation.timestamp timeIntervalSinceNow];
    if (locationAge > 5.0) return;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    // The location "unknown" error simply means the manager is currently unable to get the location.
    if ([error code] != kCLErrorLocationUnknown) {
        [self stopUpdatingLocation];
    }
}

- (void)stopUpdatingLocation {
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
}

// QUICK ACTIONS //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    completionHandler([self scheduleShortcut:shortcutItem]);
}

NSString* shortcutIdentifier;
- (BOOL) scheduleShortcut: (UIApplicationShortcutItem*) shortcutItem {
    shortcutIdentifier = [shortcutItem.type componentsSeparatedByString:@"."].lastObject;
    [self handleShortcutIdentifierIfPossible];
    return true;
}

- (void) handleShortcutIdentifierIfPossible {
    
    if(shortcutIdentifier.length == 0) {
        return; // don't handle quick actions if city is not chosen yet!
    }
    
    NSArray *items = @[@"MyTickets", @"EventsOnMap", @"NearbyEvents"];
    NSInteger item = [items indexOfObject:shortcutIdentifier];
    
    UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    BOOL login = ([Tik8DataProvider sharedTik8DataProvider].userInfo)?YES:NO;
    
    switch (item) {
        case 0: // my netbargs
            if (login) {
                self.viewController.centerPanel = [mystoryboard instantiateViewControllerWithIdentifier:@"myTicketsNav"];
                self.viewController.allowRightSwipe = true;
            } else {
                [TSMessage showNotificationWithTitle:@"لطفأ جهت مشاهده ی بلیط‌های خود، ابتدا وارد حساب کاربری شوید." type:TSMessageNotificationTypeWarning];
            }
            break;
        case 1: // netbargs on map
            self.viewController.centerPanel = [mystoryboard instantiateViewControllerWithIdentifier:@"EventMapVC"];
            self.viewController.allowRightSwipe = false;
            break;
        case 2: // nearby netbargs
            self.viewController.centerPanel = [mystoryboard instantiateViewControllerWithIdentifier:@"nearVC"];
            self.viewController.allowRightSwipe = true;
            break;
        default:
            break;
    }
    
    shortcutIdentifier = @"";
}

- (void) updateQuickActionItems {
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_8_4) {
        return;
    }
    
    BOOL login = ([Tik8DataProvider sharedTik8DataProvider].userInfo)?YES:NO;
    NSArray<UIApplicationShortcutItem *> *shortcutItems = [[UIApplication sharedApplication] shortcutItems];
    
    // if it's first time, add `on map` and `nearby` quick actions
    if (shortcutItems.count == 0) {
        NSMutableArray* arr = [NSMutableArray arrayWithArray:[[UIApplication sharedApplication] shortcutItems]];
        [arr addObject: [[UIMutableApplicationShortcutItem alloc] initWithType:@"com.tik8.Tik8.EventsOnMap" localizedTitle:@"رویدادهای های روی نقشه" localizedSubtitle:@"" icon:[UIApplicationShortcutIcon iconWithTemplateImageName: @"icon-menu-map"] userInfo:nil]];
        [arr addObject: [[UIMutableApplicationShortcutItem alloc] initWithType:@"com.tik8.Tik8.NearbyEvents" localizedTitle:@"رویدادهای نزدیک من" localizedSubtitle:@"" icon:[UIApplicationShortcutIcon iconWithTemplateImageName: @"icon-menu-nearby"] userInfo:nil]];
        [UIApplication sharedApplication].shortcutItems = arr;
        shortcutItems = arr;
    }
    
    // check if `my netbargs` quick action is needed
    if (login) {
        if (![[shortcutItems[0].type componentsSeparatedByString:@"."].lastObject  isEqual: @"MyTickets"]) {
            NSMutableArray* arr = [NSMutableArray arrayWithArray:[[UIApplication sharedApplication] shortcutItems]];
            [arr insertObject: [[UIMutableApplicationShortcutItem alloc] initWithType:@"com.tik8.Tik8.MyTickets" localizedTitle:@"بلیط‌های من" localizedSubtitle:@"" icon:[UIApplicationShortcutIcon iconWithTemplateImageName: @"icon-menu-myTik8"] userInfo:nil] atIndex:0];
            [UIApplication sharedApplication].shortcutItems = arr;
        }
    } else {
        if ([[shortcutItems[0].type componentsSeparatedByString:@"."].lastObject  isEqual: @"MyTickets"]) {
            NSMutableArray* arr = [NSMutableArray arrayWithArray:[[UIApplication sharedApplication] shortcutItems]];
            [arr removeObjectAtIndex:0];
            [UIApplication sharedApplication].shortcutItems = arr;
        }
    }
}

///// CHECK FOR UPDATE /////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void) showUpdateDialog: (BOOL) forceUpdate {
    SCLALertViewButtonBuilder *downloadButton = [SCLALertViewButtonBuilder new].title(@"دریافت و نصب")
    .actionBlock(^{
        NSString *iTunesLink = @"itms://itunes.apple.com/us/app/tik8-tykt/id1000177615?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        if (forceUpdate) {
            [self showUpdateDialog: YES];
        }
    });
    
    SCLAlertViewBuilder *builder = [SCLAlertViewBuilder new]
    .addButtonWithBuilder(downloadButton)
    .shouldDismissOnTapOutside(forceUpdate ? NO : YES)
    .showAnimationType(forceUpdate ? FadeIn : SlideInFromTop);
    
    SCLAlertViewShowBuilder *showBuilder;
    if (forceUpdate) {
        showBuilder = [SCLAlertViewShowBuilder new]
        .style(Info)
        .title(@"به روز رسانی")
        .subTitle(@"لطفأ ویرایش جدید نرم افزار تیکت را دریافت نمایید.")
        .duration(0);
    } else {
        showBuilder = [SCLAlertViewShowBuilder new]
        .style(Info)
        .closeButtonTitle(@"بعدأ یادآوری کن")
        .title(@"به روز رسانی")
        .subTitle(@"ویرایش جدیدی از نرم افزار تیکت قابل دریافت میباشد.")
        .duration(0);
    }
    
    SCLAlertView* alertView = builder.alertView;
    alertView.customViewColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    [alertView setTitleFontFamily:FontName withSize:14];
    [alertView setBodyTextFontFamily:FontName withSize:12];
    [alertView setButtonsTextFontFamily:FontName withSize:14];
    // change buttons' font
    UIView* subview = alertView.view.subviews[0].subviews.lastObject;
    if ([subview isKindOfClass:UIButton.class]) {
        UIButton* btn = (UIButton* ) subview;
        [btn.titleLabel setFont: [UIFont fontWithName:FontName size:btn.titleLabel.font.pointSize]];
    }
    
    [showBuilder showAlertView:alertView onViewController:self.window.rootViewController];
    
}


// active view controller
- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

@end
