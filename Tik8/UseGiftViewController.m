//
//  AddBalanceViewController.m
//  Netbarg
//
//  Created by Pooya on 3/6/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import "UseGiftViewController.h"
#import "ServerRequest.h"

@interface UseGiftViewController ()<UITextFieldDelegate>{
    NSString *_bankLabel;

}

@end

@implementation UseGiftViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"افزایش موجودی حساب";
    
//    if(self.navigationController.viewControllers.count > 1)
//        [self setUpImageBackButton];
    self.tfGift.delegate = self;
    
    UIToolbar *toolbar = [[UIToolbar alloc]init];
    [toolbar sizeToFit];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"تأیید" style:UIBarButtonItemStyleDone target:self action:@selector(handleActionBarDone:)];
    
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects: flexible, doneButton, nil]];
    
    
    self.tfGift.inputAccessoryView = toolbar;
    
	// Do any additional setup after loading the view.
}


- (void)handleActionBarDone:(UIBarButtonItem *)doneButtonItem
{
    [self.tfGift resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)addBalancePressed:(id)sender {
    [self.tfGift resignFirstResponder];
    if([self.tfGift.text length] >= 3){
        if([[MyReachability reachabilityForInternetConnection] isReachable]){
            [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSStatus *response = [ServerRequest useGiftCard:self.tfGift.text];
                if([response.status integerValue] == 1){
                    [ServerRequest updateUserInfo];
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];

                    if([response.status integerValue] == 1){
                        [TSMessage showNotificationInViewController:self.parentViewController title:@"افزایش اعتبار با موفقیت انجام شد." subtitle:nil type:TSMessageNotificationTypeSuccess];
                        [self.navigationController popViewControllerAnimated:YES];
                    }else{
                        [TSMessage showNotificationInViewController:self.parentViewController title:response.message_fa subtitle:nil type:TSMessageNotificationTypeError];
                    }

                });
            });
        }else{
            [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];

        }

    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"کد را درست وارد کنید" subtitle:nil type:TSMessageNotificationTypeWarning];

    }
}

@end
