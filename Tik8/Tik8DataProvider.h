//
//  Tik8DataProvider.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"
#import "JNKeychain.h"
#import "JSUser.h"
#import "JSEventType.h"

@interface Tik8DataProvider : NSObject
CWL_DECLARE_SINGLETON_FOR_CLASS(Tik8DataProvider)

@property (nonatomic,strong) NSMutableArray<JSEventType *> *eventTypes;
@property (nonatomic) JSEventType *eventType;
@property (nonatomic,strong) UIColor *eventColor;
@property (nonatomic,strong) NSString *userToken;
@property (nonatomic,strong) JSUser *userInfo;
@property (nonatomic,strong) NSArray *gateways;
@property (nonatomic,strong) NSArray *cities;
@property (nonatomic) BOOL eventTypesLoaded;
@property (nonatomic) BOOL checkedForUpdate;
-(void) updateEventType:(int)id;
-(void)userSignOut;
-(JSEventType *) getEventType:(int)id;
@end
