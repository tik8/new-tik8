//
//  JSStatus.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@interface JSStatus : JSONModel

@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *message;
@property (nonatomic,strong) NSString *message_fa;

@end
