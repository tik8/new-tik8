//
//  CelebTableViewCell.h
//  Tik8
//
//  Created by Pooya on 5/20/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketCategoryTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet YekanLabel *label;
@property (weak, nonatomic) IBOutlet YekanLabel *detail;
@property (weak, nonatomic) IBOutlet UILabel *arrow;
@property (weak, nonatomic) IBOutlet UITableView *seatsTable;

@end
