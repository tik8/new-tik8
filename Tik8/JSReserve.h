//
//  JSReserve.h
//  Tik8
//
//  Created by Pooya on 5/4/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"
#import "JSSeat.h"
@interface JSReserve : JSONModel
@property (nonatomic,strong) NSString *total_price;
@property (nonatomic,strong) NSString *count;
@property (nonatomic,strong) NSArray <JSSeat> *seats;
@end
