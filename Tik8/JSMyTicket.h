//
//  JSMyTicket.h
//  Tik8
//
//  Created by Pooya on 2/25/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"
#import "JSTicketDate.h"

@protocol JSMyTicket
@end

@interface JSMyTicket : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *quantity;
@property (nonatomic,strong) NSString *location;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) JSTicketDate *date;

@end
