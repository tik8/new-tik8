//
//  JSVenue.m
//  Tik8
//
//  Created by Pooya on 4/11/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSVenue.h"

@implementation JSVenue

+(BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

-(void)setHtml:(NSString *)html{
    _html = html;
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:html options:0];
    NSString *decodedString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
    self.parsedHtml = decodedString;
}

@end
