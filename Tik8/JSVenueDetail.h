//
//  JSVenueDetail.h
//  Tik8
//
//  Created by Pooya on 4/11/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"
#import "JSVenue.h"
#import "JSInterval.h"
#import "JSEvent.h"
@interface JSVenueDetail : JSONModel

@property (nonatomic,strong) JSVenue *venue;

@end
