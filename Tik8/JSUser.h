//
//  JSUser.h
//  Tik8
//
//  Created by Pooya on 2/4/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@interface JSUser : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *firstname;
@property (nonatomic,strong) NSString *lastname;
@property (nonatomic,strong) NSString *profile_image;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *cashBalance;
@property (nonatomic,strong) NSString *phone;
@property (nonatomic,strong) NSString *city_id;
@property (nonatomic,strong) NSString *gender;




@end
