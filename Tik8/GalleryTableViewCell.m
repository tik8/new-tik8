//
//  GalleryTableViewCell.m
//  Tik8
//
//  Created by Pooya on 5/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "GalleryTableViewCell.h"

@implementation GalleryTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDelegate,UICollectionViewDataSource>)dataSourceDelegate index:(NSInteger)index{
    self.collectionView.dataSource = dataSourceDelegate;
    self.collectionView.delegate = dataSourceDelegate;
    self.collectionView.tag = index;
    [self.collectionView reloadData];
}
@end
