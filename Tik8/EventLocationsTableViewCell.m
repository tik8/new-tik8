//
//  EventLocationsTableViewCell.m
//  Tik8
//
//  Created by Sina KH on 7/19/16.
//  Copyright © 2016 Pooya. All rights reserved.
//

#import "EventLocationsTableViewCell.h"
#import "EventLocation2TableViewCell.h"

@implementation EventLocationsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _tableView.scrollEnabled = false;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _locations.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EventLocation2TableViewCell *cell = (EventLocation2TableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"LocationCell" forIndexPath:indexPath];
    JSLocation *location = _locations[indexPath.row];
    cell.lblTitle.text = location.title;
    cell.lblAddress.text = location.address;
    cell.lblViewIntervals.layer.cornerRadius = 3;
    cell.lblViewIntervals.layer.masksToBounds = YES;
    cell.lblViewIntervals.backgroundColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_delegate locationSelected: (int) indexPath.row];
}

@end
