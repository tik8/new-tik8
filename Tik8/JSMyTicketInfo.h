//
//  JSMyTicketInfo.h
//  Tik8
//
//  Created by Pooya on 3/7/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSMyTicketMainInfo.h"


@interface JSMyTicketInfo : JSStatus

@property (nonatomic,strong) JSMyTicketMainInfo *result;

@end
