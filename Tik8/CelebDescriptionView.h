//
//  CelebDescriptionView.h
//  Tik8
//
//  Created by Pooya on 2/2/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDHTMLLabel.h"
@interface CelebDescriptionView : UIView
@property (strong, nonatomic) IBOutlet YekanLabel *lbName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbLastname;
@property (strong, nonatomic) IBOutlet YekanLabel *lbBirthday;
@property (strong, nonatomic) IBOutlet MDHTMLLabel *lbBiography;

@property (strong, nonatomic) IBOutlet UIButton *butFB;
@property (strong, nonatomic) IBOutlet UIButton *butTwitter;
@property (strong, nonatomic) IBOutlet UIButton *butInstagram;
@property (strong, nonatomic) IBOutlet UIButton *butWiki;
@property (strong, nonatomic) IBOutlet UIButton *butSite;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFbWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintTwWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintInstaWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintWikiWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintSiteWidth;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintLbDescreptionheight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintButsViewHeight;

@end
