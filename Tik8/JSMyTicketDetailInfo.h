//
//  JSMyTickerDetailInfo.h
//  Tik8
//
//  Created by Pooya on 3/7/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@protocol JSMyTicketDetailInfo 
@end

@interface JSMyTicketDetailInfo : JSONModel
@property (nonatomic,strong) NSString *ticket_number;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *zone_name;
@property (nonatomic,strong) NSString *row_number;
@property (nonatomic,strong) NSString *seat_number;
@property (nonatomic,strong) NSString *venue_input;
@property (nonatomic,strong) NSString *ticket_hash;
@end
