//
//  JSIntervalCat.h
//  Tik8
//
//  Created by Sina on 10/28/15.
//  Copyright (c) 2015 Sina. All rights reserved.
//

#import "JSONModel.h"

@interface JSIntervalCat : JSONModel

@property (nonatomic, strong) NSString* id;
@property (nonatomic, strong) NSString* identification;
@property (nonatomic, strong) NSString* min_limit;
@property (nonatomic, strong) NSString* max_limit;
@property (nonatomic, strong) NSString* min_quantity_per_user;
@property (nonatomic, strong) NSString* max_quantity_per_user;

@end
