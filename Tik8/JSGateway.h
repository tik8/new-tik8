//
//  JSGateway.h
//  Tik8
//
//  Created by Pooya on 2/28/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@interface JSGateway : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *alias;

@end
