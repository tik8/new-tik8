//
//  JSComments.h
//  Tik8
//
//  Created by Pooya on 2/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSComment.h"

@interface JSComments : JSStatus

@property (nonatomic,strong) NSString *count;
@property (nonatomic,strong) NSArray <JSComment>*result;

@end
