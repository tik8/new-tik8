//
//  JSIntervalPriceCats.h
//  Tik8
//
//  Created by Sina on 10/28/15.
//  Copyright (c) 2015 Sina. All rights reserved.
//

#import "JSONModel.h"
#import "JSIntervalCat.h"
#import "JSPriceCategory.h"

@interface JSIntervalPriceCats : JSONModel

@property (assign, nonatomic) int id;
@property (nonatomic,strong) JSIntervalCat *Interval;
@property (nonatomic,strong) NSArray <JSPriceCategory>*PriceCategories;

@end
