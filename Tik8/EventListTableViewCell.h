//
//  EventListTableViewCell.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet YekanLabel *lbName;
@property (weak, nonatomic) IBOutlet YekanLabel *lbTime;
@property (weak, nonatomic) IBOutlet YekanLabel *lbLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imgBuy;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *viewLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsTimeBottom;

@end
