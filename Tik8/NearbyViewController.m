//
//  NearNetbargViewController.m
//  Netbarg
//
//  Created by Pooya on 2/22/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import "NearbyViewController.h"
#import <MapKit/MapKit.h>
#import "ServerRequest.h"
#import "RelativeEventTableViewCell.h"
#import "EventDetailViewController.h"
#import "HexColors.h"

@interface NearbyViewController ()<CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *userLocation;
@property (nonatomic) int maxDist;
@property (nonatomic,strong) NSArray* events;
@property (nonatomic,strong) NSMutableArray *sortedEvents;
@end

@implementation NearbyViewController

-(NSMutableArray *)sortedEvents{
    if(!_sortedEvents) _sortedEvents = [NSMutableArray array];
    return _sortedEvents;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setNavbarAttr];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    if(IOS_7) self.automaticallyAdjustsScrollViewInsets = NO;

    self.tableView.delegate = self;
    self.tableView.dataSource =self;
    self.slider.tintColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    [self updateLableDescp:self.slider.value];
    
  //  [self loadData];
    //User location tracking
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    self.locationManager.distanceFilter = 500;
#ifdef __IPHONE_8_0
    NSUInteger code = [CLLocationManager authorizationStatus];
    if (code == kCLAuthorizationStatusNotDetermined && ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
        // choose one request according to your business.
        if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
            [self.locationManager requestAlwaysAuthorization];
        } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
            [self.locationManager  requestWhenInUseAuthorization];
        } else {
            NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
        }
    }
#endif
    
    [self.locationManager startUpdatingLocation];
    
    self.userLocation = [self.locationManager.location copy];
    
    if (![CLLocationManager locationServicesEnabled]){
        [self showLocationErrorDialog];
    }
    
    NSLog(@"%f %f User location", self.userLocation.coordinate.latitude, self.userLocation.coordinate.longitude);
    // Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
    [super viewWillDisappear:animated];
}

-(void)loadData{
    
    if([[MyReachability reachabilityForInternetConnection]isReachable]){
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSNearby *nearby = [ServerRequest getNearbyWithLat:self.userLocation.coordinate.latitude long:self.userLocation.coordinate.longitude];
            dispatch_async(dispatch_get_main_queue(), ^{
                if([ServerRequest checkEventTypes]) {
                    if([nearby.status integerValue]==1){
                        self.events = nearby.result;
                    }
                }
                [self calculateAndSortDeals];
                //      [self.refreshControl endRefreshing];
                [SVProgressHUD dismiss];
                
            });
        });
    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
    }
}

- (IBAction)sliderChange:(UISlider *)sender {
    NSLog(@"slider vAlue ==> %f",sender.value);
    [self updateLableDescp:sender.value];
}

-(void)updateLableDescp:(int)value{
    self.maxDist = value;
    self.labelDescp.text = [NSString stringWithFormat:@"برنامه‌های اطراف من در فاصله %d کیلومتری",value];
    [self calculateAndSortDeals];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sortedEvents.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellId = @"NearbyCell";
    RelativeEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    JSEvent *event = [self.sortedEvents objectAtIndex:indexPath.row];
    cell.lbName.text = event.title;
    cell.lbDate.text = event.interval;
    cell.lbLocation.text = event.location_title;
    cell.lbDistance.text = [self readableDistance:[event.distnceFromMyLocation doubleValue]];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:event.image_url]];
    UIColor *color = [UIColor hx_colorWithHexString:[[Tik8DataProvider sharedTik8DataProvider] getEventType:[event.type intValue]].color];
    cell.lbName.textColor = color;
    cell.lbDistance.textColor = color;

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JSEvent *event = [self.sortedEvents objectAtIndex:indexPath.row];
    [[Tik8DataProvider sharedTik8DataProvider] updateEventType:[event.type intValue]];
    [self setNavbarAttr];
    
    [self performSegueWithIdentifier:@"go to detail" sender:event];

}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    self.userLocation = [manager.location copy];
    if([[MyReachability reachabilityForInternetConnection]isReachable]){
        [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSNearby *nearby = [ServerRequest getNearbyWithLat:self.userLocation.coordinate.latitude long:self.userLocation.coordinate.longitude];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                if([nearby.status integerValue]==1){
                    self.events = nearby.result;
                }
                [self calculateAndSortDeals];
                //      [self.refreshControl endRefreshing];
                
            });
        });
    }else{
    }
    
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    if([error.domain isEqualToString:kCLErrorDomain]){
        [self showLocationErrorDialog];
    }
    NSLog(@"location error => %@",error.localizedDescription);
}

-(void)calculateAndSortDeals{
    [self.sortedEvents removeAllObjects];
    for (JSEvent *deal in self.events) {
        [self calculateDistanceFromMyLocation:self.userLocation deals:deal];
    }
    [self.sortedEvents sortUsingFunction:markerSort context:NULL];
    [self.tableView reloadData];
}

-(void)calculateDistanceFromMyLocation:(CLLocation *)myLocation deals:(JSEvent *)deal{
    CLLocation *position = [[CLLocation alloc]initWithLatitude:[deal.latitude doubleValue] longitude:[deal.longitude doubleValue]];
    deal.distnceFromMyLocation = @([myLocation distanceFromLocation:position]/1000.0);
    
    //NSLog(@"dist %f",sm.distanceFromMyLocation);
    if([deal.distnceFromMyLocation doubleValue] < self.maxDist)
        [self.sortedEvents addObject:deal];
}

NSInteger markerSort(id mark1, id mark2, void *context)
{
    JSEvent *sm1 = (JSEvent *)mark1;
    JSEvent *sm2 = (JSEvent *)mark2;
    
    double distance1 = [sm1.distnceFromMyLocation doubleValue];
    double distance2 = [sm2.distnceFromMyLocation doubleValue];
    
    if (distance1 < distance2)
        return NSOrderedAscending;
    else if (distance1 > distance2)
        return NSOrderedDescending;
    else
        return NSOrderedSame;
}

-(NSString *)readableDistance:(double)distance{
    distance *= 1000;
    return [NSString stringWithFormat:@"%d متر",(int)distance];
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"go to detail"]){
        [segue.destinationViewController setEvent:sender];
    }
    
    [self setNavbarAttr];

}

-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

-(void) showLocationErrorDialog {
    
    SCLAlertViewBuilder *builder = [SCLAlertViewBuilder new]
    .shouldDismissOnTapOutside(YES);
    
    SCLAlertViewShowBuilder *showBuilder = [SCLAlertViewShowBuilder new]
    .style(Error)
    .closeButtonTitle(@"تأیید")
    .title(@"خطا در موقعیت یابی")
    .subTitle(@"لطفا از تنظیمات گوشی خود دسترسی را برای اپ تیکت باز کنید.")
    .duration(0);
    
    SCLAlertView* alertView = builder.alertView;
    alertView.customViewColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    [alertView setTitleFontFamily:FontName withSize:14];
    [alertView setBodyTextFontFamily:FontName withSize:12];
    [alertView setButtonsTextFontFamily:FontName withSize:14];
    
    [showBuilder showAlertView:alertView onViewController:self];
}

@end
