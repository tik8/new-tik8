//
//  JSEventType.h
//  Tik8
//
//  Created by Sina on 10/21/15.
//  Copyright (c) 2015 Sina. All rights reserved.
//

#import "JSONModel.h"

@interface JSEventType : JSONModel

@property (assign, nonatomic) int id;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *alias;
@property (nonatomic,strong) NSString *color;
@property (nonatomic,strong) NSString *image_url;
@property (nonatomic,strong) NSString *marker_url;
@property (assign, nonatomic) BOOL status;

@end
