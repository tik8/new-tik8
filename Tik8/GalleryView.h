//
//  GalleryView.h
//  Tik8
//
//  Created by Pooya on 5/25/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryView : UIView
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end
