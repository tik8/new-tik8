//
//  VenueViewController.h
//  Tik8
//
//  Created by Sina on 10/28/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketCatsViewController : UIViewController

@property (strong, nonatomic) IBOutlet YekanLabel *lbNum;
@property (weak, nonatomic) IBOutlet YekanLabel *lbTotalPrice;
@property (weak, nonatomic) IBOutlet YekanLabel *lbBalance;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerHeight;
@property (nonatomic,strong) JSInterval *interval;
@property (nonatomic,strong) JSEvent *eventInfo;
@property (weak, nonatomic) IBOutlet YekanLabel *lbTotalPay;

@property (nonatomic,strong) NSMutableArray<NSString *> * labels;

-(void) labelSelected: (int) row;

@end
