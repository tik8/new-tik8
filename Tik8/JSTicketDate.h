//
//  JSTicketDate.h
//  Tik8
//
//  Created by Pooya on 2/25/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@protocol JSTicketDate 
@end

@interface JSTicketDate : JSONModel

@property (nonatomic,strong) NSString *weekday;
@property (nonatomic,strong) NSString *day;
@property (nonatomic,strong) NSString *month;
@property (nonatomic,strong) NSString *time;

@end
