//
//  DescriptionView.h
//  Tik8
//
//  Created by Pooya on 1/31/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDHTMLLabel.h"
#import <MapKit/MapKit.h>
#import "CircelView.h"
@interface DescriptionView : UIView
@property (weak, nonatomic) IBOutlet MDHTMLLabel *lbDescription;
@property (strong, nonatomic) IBOutlet YekanLabel *lbAddress;
@property (strong, nonatomic) IBOutlet UIButton *butMap;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintLbDescreptionheight;

@property (nonatomic,strong) JSLocation *locationInfo;

@property (strong, nonatomic) IBOutlet UIView *viewFacilities;
@property (strong, nonatomic) IBOutlet UIButton *butFB;
@property (strong, nonatomic) IBOutlet UIButton *butTwitter;
@property (strong, nonatomic) IBOutlet UIButton *butInstagram;
@property (strong, nonatomic) IBOutlet UIButton *butWiki;
@property (strong, nonatomic) IBOutlet CircelView *viewCircel;
@property (strong, nonatomic) IBOutlet CircelView *viewCircel2;
@property (strong, nonatomic) IBOutlet CircelView *viewCircel3;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constrintFbWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintTwWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintInstaWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintWikiWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFacilityParkingWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFacilityILLWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFacilityCoffeeSWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFacilityRestWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFacilityVIPWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFacilityWifiWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintButsViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintFacilitiesViewHeight;
@end
