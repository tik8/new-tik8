//
//  CommentViewController.m
//  Tik8
//
//  Created by Pooya on 2/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "CommentViewController.h"
#import "CommentTableViewCell.h"
#import "YIPopupTextView.h"

#define kCommentDefaultHeight 70
#define kAnswerDefaultHeight 156

@interface CommentViewController ()<UITableViewDelegate,UITableViewDataSource,YIPopupTextViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray *comments;
@end

@implementation CommentViewController

bool isLoading = true;

- (void)viewWillAppear:(BOOL)animated {
    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_eventInfo.type_id intValue]]];
    [self setNavbarAttr];
}
-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 200;
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        isLoading = true;
        [_tableView reloadData];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSComments *result = [ServerRequest getEventComments:self.eventInfo.id];
            dispatch_async(dispatch_get_main_queue(), ^{
                if([result.status integerValue] == 1){
                    self.comments = result.result;
                }else{
                    [TSMessage showNotificationInViewController:self.parentViewController title:result.message subtitle:nil type:TSMessageNotificationTypeWarning];
                }
                isLoading = false;
                [self.tableView reloadData];
            });
        });
    }else{
        
    }
    // Do any additional setup after loading the view.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.comments.count > 0 ? self.comments.count : (isLoading ? 1 : 0);
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (self.comments.count == 0) {
        return [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    }
    
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CommentCell" forIndexPath:indexPath];
    
    JSComment *comment = [self.comments objectAtIndex:indexPath.row];
    cell.viewUser.layer.borderColor = [UIColor colorWithWhite:0.547 alpha:1.000].CGColor;
    cell.viewUser.layer.borderWidth = 1.f;
    cell.viewAnswer.layer.borderColor = [UIColor colorWithWhite:0.547 alpha:1.000].CGColor;
    cell.viewAnswer.layer.borderWidth = 1.f;
    
    cell.lbUserComment.text = comment.comment;
    cell.lbUserName.text = [NSString stringWithFormat:@"%@ / %@",comment.name,comment.comment_date];
    if (comment.avatar.length > 0) {
        [cell.imgUser sd_setImageWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://www.tik8.com%@", comment.avatar]] placeholderImage:[UIImage imageNamed:@"place-holder_user"]];
    } else {
        [cell.imgUser setImage:[UIImage imageNamed:@"place-holder_user"]];
    }
    cell.imgUser.layer.borderWidth = 2.f;
    cell.imgUser.layer.borderColor =[UIColor colorWithWhite:0.547 alpha:1.000].CGColor;
    if(comment.answer.length>0){
        cell.cnsQuestionToBottom.active = false;
        cell.cnsAnswerToBottom.active = true;
        cell.cnsAnswerToBottom.constant = 4;
        cell.viewAnswer.hidden = NO;
        cell.lbAnswerComment.text = comment.answer;
        cell.lbAnswerName.text = [NSString stringWithFormat:@"تیکیت / %@",comment.answer_date];
        cell.imgAnswer.image = [UIImage imageNamed:@"comment-tik8"];
        cell.viewSeprator.hidden = NO;
    }else{
        cell.cnsAnswerToBottom.active = false;
        cell.cnsQuestionToBottom.active = true;
        cell.cnsQuestionToBottom.constant = 4;
        cell.viewAnswer.hidden = YES;
        cell.viewSeprator.hidden = YES;
    }
    
    return cell;
}
- (IBAction)addCommentPressed:(UIBarButtonItem *)sender {
    if([Tik8DataProvider sharedTik8DataProvider].userInfo){
        // NOTE: maxCount = 0 to hide count
        YIPopupTextView* popupTextView = [[YIPopupTextView alloc] initWithPlaceHolder:@"" maxCount:300 buttonStyle:YIPopupTextViewButtonStyleLeftCancelRightDone ];
        popupTextView.delegate = self;
        popupTextView.caretShiftGestureEnabled = YES;
        [popupTextView showInViewController:self.navigationController];
    }else{
       // [TSMessage showNotificationInViewController:self.parentViewController title:@"لطفا ابتدا لاگین کنید." subtitle:nil type:TSMessageNotificationTypeWarning];
        [self presentViewController:[[self storyboard] instantiateViewControllerWithIdentifier:@"loginVC"] animated:true completion:nil];
    }
    
}

- (void)popupTextView:(YIPopupTextView*)textView willDismissWithText:(NSString*)text cancelled:(BOOL)cancelled{
    if(!cancelled && text.length>0){
        if([[MyReachability reachabilityForInternetConnection] isReachable]){
            [SVProgressHUD showWithStatus:@"در حال ارسال"];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSStatus *status =[ServerRequest addCommentToEvent:self.eventInfo.id comment:text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    //[TSMessage showNotificationInViewController:self.parentViewController title:status.message subtitle:nil type:([status.status integerValue] == 1)?TSMessageNotificationTypeSuccess:TSMessageNotificationTypeError];
                    if([status.status integerValue] == 1) {
                        [TSMessage showNotificationInViewController:self.parentViewController title:@"دیدگاه شما با موفقیت ثبت گردید\nو پس از تأیید نمایش داده خواهد شد." subtitle:nil type:TSMessageNotificationTypeSuccess];
                    } else {
                        [TSMessage showNotificationInViewController:self.parentViewController title:@"خطا" subtitle:nil type:TSMessageNotificationTypeError];
                    }
                    
                });
            });
        }else{
            
        }
    }else{
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
