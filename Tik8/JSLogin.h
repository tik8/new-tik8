//
//  JSLogin.h
//  Tik8
//
//  Created by Pooya on 2/4/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSUser.h"
@interface JSLogin : JSStatus

@property (nonatomic,strong) JSUser *info;
@property (nonatomic,strong) NSString *token;

@end
