//
//  BuyViewController.m
//  Tik8
//
//  Created by Pooya on 5/6/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#define IOS_9 [[[UIDevice currentDevice] systemVersion] floatValue] >= 9

#import "MySafariViewController.h"
#import "BuyViewController.h"
#import "JSCampaignCodeStatus.h"

#define BANK_TAG_SAMAN 0
#define BANK_TAG_MELLAT 1
#define BANK_TAG_PARSIAN 2


#define BANK_SAMAN @"saman"
#define BANK_MELLAT @"mellat"
#define BANK_PARSIAN @"parsian"

@interface BuyViewController (){
    NSString *_bankLabel;

}
@property (strong, nonatomic) IBOutlet YekanLabel *lbEventName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbDay;
@property (strong, nonatomic) IBOutlet YekanLabel *lbSeatCount;
@property (strong, nonatomic) IBOutlet YekanLabel *lbTotalPrice;
@property (strong, nonatomic) IBOutlet YekanLabel *lbCampaignCode;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet YekanLabel *lbBalance;
@property (strong, nonatomic) IBOutlet YekanLabel *lbTotalPay;
@property (strong, nonatomic) IBOutlet UIView *viewBank;
@property (strong, nonatomic) IBOutlet YekanLabel *lbDescpBank;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintViewBank;
@property (weak, nonatomic) IBOutlet UIView *viewCampaign;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsCampaignHeight;
@property (weak, nonatomic) IBOutlet UIView *viewEnterCampaign;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsBankTop;

@property (strong, nonatomic) NSString* campaignCode;
@property (strong, nonatomic) JSCampaignCodeStatus* campaignCodeStatus;

- (void) updateGatewaysViews;

@end

@implementation BuyViewController

long toPay;

- (void)viewWillAppear:(BOOL)animated {
    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_eventInfo.type_id intValue]]];
    [self setNavbarAttr];
}
-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.campaignCode = nil;
    self.imgView.clipsToBounds = YES;
    self.lbEventName.text = self.eventInfo.title;
    self.lbDay.text = [NSString stringWithFormat:@"%@ %@ %@",self.interval.day,self.interval.date,self.interval.time];
    self.lbSeatCount.text = [NSString stringWithFormat:@"%@ عدد",self.reserve.count];
    self.lbTotalPrice.text = [NSString stringWithFormat:@"%@ تومان",[Utils persianDigits: self.reserve.total_price]];
    self.lbBalance.text = [NSString stringWithFormat:@"%@ تومان",[Utils formatedNumber:[[Tik8DataProvider sharedTik8DataProvider].userInfo.cashBalance doubleValue]]];
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:self.eventInfo.image_url] placeholderImage:[UIImage imageNamed:@"place-holder_event"]];
    [self.lbDescpBank sizeToFit];
    
    NSMutableArray *gateways = [NSMutableArray array];
    for (JSGateway *gateway in [Tik8DataProvider sharedTik8DataProvider].gateways) {
        [gateways addObject:gateway.alias];
    }
    
    [self updateChangingViews];
}

// check if campaign code used. then calculate and show price values!
-(void) updateChangingViews {

    long userBalance = [[Tik8DataProvider sharedTik8DataProvider].userInfo.cashBalance doubleValue];
    long totalFee = [[self.reserve.total_price stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue];

    // [START] calculate total fee with campaign
    long totalFeeWithCampaign = totalFee;
    
    if ([_campaignCodeStatus.status  isEqual: @"1"]) {
        _btnCampaignCode.hidden = true;
        _viewCampaign.hidden = false;
        _viewEnterCampaign.hidden = true;
        _cnsBankTop.constant = -41;
        _cnsCampaignHeight.constant = 41;
        if ([_campaignCodeStatus.discount_value intValue] > 0) {
            totalFeeWithCampaign = totalFee - [_campaignCodeStatus.discount_value intValue];
            _lbCampaignCode.text = [NSString stringWithFormat:@"%@ تومان", [Utils formatedNumber: [_campaignCodeStatus.discount_value intValue]]];
        } else if ([_campaignCodeStatus.discount_percent intValue] > 0) {
            totalFeeWithCampaign = totalFee - totalFee*[_campaignCodeStatus.discount_percent intValue]/100;
            _lbCampaignCode.text = [NSString stringWithFormat:@"%@ درصد", [Utils formatedNumber: [_campaignCodeStatus.discount_percent intValue]]];
        }
        totalFee = MAX(totalFee, 0);
    } else {
        _viewCampaign.hidden = true;
        _cnsCampaignHeight.constant = 0;
    }
    // [END]

    // [START] calculate to pay fee
    toPay = totalFeeWithCampaign - userBalance;
    
    if(toPay < 0){
        toPay = 0;
        self.viewBank.hidden = YES;
        self.constraintViewBank.constant = 0;
    }
    // [END]

    // [START] Set UILabel values
    _lbTotalPrice.text = [NSString stringWithFormat:@"%@ تومان", [Utils formatedNumber: totalFee]];
    _lbTotalPay.text = [NSString stringWithFormat:@"%@ تومان", [Utils formatedNumber: toPay]];
    // [END]

    if([Tik8DataProvider sharedTik8DataProvider].gateways.count>0) {
        
        [self updateGatewaysViews];
        
    } else {
        
        [self updateGateways];
        
    }
}

-(void) updateGateways {
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        if (toPay > 0) {
            [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [ServerRequest updateGatwaysAvaliable];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                if([Tik8DataProvider sharedTik8DataProvider].gateways.count>0) {
                    [self updateGatewaysViews];
                } else {
                    [TSMessage showNotificationInViewController:self.parentViewController title:@"درگاه پرداختی یافت نشد" subtitle:nil type:TSMessageNotificationTypeError];
                    
                    [self.navigationController popViewControllerAnimated:YES];
                }
            });
        });
    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
        if (toPay > 0) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void) updateGatewaysViews {
    
    NSMutableArray *gateways = [NSMutableArray array];
    for (JSGateway *gateway in [Tik8DataProvider sharedTik8DataProvider].gateways) {
        [gateways addObject:gateway.alias];
    }
    
    bool noBanks = true;
    
    if(![gateways containsObject:BANK_SAMAN]){
        self.bankSaman.hidden = YES;
        self.constraintButSamanWidth.constant = 0;
    } else {
        noBanks = false;
    }
    
    if(![gateways containsObject:BANK_PARSIAN]){
        self.bankParsian.hidden = YES;
        self.constraintButParsianWidth.constant = 0;
    } else {
        noBanks = false;
    }
    
    if(![gateways containsObject:BANK_MELLAT]){
        self.bankMellat.hidden = YES;
        self.constraintButMellatWidth.constant = 0;
    } else {
        noBanks = false;
    }
    
    if(!self.bankSaman.hidden){
        self.bankSaman.selected = YES;
        _bankLabel = BANK_SAMAN;
    }else if (!self.bankParsian.hidden){
        self.bankParsian.selected = YES;
        _bankLabel = BANK_PARSIAN;
    }else if (!self.bankMellat.hidden){
        self.bankMellat.selected = YES;
        _bankLabel = BANK_MELLAT;
    }
    
    if(noBanks) {
        self.viewBank.hidden = true;
        self.constraintViewBank.constant = 0;
    }

}

- (IBAction)bankPressed:(UIButton *)sender {
    
    if(sender.tag == BANK_TAG_SAMAN){
        self.bankSaman.selected = YES;
        self.bankMellat.selected = NO;
        self.bankParsian.selected = NO;
        _bankLabel = BANK_SAMAN;
        
        
    }else if (sender.tag == BANK_TAG_MELLAT){
        self.bankSaman.selected = NO;
        self.bankMellat.selected = YES;
        self.bankParsian.selected = NO;
        _bankLabel = BANK_MELLAT;
        
    }else{
        self.bankSaman.selected = NO;
        self.bankMellat.selected = NO;
        self.bankParsian.selected = YES;
        _bankLabel = BANK_PARSIAN;
        
    }
    
    
}

- (IBAction)buyPressed:(id)sender{
        if([[MyReachability reachabilityForInternetConnection] isReachable]){

            id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
            [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"خرید بلیط"
                                                                  action:@"اتمام خرید"
                                                                   label:[NSString stringWithFormat:@"%@: %@ - %@ %@ %@ - %@ عدد",self.eventInfo.id, self.eventInfo.title, self.interval.day,self.interval.date,self.interval.time, self.reserve.count]
                                                                   value:@1] build]];

            [SVProgressHUD showWithStatus:@"ارتباط با سرور ..."];
            JSGateway *gate;
            for (JSGateway *gateway in [Tik8DataProvider sharedTik8DataProvider].gateways) {
                if([gateway.alias isEqualToString:_bankLabel]){
                    gate = gateway;
                    break;
                }
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSPaymentResponse *response;
                if(_cats.count==0) {
                    response = [ServerRequest buyTickets:self.interval.id gateway:gate.id campaignCode: _campaignCode];
                } else {
                    response = [ServerRequest buyTicketsByCat:self.interval.id gateway:gate.id cats:_cats campaignCode: _campaignCode];
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    
                    if([response.status integerValue] == 1){
                        
                        // to gateway!
                        NSURL *URL = [NSURL URLWithString:response.payUrl];
                        if (IOS_9) {
                            MySafariViewController *sfvc = [[MySafariViewController alloc] initWithURL:URL];
                            [self presentViewController:sfvc animated:YES completion:nil];
                        } else {
                            [[UIApplication sharedApplication] openURL:URL];
                        }
                        
                    } else {
                        [TSMessage showNotificationInViewController:self.parentViewController title:response.message_fa subtitle:nil type:TSMessageNotificationTypeError];
                    }
                    
                });
            });
        }else{
            [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
        }

}

// open up campaign code popup and then check it!
- (IBAction)campaignCodePressed:(id)sender {
    
    SCLALertViewTextFieldBuilder *textField = [SCLALertViewTextFieldBuilder new].title(@"کد تخفیف شما");
    SCLALertViewButtonBuilder *doneButton = [SCLALertViewButtonBuilder new].title(@"استفاده")
    .actionBlock(^{

        // [START] check the campaign code
        _campaignCode = [textField.textField.text copy];
        
        [SVProgressHUD showWithStatus:@"بررسی کد ..."];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            JSCampaignCodeStatus *response = [ServerRequest checkCampaignCode:_campaignCode intervalId:_interval.id];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                if([response.status integerValue] == 1){
                    _campaignCodeStatus = response;
                    
                    NSString* message;
                    if (response.discount_value > 0) {
                        message = [NSString stringWithFormat:@"%@ تومان", response.discount_value];
                    } else if (response.discount_percent > 0) {
                        message = [NSString stringWithFormat:@"%@ درصد", response.discount_percent];
                    }
                    message = [message stringByAppendingString: @" تخفیف اعمال شد."];
                    [TSMessage showNotificationInViewController:self.parentViewController title:message subtitle:nil type:TSMessageNotificationTypeSuccess];
                    
                    [self updateChangingViews];
                } else {
                    [TSMessage showNotificationInViewController:self.parentViewController title:response.message_fa subtitle:nil type:TSMessageNotificationTypeError];
                }
                
            });
        });
        // [END]
    
    });
    
    SCLAlertViewBuilder *builder = [SCLAlertViewBuilder new]
    .addTextFieldWithBuilder(textField)
    .addButtonWithBuilder(doneButton)
    .shouldDismissOnTapOutside(YES);
    
    SCLAlertViewShowBuilder *showBuilder = [SCLAlertViewShowBuilder new]
    .style(Custom)
    .title(@"اعمال کد تخفیف")
    .subTitle(@"کد تخفیف را وارد نمایید.")
    .closeButtonTitle(@"انصراف")
    .duration(0)
    .image([UIImage imageNamed:@"icon-menu-myTik8"]);
    
    SCLAlertView* alertView = builder.alertView;
    alertView.customViewColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    [alertView setTitleFontFamily:FontName withSize:14];
    [alertView setBodyTextFontFamily:FontName withSize:12];
    [alertView setButtonsTextFontFamily:FontName withSize:14];
    [alertView setIconTintColor:[UIColor whiteColor]];
    for (UIView *subview in alertView.view.subviews[0].subviews)
    {
        // change buttons' font
        if ([subview isKindOfClass:UIButton.class]) {
            UIButton* btn = (UIButton* ) subview;
            [btn.titleLabel setFont: [UIFont fontWithName:FontName size:btn.titleLabel.font.pointSize]];
        }
        // become first responder
        if ([subview isKindOfClass:UITextField.class]) {
            UITextField* tf = (UITextField* ) subview;
            [tf setFont: [UIFont fontWithName:FontName size:tf.font.pointSize]];
        }
    }

    [showBuilder showAlertView:alertView onViewController:self];

}

@end
