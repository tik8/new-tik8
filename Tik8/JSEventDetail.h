//
//  JSTicketDetail.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSEvent.h"
@interface JSEventDetail : JSStatus

@property (nonatomic,strong) JSEvent *result;

@end
