//
//  CelebTableViewCell.h
//  Tik8
//
//  Created by Pooya on 5/20/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CelebTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgCeleb;
@property (strong, nonatomic) IBOutlet YekanLabel *lbName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbPosition;

@end
