//
//  JSInterval.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@protocol JSInterval @end

@interface JSInterval : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *day;
@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *status;

@end
