//
//  JSCelebrity.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"
#import "JSRelativeEvent.h"

@protocol JSCelebrity @end

@interface JSCelebrity : JSONModel
@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *firstname;
@property (nonatomic,strong) NSString *lastname;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *birthday;
@property (nonatomic,strong) NSString *facebook_address;
@property (nonatomic,strong) NSString *twitter_address;
@property (nonatomic,strong) NSString *instagram_address;
@property (nonatomic,strong) NSString *wiki_address;
@property (nonatomic,strong) NSString *website_address;
@property (nonatomic,strong) NSString *biography;
@property (nonatomic,strong) NSString *profile_image;
@property (nonatomic,strong) NSString *position;
@property (nonatomic,strong) NSArray <JSRelativeEvent>*related_events;
@end
