//
//  EventTimeCollectionViewCell.h
//  Tik8
//
//  Created by Pooya on 1/26/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTimeCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet YekanLabel *lbDay;
@property (weak, nonatomic) IBOutlet YekanLabel *lbMonth;
@property (weak, nonatomic) IBOutlet YekanLabel *lbHour;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@end
