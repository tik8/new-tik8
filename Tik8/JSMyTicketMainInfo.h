//
//  JSMyTicketMainInfo.h
//  Tik8
//
//  Created by Pooya on 3/7/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"
#import "JSMyTicketDetailInfo.h"
@interface JSMyTicketMainInfo : JSONModel

@property (nonatomic,strong) NSString *event_title;
@property (nonatomic,strong) NSString *location_title;
@property (nonatomic,strong) NSString *location_address;
@property (nonatomic,strong) NSString *venue_name;
@property (nonatomic,strong) NSString *event_date;
@property (nonatomic,strong) NSString *event_time;
@property (nonatomic,strong) NSString *total_price;
@property (nonatomic,strong) NSString *order_id;
@property (nonatomic,strong) NSString *user_name;
@property (nonatomic,strong) NSString *user_phone;
@property (nonatomic,strong) NSArray <JSMyTicketDetailInfo>*tickets;

@end
