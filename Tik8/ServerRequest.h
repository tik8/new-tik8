//
//  ServerRequest.h
//  Netbarg
//
//  Created by Pooya on 1/6/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSEventList.h"
#import "JSEventDetail.h"
#import "JSLocationDetail.h"
#import "JSCelebrityDetail.h"
#import "JSLogin.h"
#import "JSPositions.h"
#import "JSNearby.h"
#import "JSComments.h"
#import "JSMyTicketDetail.h"
#import "JSGateway.h"
#import "JSMyTicketInfo.h"
#import "JSIntervalsDetail.h"
#import "JSCityDetail.h"
#import "JSPaymentResponse.h"
#import "JSVenueDetail.h"
#import "JSReserveResponse.h"
#import "JSIntervalPriceCatsResponse.h"
#import "JSCampaignCodeStatus.h"

@interface ServerRequest : NSObject

+(JSEventList *)getEventsList:(NSString *)title page:(NSInteger)page;
+(JSEventDetail *)getTicketDetail:(NSString *)ticketID;
+(JSLocationDetail *)getLocationDetail:(NSString *)locationID;
+(JSCelebrityDetail *)getCelebrityDetail:(NSString *)celebID;
+(JSLogin *)loginWithUsername:(NSString *)username password:(NSString *)password;
+(JSStatus *)registerWithUsername:(NSString *)username password:(NSString *)password firstname:(NSString *)firstname lastname:(NSString *)lastname phone:(NSString *)phone;
+(JSStatus *)forgotPassEmail:(NSString *)email;
+(JSPositions *)getAllPositions;
+(JSNearby *)getNearbyWithLat:(double)lat long:(double)lng;
+(JSStatus *)editProfileWithName:(NSString *)firstname lastname:(NSString *)lastname phone:(NSString *)phone;
+(JSLogin *)updateUserInfo;
+(JSComments *)getEventComments:(NSString *)eventID;
+(JSStatus *)addCommentToEvent:(NSString *)eventID comment:(NSString *)comment;
+(JSMyTicketDetail *)getMyTicket;
+(JSMyTicketInfo *)getMyTicketInfo:(NSString *)ticketID;
+(JSIntervalsDetail *)getEventIntervals:(NSString *)eventID;
+(JSStatus *)subscribeInEvents:(NSString *)email inCity:(NSString *)cituID;
+(JSPaymentResponse *)addToWallet:(NSString *)amount gateway:(NSString *)gateway;
+(JSVenueDetail *)getEventvenueForInterval:(NSString *)intervalId;
+(JSVenue *)getEventvenueSeat:(NSString *)venueID;
+(JSReserveResponse *)reserveSeats:(NSString *)seats deletedSeats:(NSString *)deletedSeats;
+(JSReserveResponse *)getReserveForInterval:(NSString *)interval;
+(JSStatus *)cancelSeat:(NSString *)seatID;
+(JSPaymentResponse *)buyTickets:(NSString *)indervalID gateway:(NSString *)gateway campaignCode: (NSString *) campaignCode;
+(JSPaymentResponse *)buyTicketsByCat:(NSString *)indervalID gateway:(NSString *)gateway cats: (NSMutableArray *) cats campaignCode: (NSString *) campaignCode;
+(void)updateGatwaysAvaliable;
+(void)updateCity;
+(BOOL)checkEventTypes;
+(int)isUpdateAvailable;
+(JSIntervalPriceCatsResponse *)getIntervalPriceCategory:(NSString*) intervalId;
+(JSStatus *)useGiftCard:(NSString *)code;
+(JSCampaignCodeStatus *) checkCampaignCode: (NSString *) code intervalId: (NSString *) intervalId;

@end
