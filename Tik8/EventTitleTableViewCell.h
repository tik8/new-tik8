//
//  EventTitleTableViewCell.h
//  Tik8
//
//  Created by Pooya on 1/26/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTitleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgMain;
@property (weak, nonatomic) IBOutlet UIView *viewTime;
@property (weak, nonatomic) IBOutlet YekanLabel *lbName;
@property (weak, nonatomic) IBOutlet YekanLabel *lbTime;
@property (weak, nonatomic) IBOutlet YekanLabel *lbLocation;
@property (weak, nonatomic) IBOutlet YekanLabel *lbPrice;
@property (strong, nonatomic) IBOutlet UIButton *butCeleb;
@property (weak, nonatomic) IBOutlet UIView *viewLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsTimeBottom;

@end
