//
//  SeatViewController.m
//  Tik8
//
//  Created by Pooya on 4/15/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "SeatViewController.h"

#define CanContinueViewHeight 48

@interface SeatViewController ()<UIWebViewDelegate,UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic,strong) NSMutableArray *reservedSeat;
@property (nonatomic,strong) NSMutableArray *deletedSeat;
@property (strong, nonatomic) IBOutlet YekanLabel *lbPrice;
@property (strong, nonatomic) IBOutlet YekanLabel *lbVenue;
@property (strong, nonatomic) IBOutlet YekanLabel *lbRow;
@property (strong, nonatomic) IBOutlet YekanLabel *lbSearNom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnsCanContinueHeight;

@property (nonatomic) BOOL canContinue;

@end

@implementation SeatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
    
    [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    self.webView.delegate = self;
    [self.webView loadHTMLString:self.htmlPage baseURL:nil];
    self.reservedSeat = [NSMutableArray array];
    self.deletedSeat = [NSMutableArray array];
    self.lbPrice.text = @"";
    self.lbRow.text = @"";
    self.lbSearNom.text = @"";
    self.lbVenue.text = @"";
    self.automaticallyAdjustsScrollViewInsets = NO;
    // Do any additional setup after loading the view.
    
    _canContinue = true;
    _cnsCanContinueHeight.constant = 0;
    
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if([request.URL.scheme isEqualToString:@"tik8"]){
        NSLog(@"load req ===> %@",request.URL.fragment);
        [self addSeatToReserve:request.URL.host];
        NSArray *splited = [request.URL.fragment componentsSeparatedByString:@"-"];
        self.lbPrice.text = [splited objectAtIndex:0];
        self.lbRow.text = [splited objectAtIndex:3];
        self.lbSearNom.text = [splited objectAtIndex:2];
        self.lbVenue.text = [[splited objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        self.canContinue = true;
        if (splited.count > 4) {
            if ([[splited objectAtIndex: 4]  isEqual: @"1"]) {
                self.canContinue = false;
            }
        }
        _cnsCanContinueHeight.constant = _canContinue ? 0 : CanContinueViewHeight;
    }
    return YES;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 5;
}

#pragma mark -
#pragma mark - UIScrollView Delegate Methods

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    self.webView.scrollView.maximumZoomScale = 5;
}

-(void)addSeatToReserve:(NSString *)seat{
    if([self.reservedSeat containsObject:seat]){
        [self.reservedSeat removeObject:seat];
    }else{
        if([self.beforeReservedSeats containsObject:seat]){
            [self.beforeReservedSeats removeObject:seat];
            [self.deletedSeat addObject:seat];
        }else{
            [self.reservedSeat addObject:seat];
        }
    }
    NSLog(@"Reserve Seat => %@",self.reservedSeat);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)donePressed:(id)sender {
    if (!_canContinue) {
        return;
    }
    if(self.reservedSeat.count > 0 || self.deletedSeat.count > 0){
        NSString *seats = @"";
        if(self.reservedSeat.count > 0){
            for (NSString *string in self.reservedSeat) {
                seats = [seats stringByAppendingString:[NSString stringWithFormat:@"%@,",string]];
            }
            seats = [seats substringToIndex:seats.length-1];
        }
        NSLog(@"seats => [%@]",seats);
        
        NSString *dseats = @"";
        if(self.deletedSeat.count > 0){
            for (NSString *string in self.deletedSeat) {
                dseats = [dseats stringByAppendingString:[NSString stringWithFormat:@"%@,",string]];
            }
            dseats = [dseats substringToIndex:dseats.length-1];
        }
        NSLog(@"dseats => [%@]",dseats);
        
        if([[MyReachability reachabilityForInternetConnection] isReachable]){
            [SVProgressHUD showWithStatus:@"در حال رزرو..."];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSReserveResponse *response = [ServerRequest reserveSeats:seats deletedSeats:dseats];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if([response.status integerValue] == 1){
                        [self.delegate seatViewController:self seatsReserved:response.result];
                    }else{
                        [TSMessage showNotificationInViewController:self title:response.message subtitle:nil type:TSMessageNotificationTypeError];
                        
                    }
                    
                });
            });
        } else {
            [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
        }
    } else if (_hasOtherSeats) {
        [self dismissViewControllerAnimated:true completion:nil];
    }else{
        [TSMessage showNotificationInViewController:self title:@"ابتدا صندلی مورد نظر خود را انتخاب کنید" subtitle:nil type:TSMessageNotificationTypeWarning];
        
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
