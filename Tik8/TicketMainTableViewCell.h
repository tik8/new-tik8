//
//  TicketMainTableViewCell.h
//  Tik8
//
//  Created by Pooya on 3/7/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketMainTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet YekanLabel *lbName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbLocation;
@property (strong, nonatomic) IBOutlet YekanLabel *lbDate;
@property (strong, nonatomic) IBOutlet YekanLabel *lbTime;
@property (strong, nonatomic) IBOutlet YekanLabel *lbBuyer;
@property (strong, nonatomic) IBOutlet YekanLabel *lbPhone;
@property (strong, nonatomic) IBOutlet YekanLabel *lbAddress;
@property (strong, nonatomic) IBOutlet YekanLabel *lbTotalPrice;
@property (strong, nonatomic) IBOutlet YekanLabel *lbFactor;

@end
