//
//  SeatViewController.h
//  Tik8
//
//  Created by Pooya on 4/15/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SeatViewController;

@protocol SeatViewControllerDelegate <NSObject>

-(void)seatViewController:(SeatViewController *)sender seatsReserved:(JSReserve *)seats;

@end

@interface SeatViewController : UIViewController

@property (nonatomic,strong) NSString *htmlPage;
@property (nonatomic,strong) NSMutableArray *beforeReservedSeats;
@property (nonatomic,strong) id <SeatViewControllerDelegate> delegate;
@property (nonatomic) BOOL hasOtherSeats;

@end
