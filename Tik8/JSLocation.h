//
//  JSLocation.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"
#import "JSRelativeEvent.h"
#import "JSInterval.h"
#import "JSVenue.h"
@protocol JSLocation @end

@interface JSLocation : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *tel;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *facebook_address;
@property (nonatomic,strong) NSString *twitter_address;
@property (nonatomic,strong) NSString *instagram_address;
@property (nonatomic,strong) NSString *wiki_address;
@property (nonatomic,strong) NSString *Description;
@property (nonatomic,strong) NSString *parking;
@property (nonatomic,strong) NSString *restaurant;
@property (nonatomic,strong) NSString *internet;
@property (nonatomic,strong) NSString *wheelchair_entrance;
@property (nonatomic,strong) NSString *coffee_shop;
@property (nonatomic,strong) NSString *vip_section;
@property (nonatomic,strong) NSString *cover_url;
@property (nonatomic,strong) NSArray *galleries;
@property (nonatomic,strong) NSArray  <JSRelativeEvent>*related_events;
@property (nonatomic,strong) JSVenue *venue;

// if event is type 2, it's locations have:
@property (nonatomic,strong) NSArray  <JSInterval>*intervals;

@end
