//
//  RegisterViewController.m
//  Netbarg
//
//  Created by Pooya on 1/24/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import "RegisterViewController.h"
#import "ServerRequest.h"

#define SUCCESS_REGISTER @"ثبت نام با موفقیت انجام گردید."
#define kOFFSET_FOR_KEYBOARD 150.0

@interface RegisterViewController ()<UITextFieldDelegate>{
    UITapGestureRecognizer *_tapRecognizer;
    NSString *_provider;
    BOOL _keyboard;
}
@property (nonatomic) BOOL isSuccess;

@end

@implementation RegisterViewController

#pragma mark - view life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];

    self.butSignup.layer.cornerRadius = 4.f;
    
    //    self.navigationItem.leftBarButtonItem = [GlobalMethods buttonForBarButtonWithTitle:@"بازگشت" target:self selector:@selector(cancel:) normalColor:nil highlightColor:nil];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.textFieldEmail.delegate = self;
    self.textFieldPassword.delegate = self;
    self.textFieldName.delegate = self;
    self.textFieldLastName.delegate = self;
    self.textFieldPhone.delegate = self;
    
    
    self.textFieldPassword.returnKeyType = UIReturnKeyDone;
    
    _tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                             action:@selector(didTapAnywhere:)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if(self.scrollView.contentSize.height < self.scrollView.frame.size.height)
        self.scrollView.contentSize = self.scrollView.frame.size;
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self keyboardWillHide:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [super viewWillDisappear:animated];
}



#pragma mark - keyboard and textbox handling

-(void)keyboardWillShow :(NSNotification *) note{
    if(!_keyboard){
        _keyboard = YES;
        [self setViewMovedUp:YES];
        [self.view addGestureRecognizer:_tapRecognizer];
    }
    
}

-(void)keyboardWillHide :(NSNotification *) note{
    if(_keyboard){
        _keyboard = NO;
        [self setViewMovedUp:NO];
        [self.view removeGestureRecognizer:_tapRecognizer];
    }
    
}
-(void)setViewMovedUp:(BOOL)movedUp
{
    
    [UIView animateWithDuration:0.3 animations:^{
        if(self.scrollView.contentSize.height < self.scrollView.frame.size.height)
            self.scrollView.contentSize = self.scrollView.frame.size;
        
        UIEdgeInsets rect = self.scrollView.contentInset;
        if (movedUp)
        {
            rect.bottom += kOFFSET_FOR_KEYBOARD;
        }
        else
        {
            rect.bottom -= kOFFSET_FOR_KEYBOARD;
        }
        self.scrollView.contentInset = rect;
    } completion:^(BOOL finished) {
        
    }];
    
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType == UIReturnKeyDone)
        [self signupPressed];
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)signupPressed {
    
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        if(!(self.textFieldEmail.text.length == 0  || self.textFieldPassword.text.length == 0 || self.textFieldName.text.length == 0 || self.textFieldLastName.text.length == 0 || self.textFieldPhone.text.length == 0 )){
            
            [self.textFieldEmail resignFirstResponder];
            [self.textFieldPassword resignFirstResponder];
            [self.textFieldName resignFirstResponder];
            [self.textFieldLastName resignFirstResponder];
            [self.textFieldPhone resignFirstResponder];
            
            [SVProgressHUD showWithStatus:@"ثبت نام..."];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSStatus *result = [ServerRequest registerWithUsername:self.textFieldEmail.text password:self.textFieldPassword.text firstname:self.textFieldName.text lastname:self.textFieldLastName.text phone:self.textFieldPhone.text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if([result.status integerValue]==1){
                        [TSMessage showNotificationInViewController:self.parentViewController title:result.message subtitle:nil type:TSMessageNotificationTypeSuccess];
                        [self.navigationController popViewControllerAnimated:YES];
                    }else{
                        [TSMessage showNotificationInViewController:self.parentViewController title:result.message subtitle:nil type:TSMessageNotificationTypeError];
                    }
                });
            });
            
        }else{
            [TSMessage showNotificationInViewController:self.parentViewController title:@"تمامی فیلدها را پر نمایید" subtitle:nil type:TSMessageNotificationTypeWarning];
        }
    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"دسترسی به اینترنت موجود نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
    }
}
@end
