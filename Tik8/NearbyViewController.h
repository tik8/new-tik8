//
//  NearNetbargViewController.h
//  Netbarg
//
//  Created by Pooya on 2/22/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearbyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelDescp;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)sliderChange:(id)sender;

@end
