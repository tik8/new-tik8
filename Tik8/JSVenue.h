//
//  JSVenue.h
//  Tik8
//
//  Created by Pooya on 4/11/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@interface JSVenue : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *html;
@property (nonatomic,strong) NSString *parsedHtml;
@property (nonatomic,strong) NSArray  *reservedSeats;

@end
