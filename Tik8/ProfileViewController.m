//
//  ProfileViewController.m
//  Tik8
//
//  Created by Pooya on 2/7/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIImageView *imgUser;
@property (strong, nonatomic) IBOutlet YekanLabel *lbName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbEmail;
@property (strong, nonatomic) IBOutlet YekanLabel *lbBalance;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.imgUser sd_setImageWithURL:[NSURL URLWithString:[Tik8DataProvider sharedTik8DataProvider].userInfo.profile_image] placeholderImage:[UIImage imageNamed:@"place-holder_user"]];
    self.lbName.text = [NSString stringWithFormat:@"%@ %@",[Tik8DataProvider sharedTik8DataProvider].userInfo.firstname,[Tik8DataProvider sharedTik8DataProvider].userInfo.lastname];
    self.lbEmail.text = [Tik8DataProvider sharedTik8DataProvider].userInfo.email;
    self.lbBalance.text = [NSString stringWithFormat:@"میزان اعتبار شما: %@ تومان",[Tik8DataProvider sharedTik8DataProvider].userInfo.cashBalance];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ServerRequest updateUserInfo];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.lbBalance.text = [NSString stringWithFormat:@"میزان اعتبار شما: %@ تومان",[Tik8DataProvider sharedTik8DataProvider].userInfo.cashBalance];
        });
    });
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCell" forIndexPath:indexPath];
    
    UILabel *label = (UILabel *)[cell.contentView viewWithTag:100];
    switch (indexPath.row) {
        case 0:
            label.text = @"افزایش موجودی";
            break;
        case 1:
            label.text = @"استفاده از کد هدیه";
            break;
        case 2:
            label.text = @"اشتراک در خبرنامه";
            break;
        case 3:
            label.text = @"مشاهده بلیط‌ها";
            break;
            
        default:
            break;
    }
    
    return cell;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"go to add balance" sender:self];
            break;
        case 1:
            [self performSegueWithIdentifier:@"go to use gift" sender:self];
            break;
        case 2:
            [self performSegueWithIdentifier:@"go to subscribe" sender:self];
            break;
        case 3:
            [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"myTicketsVC"] animated:true];
            break;
            
        default:
            break;
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
