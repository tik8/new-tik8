//
//  ObjectAnnotation.h
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>
@interface ObjectAnnotation : NSObject<MKAnnotation> {
	
	CLLocationCoordinate2D	coordinate;
	NSString*				title;
	NSString*				subtitle;
    NSInteger				aid;
    UIColor *color;
}

@property (nonatomic, assign)	CLLocationCoordinate2D	coordinate;
@property (nonatomic, copy)		NSString*				title;
@property (nonatomic, copy)		NSString*				subtitle;
@property (nonatomic, readwrite)     NSInteger				aid;
@property (nonatomic, retain) UIColor *color;
@property (nonatomic,strong) JSLocation *locationInfo;
// add an init method so you can set the coordinate property on startup
//- (id)initWithCoordinate:(CLLocationCoordinate2D)coord;

@end



	
