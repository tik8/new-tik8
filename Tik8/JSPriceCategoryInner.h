//
//  JSPriceCategory.h
//  Tik8
//
//  Created by Sina on 10/29/15.
//  Copyright (c) 2015 Sina. All rights reserved.
//

#import "JSONModel.h"

@interface JSPriceCategoryInner : JSONModel

@property (nonatomic, strong) NSString* id;
@property (nonatomic,strong) NSString *model;
@property (nonatomic,strong) NSString *foreign_key;
@property (nonatomic,strong) NSString *venue_id;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *color;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *created;
@property (nonatomic,strong) NSString *modified;

@end
