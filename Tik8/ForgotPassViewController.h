//
//  ForgotPassViewController.h
//  Netbarg
//
//  Created by Pooya on 3/6/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;

- (IBAction)sendPRessed;
@property (strong, nonatomic) IBOutlet YekanButton *butForgot;
@end
