//
//  CelebTableViewCell.h
//  Tik8
//
//  Created by Pooya on 5/20/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTypeCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet YekanLabel *text;

@end
