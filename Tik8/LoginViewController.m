//
//  LoginViewController.m
//  Netbarg
//
//  Created by Pooya on 1/24/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import "LoginViewController.h"
#import "ServerRequest.h"
#import "AppboyKit.h"
#import "AppDelegate.h"

#define kOFFSET_FOR_KEYBOARD 150.0

@interface LoginViewController ()<UITextFieldDelegate>{
    UITapGestureRecognizer *_tapRecognizer;
    NSString *_provider;
    BOOL _keyboard;
}
@property (nonatomic) BOOL isSuccess;

- (IBAction)cancelPressed:(id)sender;
@end

@implementation LoginViewController

#pragma mark - view life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
    
    self.butLogin.layer.cornerRadius = 4.f;
    
    self.butSignUp.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.butSignUp.layer.borderWidth = 1.f;
    self.butSignUp.layer.cornerRadius = 4.f;
    
    self.butForgotpass.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.butForgotpass.layer.borderWidth = 1.f;
    self.butForgotpass.layer.cornerRadius = 4.f;
    
    
    self.textFieldEmailAddress.delegate = self;
    self.textFieldPassword.delegate = self;
    
    
    self.textFieldPassword.returnKeyType = UIReturnKeyDone;
    
    _tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    //    self.usernameTB.text = @"qolve2@qolve.com";
    //    self.passwordTB.text = @"123456";
    
    //self.scrollView.backgroundColor = LOGIN_BG;	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // self.navigationController.navigationBar.hidden = YES;
    //[self.navigationController setNavigationBarHidden:YES animated:NO];
    if(self.scrollView.contentSize.height < self.scrollView.frame.size.height)
        self.scrollView.contentSize = self.scrollView.frame.size;
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self keyboardWillHide:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [super viewWillDisappear:animated];
}


- (void)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
#pragma mark - keyboard and textbox handling

-(void)keyboardWillShow :(NSNotification *) note{
    if(!_keyboard){
        _keyboard = YES;
        [self setViewMovedUp:YES];
        [self.view addGestureRecognizer:_tapRecognizer];
    }
    
}

-(void)keyboardWillHide :(NSNotification *) note{
    if(_keyboard){
        _keyboard = NO;
        [self setViewMovedUp:NO];
        [self.view removeGestureRecognizer:_tapRecognizer];
    }
    
}
-(void)setViewMovedUp:(BOOL)movedUp
{
    
    [UIView animateWithDuration:0.3 animations:^{
        if(self.scrollView.contentSize.height < self.scrollView.frame.size.height)
            self.scrollView.contentSize = self.scrollView.frame.size;
        
        UIEdgeInsets rect = self.scrollView.contentInset;
        if (movedUp)
        {
            rect.bottom += kOFFSET_FOR_KEYBOARD;
        }
        else
        {
            rect.bottom -= kOFFSET_FOR_KEYBOARD;
        }
        self.scrollView.contentInset = rect;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    NSLog(@"");
}

-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType == UIReturnKeyDone)
        [self loginPressed];
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)loginPressed {
    
    BOOL hasError = NO;
    
    NSString *errorTitle = @"";
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        if(!(self.textFieldEmailAddress.text.length == 0 || self.textFieldPassword.text.length == 0)){
            [self.textFieldEmailAddress resignFirstResponder];
            [self.textFieldPassword resignFirstResponder];
            [SVProgressHUD showWithStatus:@"ورود..."];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSLogin *result = [ServerRequest loginWithUsername:self.textFieldEmailAddress.text password:self.textFieldPassword.text];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if([result.status integerValue]==1){
                        [Tik8DataProvider sharedTik8DataProvider].userToken = result.token;
                        [Tik8DataProvider sharedTik8DataProvider].userInfo = result.info;
                        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        [appDelegate updateQuickActionItems];
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                            [ServerRequest updateUserInfo];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [SVProgressHUD dismiss];

                                    [[NSNotificationCenter defaultCenter]postNotificationName:@"LOAD_MENU" object:nil];
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                    [[Appboy sharedInstance] changeUser:self.textFieldEmailAddress.text];
                                });
                        });
                        
                        
                        
                    }else{
                        [SVProgressHUD dismiss];
                        [TSMessage showNotificationInViewController:self.parentViewController title:result.message_fa subtitle:nil type:TSMessageNotificationTypeError];
                    }
                    
                    
                });
            });
            
        }else{
            hasError = YES;
            errorTitle = @"تمامی فیلدها را پر نمایید";
        }
    }else{
        hasError = YES;
        errorTitle = @"دسترسی به اینترنت موجود نمی باشد";
    }
    
    if(hasError){
        [TSMessage showNotificationInViewController:self.parentViewController title:errorTitle subtitle:nil type:TSMessageNotificationTypeWarning];
        
    }
    
}

- (IBAction)signupPressed {
}

- (IBAction)forgotPassPressed {
}
- (IBAction)cancelPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
