//
//  MyTicketViewController.m
//  Tik8
//
//  Created by Pooya on 2/25/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "MyTicketViewController.h"
#import "MyTicketTableViewCell.h"
#import "TicketInfoViewController.h"
#import "HexColors.h"

#define cOrange [UIColor hx_colorWithHexString:@"#ec762f"]

@interface MyTicketViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSArray *myTickets;
@end

@implementation MyTicketViewController

- (void)viewWillAppear:(BOOL)animated {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"بلیط های من"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    if(self.navigationController.viewControllers.count > 2){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"بازگشت" style:UIBarButtonItemStylePlain target:self action:@selector(backPressed)];
    }
    [self.navigationController.navigationBar setBarTintColor:cOrange];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSMyTicketDetail *result = [ServerRequest getMyTicket];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                if([result.status integerValue] == 1){
                    self.myTickets = result.result;
                    [self.tableView reloadData];
                } else {
                    //[TSMessage showNotificationInViewController:self.parentViewController title:result.message subtitle:nil type:TSMessageNotificationTypeWarning];
                    [TSMessage showNotificationInViewController:self.parentViewController title:@"بلیطی ثبت نشده است." subtitle:nil type:TSMessageNotificationTypeWarning];
                }
            });
        });
    }else{
        [TSMessage showNotificationInViewController:self title:nil subtitle:@"اینترنت در دسترس نمی باشد" type:TSMessageNotificationTypeWarning];
    }
    
    // Do any additional setup after loading the view.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.myTickets.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    JSMyTicketsAndDate *ticket = [self.myTickets objectAtIndex:section];
    return ticket.items.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 25;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    JSMyTicketsAndDate *ticket = [self.myTickets objectAtIndex:section];
    UILabel *label = [[UILabel alloc]init];
    label.font = [UIFont fontWithName:FontName size:15];
    label.textAlignment = NSTextAlignmentRight;
    label.text = [NSString stringWithFormat:@"     %@",ticket.date];
    label.backgroundColor = [UIColor whiteColor];
    [label sizeToFit];
    return label;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MyTicketTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyTicketCell" forIndexPath:indexPath];
    JSMyTicketsAndDate *ticket = [self.myTickets objectAtIndex:indexPath.section];
    JSMyTicket *myTick = [ticket.items objectAtIndex:indexPath.row];
    
    cell.lbName.text = myTick.title;
    cell.lbLocation.text = myTick.location;
    cell.lbCount.text = [NSString stringWithFormat:@"تعداد بلیط: %@",myTick.quantity];
    
    cell.lbDayInWeek.text = myTick.date.weekday;
    cell.lbDay.text = myTick.date.day;
    cell.lbMonth.text = myTick.date.month;
    cell.lbHour.text = myTick.date.time;
    
    switch ([myTick.type integerValue]) {
        case 3:
            cell.imgTail.image = [UIImage imageNamed:@"my-tik8-tik-Theatre"];
            break;
        case 4:
            cell.imgTail.image = [UIImage imageNamed:@"my-tik8-tik-concert"];
            break;
        default:
            break;
    }
    
    cell.lbName.textColor = cOrange;
    cell.viewContainer.layer.borderWidth = 1.f;
    cell.viewContainer.layer.borderColor = [UIColor colorWithWhite:0.816 alpha:1.000].CGColor;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JSMyTicketsAndDate *ticket = [self.myTickets objectAtIndex:indexPath.section];
    JSMyTicket *myTick = [ticket.items objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"go to info" sender:myTick];
}

-(void)backPressed{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if([segue.identifier isEqualToString:@"go to info"]){
         [segue.destinationViewController setMyTickInfo:sender];
     }
 }


@end
