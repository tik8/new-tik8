//
//  BorderView.m
//  Tik8
//
//  Created by Pooya on 6/10/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "BorderView.h"

@implementation BorderView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib{
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5f;
}

@end
