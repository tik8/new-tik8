//
//  EventListViewController.m
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "EventListViewController.h"
#import "EventListTableViewCell.h"
#import "JASidePanelController.h"
#import "EventDetailViewController.h"
#import "SeeMoreTicketViewController.h"
#import "SubcribeViewController.h"

#define CELL_HEIGHT 300

@interface EventListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSMutableArray * events;
@property (nonatomic) BOOL loading;
@property (nonatomic) NSInteger page;
@property (nonatomic) NSInteger lastSeenRow;
@property (nonatomic) BOOL noMoreAvailable;
@property (nonatomic, strong) EventListTableViewCell *prototypeCell;
@property (strong, nonatomic) IBOutlet YekanLabel *lbNoEvent;

@end

@implementation EventListViewController
static NSString *CellIdentifier = @"Event Cell";

-(NSMutableArray *)events{
    if(!_events) _events = [NSMutableArray array];
    return _events;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_eventTypeId intValue]]];
    [self setNavbarAttr];
}
-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.eventTypeId = [NSString stringWithFormat:@"%d",[Tik8DataProvider sharedTik8DataProvider].eventType.id];
    self.lbNoEvent.hidden = YES;
    _lastSeenRow = 0;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    // self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    // self.tableView.rowHeight = UITableViewAutomaticDimension;
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    [self.tableView sendSubviewToBack:refreshControl];
    [self refresh:refreshControl];

    self.navigationItem.title = [Tik8DataProvider sharedTik8DataProvider].eventType.name;
    // Do any additional setup after loading the view.
}

-(void)refresh:(UIRefreshControl *)refreshControl{
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        if(!self.loading){
            self.lastSeenRow = 0;
            self.page = 1;
            self.noMoreAvailable = NO;
            self.loading = YES;
            
            self.lbNoEvent.hidden = YES;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSEventList *events;
                events = [ServerRequest getEventsList:[Tik8DataProvider sharedTik8DataProvider].eventType.alias page:self.page];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([events.status integerValue] == 1){
                        if(self.page == 1)
                            [self.events removeAllObjects];
                        [self.events addObjectsFromArray:events.result];
                        self.page += 1;
                        if(events.count < events.limit){
                            self.noMoreAvailable = YES;
                        }
                        if(self.events.count == 0){
                            
                            self.lbNoEvent.hidden = NO;
                            [self loadSubscribe];
                        }
                    }else{
                        self.noMoreAvailable = YES;
                        if(self.events.count == 0){
                            
                            self.lbNoEvent.hidden = NO;
                            [self loadSubscribe];
                        }
                        
                    }
                    [refreshControl endRefreshing];
                    //      [self.refreshControl endRefreshing];
                    self.loading = NO;
                    [self.tableView reloadData];
                    
                });
            });
        }else{
            [refreshControl endRefreshing];
        }
    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
        [refreshControl endRefreshing];
    }
    
}

-(void)loadData{
    if([[MyReachability reachabilityForInternetConnection]isReachable]){
        self.loading = YES;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSEventList *events;
            events = [ServerRequest getEventsList:[Tik8DataProvider sharedTik8DataProvider].eventType.alias page:self.page];
            dispatch_async(dispatch_get_main_queue(), ^{
                if([events.status integerValue] == 1){
                    if(self.page == 1)
                        [self.events removeAllObjects];
                    [self.events addObjectsFromArray:events.result];
                    self.page += 1;
                    if(events.count < events.limit){
                        self.noMoreAvailable = YES;
                    }
                    if(self.events.count == 0){
                        self.lbNoEvent.hidden = NO;
                        [self loadSubscribe];
                    }
                }else{
                    self.noMoreAvailable = YES;
                    if(self.events.count == 0){
                        
                        self.lbNoEvent.hidden = NO;
                        [self loadSubscribe];
                    }
                    
                }
                self.loading = NO;
                [self.tableView reloadData];
                
            });
        });
        
    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
    }
}

-(void)loadSubscribe{
    SubcribeViewController *subsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"subscribeVC"];
    [self presentViewController:[[UINavigationController alloc]initWithRootViewController:subsVC] animated:YES completion:nil];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row < self.events.count){
        return (self.view.frame.size.width * 0.4 * 1.41)+20;
    }
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.noMoreAvailable){
        // if(self.events.count == 0)return 1;
        return self.events.count;
    }
    return self.events.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if(!_noMoreAvailable){
        if (indexPath.row >= (self.events.count /3 *2)) {
            if (!_loading) {
                // loadRequest is the method that loads the next batch of data.
                [self loadData];
            }
        }
    }
    if(indexPath.row < self.events.count){
        static NSString *CellIdentifier = @"Event Cell";
        EventListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

        JSEvent *event = [self.events objectAtIndex:indexPath.row];

        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:event.image_url] placeholderImage:[UIImage imageNamed:@"place-holder_event"]];
        cell.lbName.text = event.title;
        cell.lbTime.text = [NSString stringWithFormat:@"از %@",event.interval != nil ? event.interval : @""];
        if (event.location_title != nil) {
            cell.cnsTimeBottom.constant = 0;
            cell.viewLocation.hidden = false;
            cell.lbLocation.text = event.location_title != nil ? event.location_title : @"";
        } else {
            cell.lbLocation.text = @"";
            cell.cnsTimeBottom.constant = -29;
            cell.viewLocation.hidden = true;
        }
        cell.lbName.textColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
        cell.imgBuy.tag = indexPath.row;
        //        cell.butBuy.backgroundColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
        //        cell.butBuy.layer.cornerRadius = 4.f;

        cell.imgBuy.image = [[UIImage imageNamed:@"KharidSaree"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [cell.imgBuy setTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];

        UITapGestureRecognizer *buyTGR =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(buyPressed:)];
        [cell.imgBuy addGestureRecognizer:buyTGR];
        cell.imgBuy.userInteractionEnabled = true;

        return cell;
    }else{

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[cell viewWithTag:100];
        [indicator startAnimating];
        return cell;
    }
    
}

#pragma mark - Will Display Cell Methods

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row>_lastSeenRow) {
        cell.layer.opacity = 0.1;
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut animations:^{
            cell.layer.opacity = 1;
        } completion:nil];
        _lastSeenRow = indexPath.row;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row < self.events.count){
        JSEvent *event = [self.events objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"go to detail" sender:event];
    }
}

-(void)buyPressed:(UITapGestureRecognizer *)recognizer{
    JSEvent *event = [self.events objectAtIndex:recognizer.view.tag];
    if (event.multi_locations) {
        [self performSegueWithIdentifier:@"go to detail" sender:event];
    } else {
        [self performSegueWithIdentifier:@"go to interval" sender:event];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"go to detail"]) {
        [segue.destinationViewController setEvent:sender];
    }else if ([segue.identifier isEqualToString:@"go to interval"]) {
        [segue.destinationViewController setEventTypeId: ((JSEvent*)sender).type_id];
        [segue.destinationViewController setEventDetail:sender];
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
