//
//  JSTicket.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"
#import "JSCelebrity.h"
#import "JSLocation.h"
#import "JSInterval.h"
#import "JSVenue.h"

@protocol JSEvent @end

@interface JSEvent : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *type_id;
@property (nonatomic,strong) NSString *location_title;
@property (nonatomic,strong) NSString *interval;
@property (nonatomic,strong) NSString *image_url;
@property (nonatomic,strong) NSString *Description;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString *min_price;
@property (nonatomic,strong) NSString *link;
@property (nonatomic,strong) NSString *cover_url;
@property (nonatomic,strong) NSArray *galleries;
@property (nonatomic,strong) NSString *rules;
@property (nonatomic,strong) NSArray <JSCelebrity>*celebrities;
@property (nonatomic,strong) NSArray <JSInterval>*intervals;
@property (nonatomic,strong) NSNumber *distnceFromMyLocation;
@property (assign, nonatomic) bool has_seat_chart;

//  in list:
@property (nonatomic) BOOL multi_locations;

// type 1
@property (nonatomic,strong) JSLocation *location;

// type 2
@property (nonatomic,strong) NSArray <JSLocation>*locations;

@end
