//
//  ArtistLocationDetailViewController.m
//  Tik8
//
//  Created by Pooya on 1/31/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "ArtistLocationDetailViewController.h"
#import "CircelImageView.h"
#import "DescriptionView.h"
#import "CelebDescriptionView.h"
#import "RelatedEventView.h"
#import "HMSegmentedControl.h"
#import "RelativeEventTableViewCell.h"
#import "EventDetailViewController.h"
#import "GalleryView.h"
#import "IDMPhotoBrowser.h"
#import "HexColors.h"

#define kFacilityWidth 50
#define kFacilitiesViewHeight 80
#define kButWidth 40
#define kButsViewHeight 50

@interface ArtistLocationDetailViewController ()<UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,MDHTMLLabelDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *imgCover;
@property (weak, nonatomic) IBOutlet CircelImageView *imgCircle;
@property (weak, nonatomic) IBOutlet YekanLabel *lbName;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic,strong) DescriptionView *viewDescription;
@property (nonatomic,strong) CelebDescriptionView *viewCelebDescription;
@property (nonatomic,strong) RelatedEventView *viewRealted;
@property (nonatomic,strong) GalleryView *galleryView;

@end

@implementation ArtistLocationDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scrollView.tag = 500;
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
        if(self.celebInfo){
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSCelebrityDetail *celebDeatail = [ServerRequest getCelebrityDetail:self.celebInfo.id];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([celebDeatail.status integerValue] == 1){
                        self.celebInfo = celebDeatail.result;
                        // [self.tableView reloadData];
                        [self initCelebData];
                        [SVProgressHUD dismiss];
                    }else{
                        [TSMessage showNotificationInViewController:self title:celebDeatail.message_fa subtitle:nil type:TSMessageNotificationTypeError];
                    }
                });
            });
        }else{
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSLocationDetail *locationDetail = [ServerRequest getLocationDetail:self.locationInfo.id];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([locationDetail.status integerValue] == 1){
                        self.locationInfo = locationDetail.result;
                        // [self.tableView reloadData];
                        [self initEventData];
                        [SVProgressHUD dismiss];
                    }else{
                        [TSMessage showNotificationInViewController:self title:locationDetail.message_fa subtitle:nil type:TSMessageNotificationTypeError];
                    }
                });
            });
        }
    }else{
        [TSMessage showNotificationInViewController:self title:nil subtitle:@"اینترنت در دسترس نمی باشد" type:TSMessageNotificationTypeWarning];
    }

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_eventTypeId intValue]]];
    [self setNavbarAttr];
    
}

-(void)viewDidLayoutSubviews {
    if (self.segmentedControl.sectionTitles.count == 0) {
        [self initView];
    }
}

-(void)initView{
    [self.view layoutIfNeeded];
    self.segmentedControl.sectionTitles =(self.celebInfo)? @[@"برنامه‌های مرتبط",@"زندگی‌نامه"]: @[@"گالری تصاویر",@"برنامه‌های مرتبط",@"توضیحات"];
    _segmentedControl.titleTextAttributes           = @{NSFontAttributeName : [UIFont fontWithName:FontName size:14],
                                              NSForegroundColorAttributeName : [UIColor grayColor]};
    _segmentedControl.selectedTitleTextAttributes   = @{NSFontAttributeName : [UIFont fontWithName:FontName size:14],
                                              NSForegroundColorAttributeName : [Tik8DataProvider sharedTik8DataProvider].eventColor};
    self.segmentedControl.selectedSegmentIndex = (self.celebInfo ? 1:2);
    self.segmentedControl.backgroundColor = [UIColor whiteColor];
    self.segmentedControl.selectionIndicatorColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    __weak typeof (self) weakself = self;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        [weakself.scrollView scrollRectToVisible:CGRectMake(CGRectGetWidth(weakself.view.frame)*index, 0, CGRectGetWidth(weakself.view.frame), 200) animated:YES];
    }];

    int normalizedWidth = CGRectGetWidth(self.view.frame);

    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.contentSize = CGSizeMake(self.segmentedControl.sectionTitles.count *CGRectGetWidth(self.view.frame) , 200);
    self.scrollView.delegate = self;
    [self.scrollView scrollRectToVisible:CGRectMake(normalizedWidth*((self.celebInfo)?1:2), 0, CGRectGetWidth(self.view.frame), 200) animated:NO];
    
    if(self.celebInfo){
        self.viewCelebDescription = [[CelebDescriptionView alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)*1, 0, normalizedWidth, CGRectGetHeight(self.scrollView.frame))];
        self.viewCelebDescription.lbBiography.delegate = self;
        [self.scrollView addSubview:self.viewCelebDescription];
    }else{
        self.viewDescription = [[DescriptionView alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)*2, 0, normalizedWidth, CGRectGetHeight(self.scrollView.frame))];
        self.viewDescription.lbDescription.delegate = self;
        [self.scrollView addSubview:self.viewDescription];
    }
    
    if(!self.celebInfo){
        self.galleryView = [[GalleryView alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)*0, 0, normalizedWidth, CGRectGetHeight(self.scrollView.frame))];
        self.galleryView.collectionView.delegate = self;
        self.galleryView.collectionView.dataSource = self;
        [self.galleryView.collectionView registerNib:[UINib nibWithNibName:@"GalleryCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"GalleryCollectionCell"];
        [self.scrollView addSubview:self.galleryView];
    }

    self.viewRealted = [[RelatedEventView alloc]initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)*((self.celebInfo)?0:1), 0, normalizedWidth, CGRectGetHeight(self.scrollView.frame))];
    self.viewRealted.tableView.delegate = self;
    self.viewRealted.tableView.dataSource = self;
    self.viewRealted.tableView.rowHeight = 90;
    [self.viewRealted.tableView registerNib:[UINib nibWithNibName:@"RelativeEventTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"RelativeCell"];
    [self.scrollView addSubview:self.viewRealted];
    
    if(self.celebInfo){
        [self initCelebData];
    }else{
        [self initEventData];
    }

    [self.segmentedControl setSelectedSegmentIndex:(self.celebInfo ? 1 : 2) animated:YES]; // show selected correctly
    
}


-(void)initCelebData{
    [self.imgCover sd_setImageWithURL:[NSURL URLWithString:self.celebInfo.profile_image] placeholderImage:[UIImage imageNamed:@"place-holder_back"]];
    [self.imgCircle sd_setImageWithURL:[NSURL URLWithString:self.celebInfo.profile_image] placeholderImage:[UIImage imageNamed:@"place-holder_user"]];
    self.lbName.text = [NSString stringWithFormat:@"%@ %@",self.celebInfo.firstname,self.celebInfo.lastname];
    
    self.viewCelebDescription.lbBiography.htmlText = self.celebInfo.biography;
    self.viewCelebDescription.constraintLbDescreptionheight.constant = [MDHTMLLabel sizeThatFitsHTMLString:self.celebInfo.biography withFont:self.viewCelebDescription.lbBiography.font constraints:self.viewCelebDescription.lbBiography.frame.size limitedToNumberOfLines:0 autoDetectUrls:true];
    self.viewCelebDescription.lbName.text = [NSString stringWithFormat:@"نام: %@",self.celebInfo.firstname];
    self.viewCelebDescription.lbLastname.text = [NSString stringWithFormat:@"نام‌خانوادگی: %@",self.celebInfo.lastname];
    self.viewCelebDescription.lbBirthday.text = @"";//[NSString stringWithFormat:@"تاریخ تولد: %@",self.celebInfo.birthday];
    
    
    bool isEmpty = true;
    if(self.celebInfo.facebook_address.length > 0){
        self.viewCelebDescription.constraintFbWidth.constant = kButWidth;
        [self.viewCelebDescription.butFB addTarget:self action:@selector(fbPressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewCelebDescription.constraintFbWidth.constant = 0;
    }
    if(self.celebInfo.instagram_address.length > 0){
        self.viewCelebDescription.constraintInstaWidth.constant = kButWidth;
        [self.viewCelebDescription.butInstagram addTarget:self action:@selector(instaPressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewCelebDescription.constraintInstaWidth.constant = 0;
    }
    if(self.celebInfo.wiki_address.length > 0){
        self.viewCelebDescription.constraintWikiWidth.constant = kButWidth;
        [self.viewCelebDescription.butWiki addTarget:self action:@selector(wikiPressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewCelebDescription.constraintWikiWidth.constant = 0;
    }
    if(self.celebInfo.twitter_address.length > 0){
        self.viewCelebDescription.constraintTwWidth.constant = kButWidth;
        [self.viewCelebDescription.butTwitter addTarget:self action:@selector(twPressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewCelebDescription.constraintTwWidth.constant = 0;
    }
    if(self.celebInfo.website_address.length > 0){
        self.viewCelebDescription.constraintSiteWidth.constant = kButWidth;
        [self.viewCelebDescription.butSite addTarget:self action:@selector(sitePressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewCelebDescription.constraintSiteWidth.constant = 0;
    }

    if(isEmpty) {
        self.viewCelebDescription.constraintButsViewHeight.constant = 0;
    } else {
        self.viewCelebDescription.constraintButsViewHeight.constant = kButsViewHeight;
    }
    
    [self.viewRealted.tableView reloadData];
}


-(void)initEventData{
    [self.imgCover sd_setImageWithURL:[NSURL URLWithString:self.locationInfo.cover_url] placeholderImage:[UIImage imageNamed:@"place-holder_back"]];
    [self.imgCircle sd_setImageWithURL:[NSURL URLWithString:self.locationInfo.cover_url] placeholderImage:[UIImage imageNamed:@"place-holder_event"]];
    self.lbName.text = self.locationInfo.title;
    
    self.viewDescription.lbDescription.htmlText = self.locationInfo.Description;
    self.viewDescription.constraintLbDescreptionheight.constant = [MDHTMLLabel sizeThatFitsHTMLString:self.locationInfo.Description withFont:self.viewDescription.lbDescription.font constraints:self.viewDescription.lbDescription.frame.size limitedToNumberOfLines:0 autoDetectUrls:true];
    self.viewDescription.lbAddress.text = [NSString stringWithFormat:@"%@\nتلفن: %@", self.locationInfo.address, self.locationInfo.tel];
    self.viewDescription.locationInfo = self.locationInfo;
    [self.viewDescription.butMap addTarget:self action:@selector(mapPressed) forControlEvents:UIControlEventTouchUpInside];
    self.viewDescription.viewCircel.backgroundColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    self.viewDescription.viewCircel2.backgroundColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    self.viewDescription.viewCircel3.backgroundColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    
    // set constraint
    bool isEmpty = true;
    if([self.locationInfo.parking boolValue]){
        self.viewDescription.constraintFacilityParkingWidth.constant = kFacilityWidth;
        isEmpty = false;
    }else{
        self.viewDescription.constraintFacilityParkingWidth.constant = 0;
    }
    if([self.locationInfo.restaurant boolValue]){
        self.viewDescription.constraintFacilityRestWidth.constant = kFacilityWidth;
        isEmpty = false;
    }else{
        self.viewDescription.constraintFacilityRestWidth.constant = 0;
    }
    if([self.locationInfo.internet boolValue]){
        self.viewDescription.constraintFacilityWifiWidth.constant = kFacilityWidth;
        isEmpty = false;
    }else{
        self.viewDescription.constraintFacilityWifiWidth.constant = 0;
    }
    if([self.locationInfo.wheelchair_entrance boolValue]){
        self.viewDescription.constraintFacilityILLWidth.constant = kFacilityWidth;
        isEmpty = false;
    }else{
        self.viewDescription.constraintFacilityILLWidth.constant = 0;
    }
    if([self.locationInfo.coffee_shop boolValue]){
        self.viewDescription.constraintFacilityCoffeeSWidth.constant = kFacilityWidth;
        isEmpty = false;
    }else{
        self.viewDescription.constraintFacilityCoffeeSWidth.constant = 0;
    }
    if([self.locationInfo.vip_section boolValue]){
        self.viewDescription.constraintFacilityVIPWidth.constant = kFacilityWidth;
        isEmpty = false;
    }else{
        self.viewDescription.constraintFacilityVIPWidth.constant = 0;
    }
    if(isEmpty) {
        self.viewDescription.constraintFacilitiesViewHeight.constant = 0;
        self.viewDescription.viewFacilities.hidden = true;
    } else {
        self.viewDescription.constraintFacilitiesViewHeight.constant = kFacilitiesViewHeight;
        self.viewDescription.viewFacilities.hidden = false;
    }
    
    isEmpty = true;
    if(self.locationInfo.facebook_address.length > 0){
        self.viewDescription.constrintFbWidth.constant = kButWidth;
        [self.viewDescription.butFB addTarget:self action:@selector(fbPressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewDescription.constrintFbWidth.constant = 0;
    }
    if(self.locationInfo.instagram_address.length > 0){
        self.viewDescription.constraintInstaWidth.constant = kButWidth;
        [self.viewDescription.butInstagram addTarget:self action:@selector(instaPressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewDescription.constraintInstaWidth.constant = 0;
    }
    if(self.locationInfo.wiki_address.length > 0){
        self.viewDescription.constraintWikiWidth.constant = kButWidth;
        [self.viewDescription.butWiki addTarget:self action:@selector(wikiPressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewDescription.constraintWikiWidth.constant = 0;
    }
    if(self.locationInfo.twitter_address.length > 0){
        self.viewDescription.constraintTwWidth.constant = kButWidth;
        [self.viewDescription.butTwitter addTarget:self action:@selector(twPressed) forControlEvents:UIControlEventTouchUpInside];
        isEmpty = false;
    }else{
        self.viewDescription.constraintTwWidth.constant = 0;
    }
    if(isEmpty) {
        self.viewDescription.constraintButsViewHeight.constant = 0;
    } else {
        self.viewDescription.constraintButsViewHeight.constant = kButsViewHeight;
    }
    
    
    [self.viewRealted.tableView reloadData];
    [self.galleryView.collectionView reloadData];
}

-(void)fbPressed{
    if(self.celebInfo){
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.celebInfo.facebook_address]];
    }else{
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.locationInfo.facebook_address]];
    }
}

-(void)instaPressed{
    if(self.celebInfo){
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.celebInfo.instagram_address]];
    }else{
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.locationInfo.instagram_address]];
    }
}

-(void)wikiPressed{
    if(self.celebInfo){
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.celebInfo.wiki_address]];
    }else{
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.locationInfo.wiki_address]];
    }
}
-(void)twPressed{
    if(self.celebInfo){
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.celebInfo.twitter_address]];
    }else{
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.locationInfo.twitter_address]];
    }
}
-(void)sitePressed{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:self.celebInfo.website_address]];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if(scrollView.tag == 500){
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x/pageWidth;
        
        [self.segmentedControl setSelectedSegmentIndex:page animated:YES];
    }
}

-(void)mapPressed{
    [self performSegueWithIdentifier:@"go to map" sender:self.locationInfo];
}


#pragma mark - htmllabel delegate
-(void)HTMLLabel:(MDHTMLLabel *)label didSelectLinkWithURL:(NSURL *)URL{
    [[UIApplication sharedApplication]openURL:URL];
}

#pragma mark - tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.celebInfo)
        return self.celebInfo.related_events.count;
    return self.locationInfo.related_events.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RelativeEventTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"RelativeCell" forIndexPath:indexPath];
    JSRelativeEvent *relativeEvent = (self.celebInfo)?[self.celebInfo.related_events objectAtIndex:indexPath.row]:[self.locationInfo.related_events objectAtIndex:indexPath.row];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:relativeEvent.image_url] placeholderImage:[UIImage imageNamed:@"place-holder_event"]];
    cell.lbName.text = relativeEvent.title;
    if(self.celebInfo) {
        cell.lbName.textColor = [UIColor hx_colorWithHexString: [[Tik8DataProvider sharedTik8DataProvider] getEventType:[relativeEvent.type intValue]].color];
    } else {
        cell.lbName.textColor = [UIColor hx_colorWithHexString: [[Tik8DataProvider sharedTik8DataProvider] getEventType:[relativeEvent.type_id intValue]].color];
    }
    cell.lbDate.text = relativeEvent.date;
    cell.butBuy.tag = indexPath.row;
    cell.butBuy.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.butBuy.layer.borderWidth = 0.5f;
    cell.butBuy.layer.cornerRadius = 4.f;
    cell.lbLocation.text = relativeEvent.location_title;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JSRelativeEvent *relativeEvent = (self.celebInfo)?[self.celebInfo.related_events objectAtIndex:indexPath.row]:[self.locationInfo.related_events objectAtIndex:indexPath.row];
    JSEvent *event = [[JSEvent alloc]init];
    event.id = relativeEvent.id;
    event.image_url = relativeEvent.image_url;
    if(self.celebInfo) {
        event.type_id = relativeEvent.type;
    } else {
        event.type_id = relativeEvent.type_id;
    }
    event.location_title = relativeEvent.location_title;
    event.title = relativeEvent.title;

    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [relativeEvent.type_id intValue]]];
    [self setNavbarAttr];

    [self  performSegueWithIdentifier:@"go to event detail" sender:event];
}

#pragma mark - collectionView

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.locationInfo.galleries.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"GalleryCollectionCell";
    
    static BOOL nibMyCellloaded = NO;
    
    if(!nibMyCellloaded)
    {
        UINib *nib = [UINib nibWithNibName:@"GalleryCell" bundle: nil];
        [collectionView registerNib:nib forCellWithReuseIdentifier:identifier];
        nibMyCellloaded = YES;
    }
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *imgView = (UIImageView *)[cell.contentView viewWithTag:100];
    NSString *url = [self.locationInfo.galleries objectAtIndex:indexPath.row];
    [imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"place-holder_back"]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    IDMPhotoBrowser *photoBrowser = [[IDMPhotoBrowser alloc]initWithPhotoURLs:self.locationInfo.galleries];
    [photoBrowser setInitialPageIndex:indexPath.row];
    [self presentViewController:photoBrowser animated:YES completion:^{
        
    }];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"go to map"])
    {
        [segue.destinationViewController setLocationInfo:sender];
    }
    else if([segue.identifier isEqualToString:@"go to event detail"])
    {
        [segue.destinationViewController setEvent:sender];
        
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}


@end
