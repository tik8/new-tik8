//
//  EventLocationTableViewCell.m
//  Tik8
//
//  Created by Pooya on 1/26/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "EventLocationTableViewCell.h"
#import "ObjectAnnotation.h"
@implementation EventLocationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setLocationInfo:(JSLocation *)locationInfo{
    _locationInfo = locationInfo;
    [self loadPinOnMap];
}


-(void)loadPinOnMap {
    [self.mapView removeAnnotations:[self.mapView annotations]];
    [self zoomToMerchantLocation];
    
    MKCoordinateRegion region1 = { { 0.0, 0.0 }, { 0.0, 0.0 } };
    region1.center.latitude = [self.locationInfo.latitude floatValue];
    region1.center.longitude = [self.locationInfo.longitude floatValue];
    region1.span.longitudeDelta = 0.01f;
    region1.span.latitudeDelta = 0.01f;
    
    ObjectAnnotation *ann=[[ObjectAnnotation alloc]init];
    
    ann.title = self.locationInfo.title;
    ann.subtitle = self.locationInfo.address;
    ann.coordinate = region1.center;
    ann.color = [UIColor grayColor];
    
    [self.mapView addAnnotation:ann];
}

-(void)zoomToMerchantLocation
{
    /*Region and Zoom*/
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta=0.007;
    span.longitudeDelta=0.007;
    
    CLLocationCoordinate2D location=_mapView.userLocation.coordinate;
    
    location.latitude=[self.locationInfo.latitude floatValue];
    location.longitude=[self.locationInfo.longitude floatValue];
    region.span=span;
    region.center=location;
    
    [self.mapView setRegion:region animated:TRUE];
    [self.mapView regionThatFits:region];
}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if(![annotation isKindOfClass:[ObjectAnnotation class]]) // Don't mess user location
        return nil;
    
    static NSString *defaultPinID = @"StandardIdentifier";
    MKAnnotationView *pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if (pinView == nil){
        pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        pinView.canShowCallout = NO;
        //[pinView setImage:[UIImage imageNamed:@"map-pin"]];
        
    }
    pinView.annotation = annotation;
    
    
    return pinView;
}


@end
