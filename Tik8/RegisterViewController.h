//
//  RegisterViewController.h
//  Netbarg
//
//  Created by Pooya on 1/24/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *textFieldName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *textFieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPassword;
@property (strong, nonatomic) IBOutlet UITextField *textFieldPhone;

- (IBAction)signupPressed;
@property (strong, nonatomic) IBOutlet YekanButton *butSignup;
@end
