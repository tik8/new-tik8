//
//  GalleryTableViewCell.h
//  Tik8
//
//  Created by Pooya on 5/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircelView.h"

@interface GalleryTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet CircelView *circelView;
@property (strong, nonatomic) IBOutlet YekanLabel *lbTitle;
@property (strong, nonatomic) IBOutlet UIView *viewLine;
@property (strong, nonatomic) IBOutlet UIImageView *imgBullet;

-(void)setCollectionViewDataSourceDelegate:(id<UICollectionViewDelegate,UICollectionViewDataSource>)dataSourceDelegate index:(NSInteger)index;

@end
