//
//  JSPriceCategory.h
//  Tik8
//
//  Created by Sina on 10/28/15.
//  Copyright (c) 2015 Sina. All rights reserved.
//

#import "JSONModel.h"
#import "JSPriceCategoryInner.h"
#import "JSPriceCategory.h"

@protocol JSPriceCategory @end

@interface JSPriceCategory : JSONModel

@property (nonatomic, strong) NSString* id;
@property (nonatomic,strong) NSString *interval_id;
@property (nonatomic,strong) NSString *price_category_id;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *created;
@property (nonatomic,strong) NSString *modified;
@property (nonatomic,strong) JSPriceCategoryInner *PriceCategory;

@property (nonatomic) int num;

@end
