//
//  JSPositions.h
//  Tik8
//
//  Created by Pooya on 2/9/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSPosition.h"
@interface JSPositions : JSStatus

@property (nonatomic,strong) NSString *count;
@property (nonatomic,strong) NSArray <JSPosition>*result;

@end
