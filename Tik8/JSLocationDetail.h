//
//  JSLocationDetail.h
//  Tik8
//
//  Created by Pooya on 1/31/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"

@interface JSLocationDetail : JSStatus

@property (nonatomic,strong) JSLocation *result;
@end
