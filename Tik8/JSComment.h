//
//  JSComment.h
//  Tik8
//
//  Created by Pooya on 2/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@protocol JSComment
@end

@interface JSComment : JSONModel

@property (nonatomic,strong) NSString *comment;
@property (nonatomic,strong) NSString *comment_date;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *avatar;
@property (nonatomic,strong) NSString *answer;
@property (nonatomic,strong) NSString *answer_date;

@end
