//
//  SubcribeViewController.m
//  Tik8
//
//  Created by Pooya on 3/14/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "SubcribeViewController.h"

@interface SubcribeViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *tfEmail;
@property (strong, nonatomic) IBOutlet UITextField *tfCity;
@property (strong, nonatomic) IBOutlet YekanButton *butSubscibe;

@property (nonatomic,strong) NSString *cityID;

- (IBAction)subscribePressed:(id)sender;
@end

@implementation SubcribeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];

    if(self.navigationController.viewControllers.count == 1){
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"بستن" style:UIBarButtonItemStylePlain target:self action:@selector(closePressed)];
    }
    
    self.tfCity.delegate = self;
    self.tfEmail.delegate = self;
    
    UIPickerView *pickerView  = [[UIPickerView alloc]init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    
    UIToolbar *toolbar = [[UIToolbar alloc]init];
    [toolbar sizeToFit];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"انتخاب" style:UIBarButtonItemStyleDone target:self action:@selector(handleActionBarDone:)];
    
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects: flexible, doneButton, nil]];
    
    
    self.tfCity.inputView = pickerView;
    self.tfCity.inputAccessoryView = toolbar;
    // Do any additional setup after loading the view.
}

-(void)closePressed{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (void)handleActionBarDone:(UIBarButtonItem *)doneButtonItem
{
    [self.tfCity resignFirstResponder];
}

#pragma mark - pickerview delegate

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    NSArray *cities = [Tik8DataProvider sharedTik8DataProvider].cities;
    if([self.cityID length] == 0 && cities.count>0) {
        JSCity *city = [cities objectAtIndex:0];
        self.cityID = city.id;
        self.tfCity.text = city.name;
    }
    return cities.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    JSCity *city = [[Tik8DataProvider sharedTik8DataProvider].cities objectAtIndex:row];
    return city.name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)componen{
    JSCity *city = [[Tik8DataProvider sharedTik8DataProvider].cities objectAtIndex:row];
    self.cityID = city.id;
    self.tfCity.text = city.name;
}

- (IBAction)subscribePressed:(id)sender {
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        if(self.tfEmail.text.length > 0 && self.tfCity.text.length > 0){
            [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    JSStatus *status = [ServerRequest subscribeInEvents:self.tfEmail.text inCity:self.cityID];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        [TSMessage showNotificationInViewController:self.parentViewController title:status.message subtitle:nil type:([status.status integerValue] == 1)?TSMessageNotificationTypeSuccess:TSMessageNotificationTypeError];
                        if([status.status integerValue] == 1){
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                    });
                });
        }else{
            [TSMessage showNotificationInViewController:self.parentViewController title:@"لطفا تمامی فیلدها را پر نمایید" subtitle:nil type:TSMessageNotificationTypeWarning];
            
        }
        
    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت خود را چک کنید" subtitle:nil type:TSMessageNotificationTypeWarning];
        
    }
}
@end
