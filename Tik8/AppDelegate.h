//
//  AppDelegate.h
//  Tik8
//
//  Created by Pooya on 1/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JASidePanelController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) JASidePanelController *viewController;

- (void) updateQuickActionItems;

@end

