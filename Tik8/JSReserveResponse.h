//
//  JSReserveResponse.h
//  Tik8
//
//  Created by Pooya on 5/4/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSReserve.h"

@interface JSReserveResponse : JSStatus

@property (nonatomic,strong) JSReserve *result;

@end
