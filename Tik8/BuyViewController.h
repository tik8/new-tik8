//
//  BuyViewController.h
//  Tik8
//
//  Created by Pooya on 5/6/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *bankSaman;
@property (weak, nonatomic) IBOutlet UIButton *bankMellat;
@property (weak, nonatomic) IBOutlet UIButton *bankParsian;

@property (weak, nonatomic) IBOutlet YekanButton *btnCampaignCode;

- (IBAction)bankPressed:(id)sender;
- (IBAction)buyPressed:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButMellatWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButSamanWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButParsianWidth;

@property (nonatomic,strong) JSInterval *interval;
@property (nonatomic,strong) JSEvent *eventInfo;
@property (nonatomic,strong) JSReserve *reserve;
@property (strong, nonatomic) NSMutableArray<NSDictionary*> *cats;

@end
