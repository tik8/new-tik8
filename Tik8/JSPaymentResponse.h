//
//  JSPaymentResponse.h
//  Tik8
//
//  Created by Pooya on 4/8/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"

@interface JSPaymentResponse : JSStatus

@property (nonatomic,strong) NSString *payUrl;

@end
