//
//  RelativeEventTableViewCell.h
//  Tik8
//
//  Created by Pooya on 1/31/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RelativeEventTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet YekanLabel *lbName;
@property (strong, nonatomic) IBOutlet YekanLabel *lbDate;
@property (strong, nonatomic) IBOutlet YekanLabel *lbLocation;
@property (strong, nonatomic) IBOutlet YekanButton *butBuy;
@property (strong, nonatomic) IBOutlet YekanLabel *lbDistance;

@end
