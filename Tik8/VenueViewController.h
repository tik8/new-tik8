//
//  VenueViewController.h
//  Tik8
//
//  Created by Pooya on 4/11/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueViewController : UIViewController

@property (nonatomic,strong) JSInterval *interval;
@property (nonatomic,strong) JSEvent *eventInfo;
@property (nonatomic, strong) JSVenue *venue;

@end
