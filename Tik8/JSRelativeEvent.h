//
//  JSRelativeEvent.h
//  Tik8
//
//  Created by Pooya on 1/31/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@protocol JSRelativeEvent
@end

@interface JSRelativeEvent : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *type;
@property (nonatomic,strong) NSString *type_id;
@property (nonatomic,strong) NSString *image_url;
@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSString *location_title;



@end
