//
//  EventDetailViewController.h
//  Tik8
//
//  Created by Pooya on 1/26/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailViewController : UIViewController

@property(nonatomic,strong) JSEvent *event;

-(void) locationSelected: (int) itemIndex;

@end
