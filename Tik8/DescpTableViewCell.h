//
//  DescpTableViewCell.h
//  Tik8
//
//  Created by Pooya on 2/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDHTMLLabel.h"
#import "CircelView.h"
@interface DescpTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MDHTMLLabel *lbDescp;
@property (strong, nonatomic) IBOutlet YekanLabel *lbMore;
@property (strong, nonatomic) IBOutlet YekanLabel *lbTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgBullet;

@end
