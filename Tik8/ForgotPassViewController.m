//
//  ForgotPassViewController.m
//  Netbarg
//
//  Created by Pooya on 3/6/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import "ForgotPassViewController.h"
@interface ForgotPassViewController ()<UITextFieldDelegate>

@end

@implementation ForgotPassViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.butForgot.layer.cornerRadius = 4.f;

    self.tfEmail.delegate = self;
    self.tfEmail.returnKeyType = UIReturnKeySend;

    
    
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
        [self.tfEmail becomeFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self sendPRessed];
    return YES;
}

- (IBAction)sendPRessed {
    if(self.tfEmail.text.length >0){
        if([[MyReachability reachabilityForInternetConnection] isReachable]){

            [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                [SVProgressHUD dismiss];
                JSStatus *result = [ServerRequest forgotPassEmail:self.tfEmail.text];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([result.status integerValue]==1){
                         [TSMessage showNotificationInViewController:self.parentViewController title:@"درخواست شما ارسال شد" subtitle:@"لطفا برای تغییر کلمه عبور، برروی آدرسی که به پست الکترونیکی شما ارسال شده است، کلیک نمایید" type:TSMessageNotificationTypeSuccess];
                    }else{
                         [TSMessage showNotificationInViewController:self.parentViewController title:@"ایمیل شما یافت نشد" subtitle:nil type:TSMessageNotificationTypeError];
                    }
                });
            });
        }else{
             [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
        }
    }else{
         [TSMessage showNotificationInViewController:self.parentViewController title:@"ایمیل خود را وارد کنید" subtitle:nil type:TSMessageNotificationTypeWarning];
    }
    
}
@end
