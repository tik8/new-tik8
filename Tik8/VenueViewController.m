//
//  VenueViewController.m
//  Tik8
//
//  Created by Pooya on 4/11/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "VenueViewController.h"
#import "SeatViewController.h"
#import "SeatTableViewCell.h"
#import "BuyViewController.h"
@interface VenueViewController ()<UIWebViewDelegate,SeatViewControllerDelegate,UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) JSReserve *seats;

- (IBAction)buyPressed:(UIBarButtonItem *)sender;
- (void) updateUserInfo;
@end

@implementation VenueViewController

- (void)viewWillAppear:(BOOL)animated {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"خرید رویداد-  %@: %@, سانس: %@",self.eventInfo.id, self.eventInfo.title, self.interval.id]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_eventInfo.type_id intValue]]];
    [self setNavbarAttr];
}

-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.webView.delegate = self;
    //self.webView.opaque = YES;
    self.tableView.delegate =self;
    self.tableView.dataSource =self;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSVenueDetail *detail = [ServerRequest getEventvenueForInterval:self.interval.id];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(detail){
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        JSReserveResponse *reserve = [ServerRequest getReserveForInterval:self.interval.id];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if([reserve.status integerValue] == 1){
                                if(reserve.result.seats.count > 0){
                                    self.seats = reserve.result;
                                }
                                [self.tableView reloadData];
                            }
                        });
                    });
                    [[NSURLCache sharedURLCache] removeAllCachedResponses];
                    [self.webView loadHTMLString:detail.venue.parsedHtml baseURL:nil];
                    [self updateUserInfo];
                }else{
                    [SVProgressHUD dismiss];
                    [TSMessage showNotificationInViewController:self title:@"خطا" subtitle:nil type:TSMessageNotificationTypeError];
                    
                }
                
            });
        });
    }else{
        
    }
    // Do any additional setup after loading the view.
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"load req ===> %@",request.URL.host);
    if([request.URL.scheme isEqualToString:@"tik8"]){
        if([[MyReachability reachabilityForInternetConnection] isReachable]){
            [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSVenue *venue = [ServerRequest getEventvenueSeat:request.URL.host];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if(venue.parsedHtml){
                        SeatViewController *seatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"seatVC"];
                        seatVC.htmlPage = venue.parsedHtml;
                        seatVC.delegate = self;
                        seatVC.beforeReservedSeats = [venue.reservedSeats mutableCopy];
                        seatVC.hasOtherSeats = _seats.seats.count > seatVC.beforeReservedSeats.count ? true : false;
                        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:seatVC] animated:YES completion:nil];
                    }
                });
            });
        }else{
            
        }
    }
    return YES;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
    webView.scrollView.delegate = self; // set delegate method of UISrollView
    webView.scrollView.maximumZoomScale = 5;
}

#pragma mark -
#pragma mark - UIScrollView Delegate Methods

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale
{
    self.webView.scrollView.maximumZoomScale = 5;
}

-(void)seatViewController:(SeatViewController *)sender seatsReserved:(JSReserve *)seats{
    [self dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"REserved: => %@",seats);
    self.seats = seats;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.seats.seats.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SeatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SeatCell" forIndexPath:indexPath];
    
    JSSeat *seat = [self.seats.seats objectAtIndex:indexPath.row];
    cell.lbPrice.text = seat.price;
    cell.lbVenue.text = seat.zone;
    cell.lbSeatNi.text = seat.snumber;
    cell.lbRow.text = seat.row_number;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    JSSeat *seat =[self.seats.seats objectAtIndex:indexPath.row];
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        [SVProgressHUD showWithStatus:@"در حال حذف..."];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSStatus *status = [ServerRequest cancelSeat:seat.id];
            JSReserveResponse *reserve = [ServerRequest getReserveForInterval:self.interval.id];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                if([status.status integerValue] == 1){
                    NSMutableArray *array = [self.seats.seats mutableCopy];
                    [array removeObjectAtIndex:indexPath.row];
                    self.seats.seats = [array copy];
                    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                    if([reserve.status integerValue] == 1){
                        if(reserve.result.seats.count > 0){
                            self.seats = reserve.result;
                        }
                    }
                }else{
                    [TSMessage showNotificationInViewController:self title:status.message subtitle:nil type:TSMessageNotificationTypeError];
                    
                }
            });
        });
    }else{
        
    }
    
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"go to buy"]) {
        BuyViewController *buyVc = segue.destinationViewController;
        buyVc.interval = self.interval;
        buyVc.eventInfo = self.eventInfo;
        buyVc.reserve = self.seats;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)buyPressed:(UIBarButtonItem *)sender {
    if(self.seats.seats.count > 0){
        [self performSegueWithIdentifier:@"go to buy" sender:self];
    }else{
        [TSMessage showNotificationInViewController:self title:@"شما صندلی ای رزرو نکرده اید" subtitle:nil type:TSMessageNotificationTypeWarning];
        
    }
}

- (void) updateUserInfo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ServerRequest updateUserInfo];
        dispatch_async(dispatch_get_main_queue(), ^{

        });
    });
}

@end
