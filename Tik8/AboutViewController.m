//
//  AboutViewController.m
//  Tik8
//
//  Created by Pooya on 5/30/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "AboutViewController.h"
#import "HexColors.h"

#define cOrange [UIColor hx_colorWithHexString:@"#ec762f"]

@interface AboutViewController ()
@property (strong, nonatomic) IBOutlet YekanLabel *lbAbout;
@property (strong, nonatomic) IBOutlet UIImageView *img;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.img.layer.cornerRadius = 25.f;
    self.img.layer.masksToBounds = YES;

    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    self.lbAbout.text = [NSString stringWithFormat:@"اپلیکیشن تیکت نسخه: %@",[Utils persianDigits: appVersionString]];
    [self.navigationController.navigationBar setBarTintColor:cOrange];

    _img.userInteractionEnabled = YES;
    UITapGestureRecognizer *uiTapGestureRecognizer =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(toSite:)];
    [_img addGestureRecognizer:uiTapGestureRecognizer];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) toSite: (UITapGestureRecognizer *) recognizer {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.tik8.com"]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
