//
//  JSCelebrityDetail.h
//  Tik8
//
//  Created by Pooya on 2/2/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"

@interface JSCelebrityDetail : JSStatus
@property (nonatomic,strong) JSCelebrity *result;

@end
