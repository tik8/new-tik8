//
//  JSMyTicketDetail.h
//  Tik8
//
//  Created by Pooya on 2/25/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSMyTicketsAndDate.h"
@interface JSMyTicketDetail : JSStatus

@property (nonatomic,strong) NSString *count;
@property (nonatomic,strong) NSArray <JSMyTicketsAndDate>*result;

@end
