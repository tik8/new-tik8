//
//  Utils.h
//  Tik8
//
//  Created by Sina KH on 7/17/16.
//  Copyright © 2016 Pooya. All rights reserved.
//

@interface Utils : NSObject 

+(NSString*) persianDigits: (NSString*) string;

+(NSString *)formatedNumber:(double)value;

@end
