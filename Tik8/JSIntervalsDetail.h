//
//  JSIntervalsDetail.h
//  Tik8
//
//  Created by Pooya on 3/7/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSInterval.h"
@interface JSIntervalsDetail : JSStatus

@property (nonatomic,strong) NSMutableArray <JSInterval>*result;

@end
