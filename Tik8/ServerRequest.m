//
//  ServerRequest.m
//  Netbarg
//
//  Created by Pooya on 1/6/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import "ServerRequest.h"

#define BASE_URL @"http://tik8.com/api/v1.0/"
//#define BASE_URL @"http://develop.tik8.com/api/v1.0/"
//#define API_KEY @"092d6a493747dab03061b704dc4a28cf"


@implementation ServerRequest

+(NSData *)sendRequestWithURL:(NSString *)url httpMethod:(NSString *)method httpBody:(NSData *)body security:(BOOL)security {
    NSString *encodeUrl = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)url, NULL, CFSTR(" "), kCFStringEncodingUTF8));
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_URL,encodeUrl]]];
    NSLog(@"Request ====> [%@]",request);
    [request addValue:@"iOS" forHTTPHeaderField:@"platform"]; // platform
    //[request setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    if([method isEqualToString:@"POST"])
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    if(security){
        NSString *token = [Tik8DataProvider sharedTik8DataProvider].userToken;
        [request addValue:token forHTTPHeaderField:@"X-Auth-Token"];
    }

    request.HTTPMethod = method;
    if(body) request.HTTPBody = body;
    NSURLResponse *response = nil;
    NSError *error;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if(error){
        NSLog(@"[ERROR]=>[%@] => [%@]",url,error.localizedDescription);
        return nil;
    }
    else
        return data;
}

+(JSEventList *)getEventsList:(NSString *)title page:(NSInteger)page{
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    NSString *url = [NSString stringWithFormat:@"events/events/lists/%@/iOS/%@?page=%ld",title, version, (long)page];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSEventList alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSEventDetail *)getTicketDetail:(NSString *)ticketID{
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
    NSString *url = [NSString stringWithFormat:@"events/events/info/%@/iOS/%@",ticketID, version];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
      //  NSString *string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSEventDetail alloc]initWithDictionary:resultDict error:nil];

}
    return nil;
}

+(JSLocationDetail *)getLocationDetail:(NSString *)locationID{
    NSString *url = [NSString stringWithFormat:@"locations/locations/info/%@",locationID];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
      //  NSString *string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];

        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSLocationDetail alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSCelebrityDetail *)getCelebrityDetail:(NSString *)celebID{
    NSString *url = [NSString stringWithFormat:@"celebrities/celebrities/info/%@",celebID];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSCelebrityDetail alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSLogin *)loginWithUsername:(NSString *)username password:(NSString *)password{
    NSString *url = @"users/users/login";
    NSString *bodystring = [NSString stringWithFormat:@"email=%@&password=%@",username,password];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSLogin alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSStatus *)registerWithUsername:(NSString *)username password:(NSString *)password firstname:(NSString *)firstname lastname:(NSString *)lastname phone:(NSString *)phone{
    NSString *url = @"users/users/register";
    NSString *bodystring = [NSString stringWithFormat:@"email=%@&password=%@&firstname=%@&lastname=%@&phone=%@",username,password,firstname,lastname,phone];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSStatus alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSStatus *)forgotPassEmail:(NSString *)email{
    NSString *url = @"users/users/forgotpassword";
    NSString *bodystring = [NSString stringWithFormat:@"email=%@",email];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSStatus alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSPositions *)getAllPositions{
    NSString *url = @"events/events/positions/all";
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSPositions alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSNearby *)getNearbyWithLat:(double)lat long:(double)lng{
    NSString *url = [NSString stringWithFormat:@"events/events/nearBy?lat=%f&lng=%f",lat,lng];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSNearby alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSStatus *)editProfileWithName:(NSString *)firstname lastname:(NSString *)lastname phone:(NSString *)phone{
    NSString *url = @"users/users/edit";
    NSString *bodystring = [NSString stringWithFormat:@"firstname=%@&lastname=%@&phone=%@",firstname,lastname,phone];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSStatus alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSLogin *)updateUserInfo{
    NSString *url = @"users/users/info";
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        JSLogin *login =  [[JSLogin alloc]initWithDictionary:resultDict error:nil];
        if([login.status intValue] == 1 && login.info){
            [Tik8DataProvider sharedTik8DataProvider].userInfo = login.info;
        } else {
            [[Tik8DataProvider sharedTik8DataProvider]userSignOut];
        }
        return login;
    }
    return nil;
}

+(JSComments *)getEventComments:(NSString *)eventID{
    NSString *url = [NSString stringWithFormat:@"comments/comments/event/%@",eventID];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSComments alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSStatus *)addCommentToEvent:(NSString *)eventID comment:(NSString *)comment{
    NSString *url = [NSString stringWithFormat:@"comments/comments/add/%@",eventID];
    NSString *bodystring = [NSString stringWithFormat:@"body=%@",comment];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];

    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSStatus alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSMyTicketDetail *)getMyTicket{
    NSString *url = @"users/user_orders/lists";
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSMyTicketDetail alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSMyTicketInfo *)getMyTicketInfo:(NSString *)ticketID{
    NSString *url = [NSString stringWithFormat: @"users/user_tickets/info/%@",ticketID ];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSMyTicketInfo alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSIntervalsDetail *)getEventIntervals:(NSString *)eventID{
    NSString *url = [NSString stringWithFormat: @"events/events/intervals/%@/1",eventID ];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        JSIntervalsDetail *intervals = [[JSIntervalsDetail alloc]initWithDictionary:resultDict error:nil];
        int i=0;
        while(i<intervals.result.count) {
            if([((JSInterval *) intervals.result[i]).status intValue]==1) {
                i++;
            } else {
                [intervals.result removeObjectAtIndex:i];
            }
        }
        return intervals;
    }
    return nil;
}

+(JSStatus *)subscribeInEvents: (NSString *)email inCity:(NSString *)cituID{
    NSString *url = @"users/user_subscriptions/subscribe";
    NSString *bodystring = [NSString stringWithFormat:@"email=%@&cityId=%@",email,cituID];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSStatus alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSPaymentResponse *)addToWallet:(NSString *)amount gateway:(NSString *)gateway{
    NSString *url = @"payment/buy";
    NSString *bodystring = [NSString stringWithFormat:@"gateway=%@&totalPrice=%@&type=1",gateway,amount];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
     //   NSString *string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];

        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSPaymentResponse alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSPaymentResponse *)buyTickets:(NSString *)indervalID gateway:(NSString *)gateway campaignCode: (NSString *) campaignCode {
    NSString *url = @"payment/buy";
    NSString *bodystring = [NSString stringWithFormat:@"gateway=%@&intervalId=%@",gateway,indervalID];
    if (campaignCode != nil) {
        bodystring = [bodystring stringByAppendingString: [NSString stringWithFormat:@"&campaign_code=%@",campaignCode]];
    }
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSPaymentResponse alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}
+(JSPaymentResponse *)buyTicketsByCat:(NSString *)indervalID gateway:(NSString *)gateway cats: (NSMutableArray *) cats campaignCode: (NSString *) campaignCode {
    NSString *url = @"payment/buy";
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:cats
                                                       options:(NSJSONWritingOptions) 0
                                                         error:&error];
    
    NSString* catsJSON;
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        return nil;
    } else {
        catsJSON = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    NSString *bodystring = [NSString stringWithFormat:@"gateway=%@&intervalId=%@&cats=%@", gateway, indervalID, catsJSON];
    if (campaignCode != nil) {
        bodystring = [bodystring stringByAppendingString: [NSString stringWithFormat:@"&campaign_code=%@",campaignCode]];
    }
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSPaymentResponse alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSVenueDetail *)getEventvenueForInterval:(NSString *)intervalId{
    NSString *url = [NSString stringWithFormat:@"events/events/venue/%@",intervalId];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
       // NSString *string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        if([[resultDict valueForKey:@"status"] integerValue]==1){
            JSVenueDetail *venue = [[JSVenueDetail alloc ]initWithDictionary:[resultDict valueForKey:@"result"] error:nil ];
            return venue;
        }

    }
    return nil;
}

+(JSVenue *)getEventvenueSeat:(NSString *)venueID{
    NSString *url = [NSString stringWithFormat:@"locations/venue_seats/showSeats/%@",venueID];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:YES];
    if(data){
       // return string;
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
       // NSString *string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];

        if([[resultDict valueForKey:@"status"] integerValue]==1){
            JSVenue *venue = [[JSVenue alloc ]initWithDictionary:[resultDict valueForKey:@"result"] error:nil ];
            return venue;
        }
        
    }
    return nil;
}

+(JSReserveResponse *)reserveSeats:(NSString *)seats deletedSeats:(NSString *)deletedSeats{
    NSString *url = @"users/users_seats/reserve";
    NSString *bodystring = [NSString stringWithFormat:@"seats=%@&deletedSeats=%@",seats,deletedSeats];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        //NSString *string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            JSReserveResponse *venue = [[JSReserveResponse alloc ]initWithDictionary:resultDict error:nil];
            return venue;
        
        
    }
    return nil;
}

+(JSReserveResponse *)getReserveForInterval:(NSString *)interval{
    NSString *url = [NSString stringWithFormat:@"users/users_seats/reserve/%@",interval];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
    //    NSString *string = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        JSReserveResponse *venue = [[JSReserveResponse alloc ]initWithDictionary:resultDict error:nil];
        return venue;
        
        
    }
    return nil;
}

+(JSStatus *)cancelSeat:(NSString *)seatID{
    NSString *url = @"users/users_seats/cancel";
    NSString *bodystring = [NSString stringWithFormat:@"seats=%@",seatID];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSStatus alloc]initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(void )updateGatwaysAvaliable{
    NSString *url = @"payment/gateways";
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        if([[resultDict valueForKey:@"status"] integerValue]==1){
            NSArray *gateways = [JSGateway arrayOfModelsFromDictionaries:[resultDict valueForKey:@"result"]];
            if(gateways.count > 0) {
                [Tik8DataProvider sharedTik8DataProvider].gateways = gateways;
                NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:gateways];
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:encodedObject forKey:@"gateways"];
                [defaults synchronize];
            }
        }
    }
}

+(void )updateCity{
    NSString *url = @"settings/cities/all";
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:NO];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        if([[resultDict valueForKey:@"status"] integerValue]==1){
            NSArray *cities = [JSCity arrayOfModelsFromDictionaries:[resultDict valueForKey:@"result"]];
            if(cities.count > 0)
                [Tik8DataProvider sharedTik8DataProvider].cities = cities;
        }
    }
}

+(BOOL)checkEventTypes {
    if([Tik8DataProvider sharedTik8DataProvider].eventTypesLoaded) {
        return true;
    }
    NSString *url = @"settings/types/all";
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:nil security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        if([[resultDict valueForKey:@"status"] integerValue]==1){
            [[Tik8DataProvider sharedTik8DataProvider] setEventTypes: [JSEventType arrayOfModelsFromDictionaries:[resultDict valueForKey:@"result"]]];
            [Tik8DataProvider sharedTik8DataProvider].eventTypesLoaded = YES;
            NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:[Tik8DataProvider sharedTik8DataProvider].eventTypes];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:encodedObject forKey:@"eventTypes"];
            [defaults synchronize];
            return true;
        }
    }
    return false;
}

+(int)isUpdateAvailable {
    NSString *url = @"users/users/version";
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:nil security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        if([[resultDict valueForKey:@"status"] integerValue]==1){
            NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
            if([[resultDict valueForKey:@"ios_force_version"] integerValue]>[version integerValue]) {
                return 2;
            }
            if([[resultDict valueForKey:@"ios_version"] integerValue]>[version integerValue]) {
                return 1;
            }
            return 0;
        }
    }
    return 0;
}


+(JSIntervalPriceCatsResponse *)getIntervalPriceCategory:(NSString*) eventId {
    NSString *url = [NSString stringWithFormat: @"events/events/intervalPriceCategories/%@", eventId ];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"GET" httpBody:nil security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        if([[resultDict valueForKey:@"status"] integerValue]==1){
            return [[JSIntervalPriceCatsResponse alloc] initWithDictionary:resultDict error:nil];
        }
    }
    return nil;
}

+(JSStatus *)useGiftCard:(NSString *)code {
    NSString *url = @"users/users/addToWalletWithCode";
    NSString *bodystring = [NSString stringWithFormat:@"code=%@",code];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSStatus alloc] initWithDictionary:resultDict error:nil];
    }
    return nil;
}

+(JSCampaignCodeStatus *) checkCampaignCode: (NSString *) code intervalId: (NSString *) intervalId {
    NSString *url = @"users/users/checkCodeOfCampaign";
    NSString *bodystring = [NSString stringWithFormat:@"code=%@&intervalId=%@", code, intervalId];
    NSData *body = [bodystring dataUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [self sendRequestWithURL:url httpMethod:@"POST" httpBody:body security:YES];
    if(data){
        NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil ];
        return [[JSCampaignCodeStatus alloc] initWithDictionary:resultDict error:nil];
    }
    return nil;
}

@end
