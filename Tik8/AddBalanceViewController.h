//
//  AddBalanceViewController.h
//  Netbarg
//
//  Created by Pooya on 3/6/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddBalanceViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UITextField *tfBalance;
@property (weak, nonatomic) IBOutlet UIButton *bankSaman;
@property (weak, nonatomic) IBOutlet UIButton *bankMellat;
@property (weak, nonatomic) IBOutlet UIButton *bankParsian;

- (IBAction)bankPressed:(id)sender;
- (IBAction)addBalancePressed:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButMellatWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButSamanWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButParsianWidth;
@end
