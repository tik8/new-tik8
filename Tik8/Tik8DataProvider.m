//
//  Tik8DataProvider.m
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "Tik8DataProvider.h"
#import "AppDelegate.h"
#import "HexColors.h"

@implementation Tik8DataProvider
CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(Tik8DataProvider)

@synthesize userToken = _userToken;
@synthesize userInfo = _userInfo;
@synthesize gateways = _gateways;
@synthesize cities = _cities;
@synthesize eventTypes = _eventTypes;
@synthesize eventTypesLoaded = _eventTypesLoaded;

-(NSString *)userToken{
    if(!_userToken){
        _userToken = [JNKeychain loadValueForKey:@"utkn"];
    }
    return _userToken;
}

-(void)setUserToken:(NSString *)userToken{
    _userToken = userToken;
    [JNKeychain saveValue:userToken forKey:@"utkn"];
}

-(JSUser *)userInfo{
    if(!_userInfo){
        _userInfo = [[JSUser alloc]initWithDictionary:[[NSUserDefaults standardUserDefaults]valueForKey:@"_userInfo"] error:nil];;
    }
    return _userInfo;
}

-(void)setUserInfo:(JSUser *)userInfo{
    _userInfo = userInfo;
    [[NSUserDefaults standardUserDefaults]setValue:[userInfo toDictionary] forKey:@"_userInfo"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(NSArray *)gateways{
    if(!_gateways){
        _gateways = [JSGateway arrayOfModelsFromDictionaries:[[NSUserDefaults standardUserDefaults]valueForKey:@"gtw"]];
    }
    return _gateways;
}

-(void)setGateways:(NSArray *)gateways{
    _gateways = gateways;
    [[NSUserDefaults standardUserDefaults]setValue:[JSGateway arrayOfDictionariesFromModels:gateways] forKey:@"gtw"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(NSArray *)cities{
    if(!_cities){
        _cities = [JSCity arrayOfModelsFromDictionaries:[[NSUserDefaults standardUserDefaults]valueForKey:@"_cities"]];
    }
    return _cities;
}

-(void)setCities:(NSArray *)cities{
    _cities = cities;
    [[NSUserDefaults standardUserDefaults]setValue:[JSCity arrayOfDictionariesFromModels:_cities] forKey:@"_cities"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(UIColor *)eventColor{
    if (_eventType == nil) {
        _eventType = [[JSEventType alloc] init];
    }
    if (_eventType.id == 0) {
        _eventType.color = @"#ec762f";
    }
    return [UIColor hx_colorWithHexString: self.eventType.color];
}

-(void)userSignOut{

    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"_userInfo"];
    [[NSUserDefaults standardUserDefaults]synchronize];

    [JNKeychain deleteValueForKey:@"utkn"];
    self.userInfo = nil;
    self.userToken = nil;

    AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [appDelegate updateQuickActionItems];

}

-(void)updateEventType:(int) id {
    for(int i=0; i<_eventTypes.count; i++) {
        if(((JSEventType *)_eventTypes[i]).id == id) {
            _eventType = _eventTypes[i];
            return;
        }
    }
}

-(JSEventType *)getEventType:(int) id {
    if(_eventTypes!=NULL) {
        for(int i=0; i<_eventTypes.count; i++) {
            if((_eventTypes[i]).id == id) {
                return _eventTypes[i];
            }
        }
        if(_eventTypes.count>0) {
            return _eventTypes[0];
        }
    }
    JSEventType *eventType = [[JSEventType alloc] init];
    eventType.color = @"#ec762f";
    return eventType;
}

-(void) setEventTypes: (NSMutableArray<JSEventType*>*) eventTypes {
    _eventTypes = eventTypes;
    if(_eventType==nil) {
        for(int i=0; i<eventTypes.count; i++) {
            if(eventTypes[i].status) {
                _eventType = eventTypes[i];
                break;
            }
        }
    }
}

@end
