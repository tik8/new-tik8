//
//  TicketInfoViewController.m
//  Tik8
//
//  Created by Pooya on 3/7/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "TicketInfoViewController.h"
#import "TicketInfoTableViewCell.h"
#import "TicketMainTableViewCell.h"
#import "UIImage+MDQRCode.h"
#import "DHSmartScreenshot.h"
#define kMainCellHeight 230
#define kInfoCellHeight 300

@interface TicketInfoViewController ()<UITableViewDelegate,UITableViewDataSource, UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) JSMyTicketMainInfo *ticketMainInfo;
@end

@implementation TicketInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:@"بلیط ها"
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
    
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSMyTicketInfo *info = [ServerRequest getMyTicketInfo:self.myTickInfo.id];
            dispatch_async(dispatch_get_main_queue(), ^{
                if([info.status integerValue]==1){
                    self.ticketMainInfo = info.result;
                    [self.tableView reloadData];
                }else{
                    [TSMessage showNotificationInViewController:self.parentViewController title:info.message subtitle:nil type:TSMessageNotificationTypeWarning];
                }
            });
        });
    }else{
        
    }
    // Do any additional setup after loading the view.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.ticketMainInfo) return self.ticketMainInfo.tickets.count +1;
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0)return kMainCellHeight;
    return kInfoCellHeight;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row ==0){
        TicketMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TicketMainCell" forIndexPath:indexPath];
        cell.lbName.text = self.ticketMainInfo.event_title;
        cell.lbLocation.text = [NSString stringWithFormat:@"محل اجرا: %@",self.ticketMainInfo.location_title];
        cell.lbDate.text = [NSString stringWithFormat:@"زمان اجرا: %@",self.ticketMainInfo.event_date];
        cell.lbTime.text = [NSString stringWithFormat:@"ساعت اجرا: %@",self.ticketMainInfo.event_time];
        cell.lbBuyer.text = [NSString stringWithFormat:@"نام خریدار: %@",self.ticketMainInfo.user_name];
        cell.lbPhone.text = [NSString stringWithFormat:@"تلفن: %@",self.ticketMainInfo.user_phone];
        cell.lbAddress.text = [NSString stringWithFormat:@"آدرس: %@",self.ticketMainInfo.location_address];
        cell.lbTotalPrice.text = [NSString stringWithFormat:@"قیمت کل: %@ تومان",self.ticketMainInfo.total_price];
        cell.lbFactor.text =[NSString stringWithFormat:@"شماره فاکتور: %@",self.ticketMainInfo.order_id];
        return cell;
    }else{
        TicketInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TicketInfoCell" forIndexPath:indexPath];
        JSMyTicketDetailInfo *deatilInfo = [self.ticketMainInfo.tickets objectAtIndex:indexPath.row-1];
        cell.lbTicketNumber.text = [NSString stringWithFormat:@"شماره بلیط: %@",deatilInfo.ticket_number];
        cell.lbPrice.text = [NSString stringWithFormat:@"مبلغ بلیط: %@ تومان",deatilInfo.price];
        cell.lbSalon.text = [NSString stringWithFormat:@"سالن اجرا: %@",self.ticketMainInfo.venue_name];
        cell.lbInput.text = [NSString stringWithFormat:@"ورودی: %@",deatilInfo.venue_input];
        cell.lbZone.text = [NSString stringWithFormat:@"موقعیت: %@",deatilInfo.zone_name];
        cell.lbrow.text = [NSString stringWithFormat:@"ردیف: %@",deatilInfo.row_number];
        cell.lbSeat.text = [NSString stringWithFormat:@"شماره صندلی: %@",deatilInfo.seat_number];
        cell.imgQR.image =[UIImage mdQRCodeForString:deatilInfo.ticket_hash size:300];//[self createNonInterpolatedUIImageFromCIImage:[self createQRForString:deatilInfo.ticket_hash] withScale:5.f];
        return cell;
    }
}
/*
- (CIImage *)createQRForString:(NSString *)qrString
{

    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [qrString dataUsingEncoding:NSISOLatin1StringEncoding];
    
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [qrFilter setDefaults];

    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    //[qrFilter setValue:@"H" forKey:@"inputCorrectionLevel"];
    
    // Send the image back
    return qrFilter.outputImage;
}

- (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image withScale:(CGFloat)scale
{
    // Render the CIImage into a CGImage
    CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:image fromRect:image.extent];
    
    // Now we'll rescale using CoreGraphics
    UIGraphicsBeginImageContext(CGSizeMake(image.extent.size.width * scale, image.extent.size.width * scale));
    CGContextRef context = UIGraphicsGetCurrentContext();
    // We don't want to interpolate (since we've got a pixel-correct image)
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextDrawImage(context, CGContextGetClipBoundingBox(context), cgImage);
    // Get the image out
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // Tidy up
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    return scaledImage;
}
*/

- (IBAction)actions:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"بازگشت" destructiveButtonTitle:nil otherButtonTitles:
                            @"ذخیره در گالری",nil];
    popup.tag = 1;
    [popup showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (popup.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    [self saveTicket];
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

- (void)saveTicket {
    [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];

    UIImage *image = [self.tableView screenshot];

    NSData *imageData = UIImagePNGRepresentation(image);

    UIImage* imgPng = [UIImage imageWithData:imageData]; // wrap UIImage
    UIImageWriteToSavedPhotosAlbum(imgPng,
                                   self, // send the message to 'self' when calling the callback
                                   @selector(thisImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:), // the selector to tell the method to call on completion
                                   NULL); // you generally won't need a contextInfo here

}

- (void)thisImage:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error usingContextInfo:(void*)ctxInfo {
    [SVProgressHUD dismiss];
    if (error) {
        [TSMessage showNotificationInViewController:self title:nil subtitle:@"خطا در ذخیره سازی\nلطفاً از تنظیمات دستگاه، دسترسی تیکت به تصاویر را برقرار نمایید." type:TSMessageNotificationTypeError];
    } else {
        [TSMessage showNotificationInViewController:self title:nil subtitle:@"تصویر بلیط شما در گالری تصاویر دستگاه ذخیره گردید." type:TSMessageNotificationTypeSuccess];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
