//
//  AddBalanceViewController.m
//  Netbarg
//
//  Created by Pooya on 3/6/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#define IOS_9 [[[UIDevice currentDevice] systemVersion] floatValue] >= 9

#import "MySafariViewController.h"
#import "AddBalanceViewController.h"
#import "ServerRequest.h"
#define BANK_TAG_SAMAN 0
#define BANK_TAG_MELLAT 1
#define BANK_TAG_PARSIAN 2


#define BANK_SAMAN @"saman"
#define BANK_MELLAT @"mellat"
#define BANK_PARSIAN @"parsian"

@interface AddBalanceViewController ()<UITextFieldDelegate>{
    NSString *_bankLabel;

}

@end

@implementation AddBalanceViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"افزایش موجودی حساب";

//    if(self.navigationController.viewControllers.count > 1)
//        [self setUpImageBackButton];
    self.tfBalance.delegate = self;
    self.tfBalance.keyboardType = UIKeyboardTypeNumberPad;
    
    
    UIToolbar *toolbar = [[UIToolbar alloc]init];
    [toolbar sizeToFit];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(handleActionBarDone:)];
    
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolbar setItems:[NSArray arrayWithObjects: flexible, doneButton, nil]];

    self.tfBalance.inputAccessoryView = toolbar;
    NSMutableArray *gateways = [NSMutableArray array];
    for (JSGateway *gateway in [Tik8DataProvider sharedTik8DataProvider].gateways) {
        [gateways addObject:gateway.alias];
    }
    if(![gateways containsObject:BANK_SAMAN]){
        self.bankSaman.hidden = YES;
        self.constraintButSamanWidth.constant = 0;
    }
    
    if(![gateways containsObject:BANK_PARSIAN]){
        self.bankParsian.hidden = YES;
        self.constraintButParsianWidth.constant = 0;
    }
    
    if(![gateways containsObject:BANK_MELLAT]){
        self.bankMellat.hidden = YES;
        self.constraintButMellatWidth.constant = 0;
    }
    
    if(!self.bankSaman.hidden){
        self.bankSaman.selected = YES;
        _bankLabel = BANK_SAMAN;
    }else if (!self.bankParsian.hidden){
        self.bankParsian.selected = YES;
        _bankLabel = BANK_PARSIAN;
    }else if (!self.bankMellat.hidden){
        self.bankMellat.selected = YES;
        _bankLabel = BANK_MELLAT;
    }

    
    
	// Do any additional setup after loading the view.
}


- (void)handleActionBarDone:(UIBarButtonItem *)doneButtonItem
{
    [self.tfBalance resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)bankPressed:(UIButton *)sender {
    
    if(sender.tag == BANK_TAG_SAMAN){
        self.bankSaman.selected = YES;
        self.bankMellat.selected = NO;
        self.bankParsian.selected = NO;
        _bankLabel = BANK_SAMAN;
        
        
    }else if (sender.tag == BANK_TAG_MELLAT){
        self.bankSaman.selected = NO;
        self.bankMellat.selected = YES;
        self.bankParsian.selected = NO;
        _bankLabel = BANK_MELLAT;
        
    }else{
        self.bankSaman.selected = NO;
        self.bankMellat.selected = NO;
        self.bankParsian.selected = YES;
        _bankLabel = BANK_PARSIAN;
        
    }
    

}

- (IBAction)addBalancePressed:(id)sender {
    [self.tfBalance resignFirstResponder];
    if([self.tfBalance.text longLongValue] >= 1000){
        if([[MyReachability reachabilityForInternetConnection] isReachable]){
            [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
            JSGateway *gate;
            for (JSGateway *gateway in [Tik8DataProvider sharedTik8DataProvider].gateways) {
                if([gateway.alias isEqualToString:_bankLabel]){
                    gate = gateway;
                    break;
                }
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                JSPaymentResponse *response = [ServerRequest addToWallet:self.tfBalance.text gateway:gate.id];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];

                    if([response.status integerValue] == 1){
                        // to gateway!
                        NSURL *URL = [NSURL URLWithString:response.payUrl];
                        if (IOS_9) {
                            MySafariViewController *sfvc = [[MySafariViewController alloc] initWithURL:URL];
                            [self presentViewController:sfvc animated:YES completion:nil];
                        } else {
                            [[UIApplication sharedApplication] openURL:URL];
                        }
                    }else{
                        [TSMessage showNotificationInViewController:self.parentViewController title:@"خطا" subtitle:nil type:TSMessageNotificationTypeError];
                    }

                });
            });
        }else{
            [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];

        }

    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"مبلغ را درست وارد کنید" subtitle:nil type:TSMessageNotificationTypeWarning];

    }
}

@end
