//
//  YekanButton.m
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "YekanButton.h"

@implementation YekanButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)awakeFromNib{
    [super awakeFromNib];
    [self.titleLabel  setFont:[UIFont fontWithName:FontName size:self.titleLabel.font.pointSize]];
}
@end
