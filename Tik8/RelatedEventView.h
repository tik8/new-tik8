//
//  RelatedEventView.h
//  Tik8
//
//  Created by Pooya on 1/31/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RelatedEventView : UIView
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
