//
//  VenueViewController.m
//  Tik8
//
//  Created by Sina on 10/28/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "TicketSeatsTableViewController.h"
#import "SeatViewController.h"
#import "TicketCategoryTableViewCell.h"
#import "BuyViewController.h"
#import "JSIntervalPriceCats.h"

@interface TicketSeatsTableViewController ()
@end

@implementation TicketSeatsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _labels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TicketCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"seatsNumCell" forIndexPath:indexPath];

    ((YekanLabel *) [cell.contentView viewWithTag:10]).text = _labels[indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [_parentVC labelSelected: (int) indexPath.row];

}


#pragma mark - Navigation

@end
