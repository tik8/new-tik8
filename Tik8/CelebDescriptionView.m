//
//  CelebDescriptionView.m
//  Tik8
//
//  Created by Pooya on 2/2/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "CelebDescriptionView.h"

@implementation CelebDescriptionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"CelebDescriptionView" owner:self options:nil];
        self = [nib objectAtIndex:0];
        self.frame = frame;
        [self.lbBiography setFont:[UIFont fontWithName:FontName size:self.lbBiography.font.pointSize]];
    }
    return self;
}
@end
