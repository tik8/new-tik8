//
//  SeeMoreTicketViewController.h
//  Tik8
//
//  Created by Pooya on 2/18/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SeeMoreTicketViewController : UIViewController
@property (nonatomic,strong) NSString *eventTypeId;
@property (nonatomic,strong) JSEvent *eventDetail;
@property (weak, nonatomic) IBOutlet YekanLabel *lblTitle;
@property (weak, nonatomic) IBOutlet YekanLabel *lblAddress;
@end
