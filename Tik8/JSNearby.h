//
//  JSNearby.h
//  Tik8
//
//  Created by Pooya on 2/14/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSEvent.h"
@interface JSNearby : JSStatus

@property (nonatomic,strong) NSString *city_id;
@property (nonatomic,strong) NSString *count;
@property (nonatomic,strong) NSArray <JSEvent>*result;

@end
