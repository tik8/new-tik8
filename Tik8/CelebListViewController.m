//
//  CelebListViewController.m
//  Tik8
//
//  Created by Pooya on 5/20/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "CelebListViewController.h"
#import "CelebTableViewCell.h"
#import "ArtistLocationDetailViewController.h"
@interface CelebListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CelebListViewController

- (void)viewWillAppear:(BOOL)animated {
    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_eventTypeId intValue]]];
    [self setNavbarAttr];
}
-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.celebList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CelebTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CelebCell" forIndexPath:indexPath];
    
    JSCelebrity *celeb = [self.celebList objectAtIndex:indexPath.row];
    
    [cell.imgCeleb sd_setImageWithURL:[NSURL URLWithString:celeb.profile_image] placeholderImage:[UIImage imageNamed:@"place-holder_user"]];
    cell.lbName.text = [NSString stringWithFormat:@"%@ %@",celeb.firstname,celeb.lastname];
    cell.lbPosition.text = celeb.position;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    JSCelebrity *celeb = [self.celebList objectAtIndex:indexPath.row];
    if([celeb.status boolValue]){
        [self performSegueWithIdentifier:@"go to celeb detail" sender:celeb];
    }else{
        [TSMessage showNotificationInViewController:self title:nil subtitle:@"پروفایل هنرمند موجود نمی‌باشد" type:TSMessageNotificationTypeWarning];        
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"go to celeb detail"])
    {
        [segue.destinationViewController setEventTypeId: _eventTypeId];
        [segue.destinationViewController setCelebInfo:sender];
    }
}


@end
