//
//  JSPosition.h
//  Tik8
//
//  Created by Pooya on 2/9/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JSEvent.h"
#import "JSLocation.h"

@protocol JSPosition 

@end

@interface JSPosition : JSONModel

@property (nonatomic,strong) JSLocation * location;
@property (nonatomic,strong) NSString *rotation;
@property (nonatomic,strong) JSEvent * event;

@end
