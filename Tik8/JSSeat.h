//
//  JSSeat.h
//  Tik8
//
//  Created by Pooya on 5/4/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@protocol JSSeat
@end

@interface JSSeat : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *row_number;
@property (nonatomic,strong) NSString *snumber;
@property (nonatomic,strong) NSString *zone;

@end
