//
//  CelebListViewController.h
//  Tik8
//
//  Created by Pooya on 5/20/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CelebListViewController : UIViewController

@property (nonatomic,strong) NSString *eventTypeId;
@property (nonatomic,strong) NSArray *celebList;

@end
