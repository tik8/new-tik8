//
//  MenuTableViewCell.h
//  Tik8
//
//  Created by Pooya on 2/2/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgIcon;
@property (strong, nonatomic) IBOutlet YekanLabel *lbTitle;

@end
