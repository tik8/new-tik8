//
//  JSCityDetail.h
//  Tik8
//
//  Created by Pooya on 3/14/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSCity.h"

@interface JSCityDetail : JSStatus

@property (nonatomic,strong) NSArray <JSCity>*result;

@end
