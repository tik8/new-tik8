//
//  JSTicketList.h
//  Tik8
//
//  Created by Pooya on 1/24/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSStatus.h"
#import "JSEvent.h"
@interface JSEventList : JSStatus

@property (nonatomic,strong) NSString *limit;
@property (nonatomic,strong) NSString *page;
@property (nonatomic,strong) NSString *count;
@property (nonatomic,strong) NSArray <JSEvent>*result;

@end
