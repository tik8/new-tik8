//
//  VenueViewController.h
//  Tik8
//
//  Created by Sina on 10/28/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketCatsViewController.h"

@interface TicketSeatsTableViewController : UITableViewController

@property (nonatomic,strong) NSMutableArray<NSString*> *labels;
@property (nonatomic,strong) TicketCatsViewController *parentVC;

@end
