//
//  JSCity.h
//  Tik8
//
//  Created by Pooya on 3/14/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"

@protocol JSCity
@end

@interface JSCity : JSONModel

@property (nonatomic,strong) NSString *id;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *alias;

@end
