//
//  Utils.m
//  Tik8
//
//  Created by Sina KH on 7/17/16.
//  Copyright © 2016 Pooya. All rights reserved.
//

#import "Utils.h"
#define IOS_7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7

@implementation Utils

+(NSString*) persianDigits: (NSString*) string {
    string = [string stringByReplacingOccurrencesOfString:@"0" withString:@"۰"];
    string = [string stringByReplacingOccurrencesOfString:@"1" withString:@"۱"];
    string = [string stringByReplacingOccurrencesOfString:@"2" withString:@"۲"];
    string = [string stringByReplacingOccurrencesOfString:@"3" withString:@"۳"];
    string = [string stringByReplacingOccurrencesOfString:@"4" withString:@"۴"];
    string = [string stringByReplacingOccurrencesOfString:@"5" withString:@"۵"];
    string = [string stringByReplacingOccurrencesOfString:@"6" withString:@"۶"];
    string = [string stringByReplacingOccurrencesOfString:@"7" withString:@"۷"];
    string = [string stringByReplacingOccurrencesOfString:@"8" withString:@"۸"];
    string = [string stringByReplacingOccurrencesOfString:@"9" withString:@"۹"];
    return string;
}


+(NSString *)formatedNumber:(double)value{
    NSNumber *number = [NSNumber numberWithDouble:value];
    
    NSLocale *IRLocal = [[NSLocale alloc] initWithLocaleIdentifier:@"fa_IR"];
    NSNumberFormatter *numberFormatterToPersian = [[NSNumberFormatter alloc]init];
    [numberFormatterToPersian setLocale:IRLocal];
    [numberFormatterToPersian setCurrencyGroupingSeparator:@","];
    [numberFormatterToPersian setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatterToPersian setCurrencySymbol:@" "];
    
    
    NSString *numberString = [numberFormatterToPersian stringFromNumber:number];
    
    //    return [_original_price substringToIndex:_original_price.length-1];
    if(IOS_7)
        numberString = [ numberString substringFromIndex:2];
    else
        numberString = [ numberString substringToIndex:(numberString.length - 2)];
    
    return numberString;
}

@end
