//
//  EventsMapViewController.m
//  Tik8
//
//  Created by Pooya on 2/9/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "EventsMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "EventDetailViewController.h"

#define kMarkerSize 36

@interface EventsMapViewController ()<GMSMapViewDelegate>
@property (nonatomic,strong) GMSMapView *mapView;
@property (nonatomic,strong) NSArray *positions;
@end

@implementation EventsMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavbarAttr];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:35.689385
                                                            longitude:51.403956
                                                                 zoom:8];
    self.mapView = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.delegate = self;
    
    self.mapView.settings.myLocationButton = YES;
    
    
    [self.view addSubview:self.mapView ];
    
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSPositions *positions = [ServerRequest getAllPositions];
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                if([positions.status integerValue] == 1){
                    self.positions = positions.result;
                    [self checkForSamePositions];
                }
                [self loadMarkers];
            });
        });
    }else{
        
    }
    
    // Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated{
    [SVProgressHUD dismiss];
    
    [super viewWillDisappear:animated];
}

-(void)checkForSamePositions {
    for(int i=0; i<_positions.count; i++) {
        JSPosition *pos_i = _positions[i];
        for(int j=0; j<i; j++) {
            JSPosition *pos_j = _positions[j];
            if([pos_i.location.latitude doubleValue] == [pos_j.location.latitude doubleValue]
               && [pos_i.location.longitude doubleValue] == [pos_j.location.longitude doubleValue]) {
                pos_i.rotation = [NSString stringWithFormat:@"%f", [pos_i.rotation doubleValue] + 60];
            }
        }
    }
}
-(void)loadMarkers{
    for (JSPosition *pos in self.positions) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake([pos.location.latitude doubleValue], [pos.location.longitude doubleValue]);
        marker.title =pos.event.title;
        marker.snippet = pos.location.title;
        UIImageView *img = [[UIImageView alloc] init];
        [img sd_setImageWithURL:[NSURL URLWithString:[[Tik8DataProvider sharedTik8DataProvider] getEventType:[pos.event.type intValue]].marker_url] placeholderImage:[UIImage imageNamed:@"marker_default"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            marker.icon = [self imageWithImage:img.image scaledToSize:CGSizeMake(kMarkerSize, img.image.size.height/img.image.size.width*kMarkerSize)];
        }];
        marker.icon = [self imageWithImage:img.image scaledToSize:CGSizeMake(kMarkerSize, img.image.size.height/img.image.size.width*kMarkerSize)];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.rotation = [pos.rotation doubleValue];
        
        marker.map = self.mapView;
        marker.userData = pos;
    }
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    JSPosition *pos = marker.userData;
    pos.event.location_title = pos.location.title;
    
    [[Tik8DataProvider sharedTik8DataProvider] updateEventType:[pos.event.type intValue]];
    [self setNavbarAttr];
    
    [self performSegueWithIdentifier:@"go to detail" sender:pos.event];
    
    
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"go to detail"]) {
        [segue.destinationViewController setEvent:sender];
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

@end
