//
//  NoCurserTextField.m
//  WhoSolves
//
//  Created by Pooya on 12/29/13.
//  Copyright (c) 2013 HyperOffice. All rights reserved.
//

#import "NoCurserTextField.h"

@implementation NoCurserTextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    return CGRectZero;    // ensures the cursor won't appear
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
