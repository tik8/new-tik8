//
//  EventLocationTableViewCell.h
//  Tik8
//
//  Created by Pooya on 1/26/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircelView.h"
#import <MapKit/MapKit.h>
@interface EventLocationTableViewCell : UITableViewCell<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *butMap;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet CircelView *viewCircel;
@property (weak, nonatomic) IBOutlet YekanLabel *lbAddress;
@property (weak, nonatomic) IBOutlet YekanButton *butSeeLocation;
@property (strong, nonatomic) IBOutlet UIImageView *imgBullet;

@property (nonatomic,strong) JSLocation *locationInfo;
@end
