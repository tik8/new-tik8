//
//  VenueViewController.m
//  Tik8
//
//  Created by Sina on 10/28/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "TicketCatsViewController.h"
#import "SeatViewController.h"
#import "TicketCategoryTableViewCell.h"
#import "BuyViewController.h"
#import "JSIntervalPriceCats.h"
#import "TicketSeatsTableViewController.h"
#define IOS_7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7

#define kFooterHeight 228

@interface TicketCatsViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic,strong) JSReserve *seats;
@property (strong, nonatomic) NSMutableArray<NSDictionary*> *cats;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) JSIntervalPriceCats *priceCats;
@property (nonatomic) NSIndexPath* selectedIndexPath;
@property NSMutableArray<TicketSeatsTableViewController*> *tstvcs;

- (void) updateUserInfo;
@end

@implementation TicketCatsViewController

- (void)viewWillAppear:(BOOL)animated {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"خرید رویداد-  %@: %@, سانس: %@",self.eventInfo.id, self.eventInfo.title, self.interval.id]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    [[Tik8DataProvider sharedTik8DataProvider] setEventType:[[Tik8DataProvider sharedTik8DataProvider] getEventType: [_eventInfo.type_id intValue]]];
    [self setNavbarAttr];
}
-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    _tableView.separatorColor = [UIColor clearColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self checkSeats];
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        [SVProgressHUD showWithStatus:@"لطفاً کمی صبر کنید"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSIntervalPriceCatsResponse *jsResponse = [ServerRequest getIntervalPriceCategory:_interval.id];
            if([jsResponse.status integerValue] == 1) {
                _priceCats = jsResponse.result;
                _tstvcs = [[NSMutableArray alloc] init];
                for (int i=0; i<_priceCats.PriceCategories.count; i++) {
                    [_tstvcs addObject: [[TicketSeatsTableViewController alloc] init]];
                }
                _labels = [[NSMutableArray alloc] init];
                [_labels addObject:@"انتخاب کنید"];
                for(int i=[_priceCats.Interval.min_quantity_per_user intValue]; i <= [_priceCats.Interval.max_quantity_per_user intValue]; i++) {
                    [_labels addObject:[NSString stringWithFormat:@"%d صندلی", i]];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [_tableView reloadData];
                    [self updateUserInfo];
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_tableView reloadData];
                    [TSMessage showNotificationInViewController:self.parentViewController title:@"خطا" subtitle:nil type:TSMessageNotificationTypeError];
                });
            }
        });
    }else{
        [TSMessage showNotificationInViewController:self.parentViewController title:@"اینترنت در دسترس نمی باشد" subtitle:nil type:TSMessageNotificationTypeWarning];
    }
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.priceCats.PriceCategories.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TicketCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ticketCategoryCell" forIndexPath:indexPath];

    JSPriceCategory *jsPriceCategory = _priceCats.PriceCategories[indexPath.row];
    JSPriceCategoryInner *jsPriceCategoryInner = jsPriceCategory.PriceCategory;
    cell.label.text = [NSString stringWithFormat:@"%@ (%@ تومان)", jsPriceCategoryInner.name, jsPriceCategory.price];
    if(jsPriceCategory.num==0) {
        cell.detail.text = @"انتخاب کنید";
    } else {
        cell.detail.text = [NSString stringWithFormat:@"%d صندلی", jsPriceCategory.num];
    }
    [cell.arrow setTextColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];

    TicketSeatsTableViewController *tstvc = _tstvcs[indexPath.row];
    cell.seatsTable.delegate = tstvc;
    cell.seatsTable.dataSource = tstvc;
    cell.seatsTable.scrollEnabled = false;
    tstvc.parentVC = self;
    tstvc.labels = _labels;
    [cell.seatsTable reloadData];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if(_selectedIndexPath!=indexPath) {
        _selectedIndexPath = indexPath;
    } else {
        _selectedIndexPath = nil;
    }
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(_selectedIndexPath!=nil && indexPath.row == _selectedIndexPath.row) {
        return ([_priceCats.Interval.max_quantity_per_user intValue] - [_priceCats.Interval.min_quantity_per_user intValue] + 2)*48 + 64;
    }
    return 64;
}

-(void) labelSelected: (int) row {

    int tempSum = 0;
    int tempNum = ((JSPriceCategory *) _priceCats.PriceCategories[_selectedIndexPath.row]).num;
    ((JSPriceCategory *) _priceCats.PriceCategories[_selectedIndexPath.row]).num = row;
    for (int i=0; i< _priceCats.PriceCategories.count; i++) {
        tempSum += ((JSPriceCategory *)  _priceCats.PriceCategories[i]).num;
    }

    if (tempSum <= [_priceCats.Interval.max_quantity_per_user intValue]) {
        [self checkSeats];
    } else {
        ((JSPriceCategory *) _priceCats.PriceCategories[_selectedIndexPath.row]).num = tempNum;
        if ([_priceCats.Interval.max_quantity_per_user intValue]>0) {
            [TSMessage showNotificationInViewController:self.parentViewController title:[NSString stringWithFormat:@"شما مجاز به خرید %d بلیط از این رویداد می باشید.", [_priceCats.Interval.max_quantity_per_user intValue]] subtitle:nil type:TSMessageNotificationTypeWarning];
        } else {
            [TSMessage showNotificationInViewController:self.parentViewController title:@"شما مجاز به خرید بلیط بیشتری از این رویداد نیستید." subtitle:nil type:TSMessageNotificationTypeWarning];
        }
    }
    _selectedIndexPath = nil;

    [_tableView reloadData];
}

-(void) checkSeats {

    self.seats = [[JSReserve alloc] init];
    self.seats.count = @"0";
    self.seats.total_price = @"0";
    NSMutableArray *seats = [[NSMutableArray<JSSeat> alloc] init];
    
    _cats = [[NSMutableArray alloc] init];
    for(int i=0; i<_priceCats.PriceCategories.count; i++) {
        if(((JSPriceCategory*) _priceCats.PriceCategories[i]).num>0) {
            NSMutableDictionary *cat = [[NSMutableDictionary alloc] initWithCapacity:2];
            JSPriceCategory *jSPriceCategory = (JSPriceCategory*) _priceCats.PriceCategories[i];
            [cat setValue:jSPriceCategory.id forKey:@"id"];
            [cat setValue:[NSString stringWithFormat:@"%d", jSPriceCategory.num] forKey:@"num"];
            [_cats addObject:cat];

            self.seats.count = [NSString stringWithFormat:@"%d", [self.seats.count intValue]+jSPriceCategory.num];
            self.seats.total_price = [NSString stringWithFormat:@"%d", [self.seats.total_price intValue]+[jSPriceCategory.price intValue]*jSPriceCategory.num];
            
            JSSeat *seat = [[JSSeat alloc] init];
            seat.id = jSPriceCategory.id;
            seat.snumber = [NSString stringWithFormat:@"%d", jSPriceCategory.num];
            [seats addObject:seat];
        }
    }
    self.seats.seats = [seats copy];

    if(self.seats.seats.count>0) {
        _lbNum.text = [NSString stringWithFormat:@"%@ عدد",self.seats.count];
        _lbBalance.text = [NSString stringWithFormat:@"%@ تومان",[self formatedNumber:[[Tik8DataProvider sharedTik8DataProvider].userInfo.cashBalance doubleValue]]];
        _lbTotalPrice.text = [NSString stringWithFormat:@"%@ تومان",[self formatedNumber: [self.seats.total_price intValue]]];
        _lbBalance.text = [NSString stringWithFormat:@"%@ تومان",[self formatedNumber:[[Tik8DataProvider sharedTik8DataProvider].userInfo.cashBalance doubleValue]]];
        _lbTotalPay.text = [NSString stringWithFormat:@"%@ تومان",[self calculateTotalPriceToPay]];
        _footerHeight.constant = kFooterHeight;
        _footerView.hidden = false;
    } else {
        _footerHeight.constant = 0;
        _footerView.hidden = true;
    }
}

-(NSString *)calculateTotalPriceToPay{
    long userBalance = [[Tik8DataProvider sharedTik8DataProvider].userInfo.cashBalance doubleValue];
    long totalFee = [[_seats.total_price stringByReplacingOccurrencesOfString:@"," withString:@""] doubleValue];
    long pay = totalFee-userBalance;
    if(pay < 0){
        pay = 0;
    }
    return  [self formatedNumber:pay];
}

-(NSString *)formatedNumber:(double)value{
    NSNumber *number = [NSNumber numberWithDouble:value];
    
    NSLocale *IRLocal = [[NSLocale alloc] initWithLocaleIdentifier:@"fa_IR"];
    NSNumberFormatter *numberFormatterToPersian = [[NSNumberFormatter alloc]init];
    [numberFormatterToPersian setLocale:IRLocal];
    [numberFormatterToPersian setCurrencyGroupingSeparator:@","];
    [numberFormatterToPersian setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatterToPersian setCurrencySymbol:@" "];
    
    
    NSString *numberString = [numberFormatterToPersian stringFromNumber:number];
    
    //    return [_original_price substringToIndex:_original_price.length-1];
    if(IOS_7)
        numberString = [ numberString substringFromIndex:2];
    else
        numberString = [ numberString substringToIndex:(numberString.length - 2)];
    
    return numberString;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"go to buy"]) {
        BuyViewController *buyVc = segue.destinationViewController;
        buyVc.interval = self.interval;
        buyVc.eventInfo = self.eventInfo;
        buyVc.reserve = self.seats;
        buyVc.cats = self.cats;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


- (IBAction)buyPressed:(NSObject *)sender {

    if(self.seats.seats.count > 0){
        [self performSegueWithIdentifier:@"go to buy" sender:self];
    }else{
        [TSMessage showNotificationInViewController:self title:@"شما بلیطی برای خرید انتخاب نکرده اید" subtitle:nil type:TSMessageNotificationTypeWarning];
    }
}

- (void) updateUserInfo {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [ServerRequest updateUserInfo];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self checkSeats];
        });
    });
}

@end
