//
//  JSMyTicketsAndDate.h
//  Tik8
//
//  Created by Pooya on 2/25/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "JSONModel.h"
#import "JSMyTicket.h"

@protocol JSMyTicketsAndDate
@end

@interface JSMyTicketsAndDate : JSONModel

@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSArray <JSMyTicket>*items;

@end
