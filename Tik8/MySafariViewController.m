//
//  MySafariViewController.m
//  Tik8
//
//  Created by Sina KH on 9/24/16.
//  Copyright © 2016 Pooya. All rights reserved.
//

#import "MySafariViewController.h"

@interface MySafariViewController ()

@end

@implementation MySafariViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleLightContent];
}

@end
