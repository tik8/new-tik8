//
//  AddBalanceViewController.h
//  Netbarg
//
//  Created by Pooya on 3/6/14.
//  Copyright (c) 2014 Netbarg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UseGiftViewController : GAITrackedViewController
@property (weak, nonatomic) IBOutlet UITextField *tfGift;

- (IBAction)addBalancePressed:(id)sender;
@end
