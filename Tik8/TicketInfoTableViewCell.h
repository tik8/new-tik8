//
//  TicketInfoTableViewCell.h
//  Tik8
//
//  Created by Pooya on 3/7/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketInfoTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet YekanLabel *lbTicketNumber;
@property (strong, nonatomic) IBOutlet YekanLabel *lbPrice;
@property (strong, nonatomic) IBOutlet YekanLabel *lbSalon;
@property (strong, nonatomic) IBOutlet YekanLabel *lbInput;
@property (strong, nonatomic) IBOutlet YekanLabel *lbZone;
@property (strong, nonatomic) IBOutlet YekanLabel *lbrow;
@property (strong, nonatomic) IBOutlet YekanLabel *lbSeat;
@property (strong, nonatomic) IBOutlet UIImageView *imgQR;

@end
