//
//  EventLocationsTableViewCell.h
//  Tik8
//
//  Created by Sina KH on 7/19/16.
//  Copyright © 2016 Pooya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDetailViewController.h"

@interface EventLocationsTableViewCell : UITableViewCell <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *imgBullet;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray <JSLocation> *locations;

@property (nonatomic, strong) EventDetailViewController *delegate;

@end
