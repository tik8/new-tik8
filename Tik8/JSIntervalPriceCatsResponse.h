//
//  JSIntervalPriceCats.h
//  Tik8
//
//  Created by Sina on 10/29/15.
//  Copyright (c) 2015 Sina. All rights reserved.
//

#import "JSONModel.h"
#import "JSIntervalCat.h"
#import "JSIntervalPriceCats.h"

@interface JSIntervalPriceCatsResponse : JSStatus

@property (nonatomic,strong) JSIntervalPriceCats *result;

@end
