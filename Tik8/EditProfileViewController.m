//
//  EditProfileViewController.m
//  Tik8
//
//  Created by Pooya on 2/14/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

- (IBAction)changedPressed;
@property (strong, nonatomic) IBOutlet UITextField *txName;
@property (strong, nonatomic) IBOutlet UITextField *txLastName;
@property (strong, nonatomic) IBOutlet UITextField *txPhone;
@property (strong, nonatomic) IBOutlet YekanButton *butChange;
@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txName.text = [Tik8DataProvider sharedTik8DataProvider].userInfo.firstname;
    self.txLastName.text = [Tik8DataProvider sharedTik8DataProvider].userInfo.lastname;
    self.txPhone.text = [Tik8DataProvider sharedTik8DataProvider].userInfo.phone;
    self.butChange.layer.cornerRadius = 4.f;
}

- (IBAction)changedPressed {
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        [SVProgressHUD showWithStatus:@"در حال ارسال"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            JSStatus *status = [ServerRequest editProfileWithName:self.txName.text lastname:self.txLastName.text phone:self.txPhone.text];
            if([status.status integerValue]==1){
                [ServerRequest updateUserInfo];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [TSMessage showNotificationInViewController:self.parentViewController title:status.message subtitle:nil type:TSMessageNotificationTypeSuccess];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    [TSMessage showNotificationInViewController:self.parentViewController title:status.message subtitle:nil type:TSMessageNotificationTypeError];
                });
                
            }
            
            
            
        });
    }else{
        
    }
}
@end
