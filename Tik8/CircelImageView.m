//
//  CircelImageView.m
//  Tik8
//
//  Created by Pooya on 1/31/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "CircelImageView.h"

@implementation CircelImageView

// Updated for iOS 10 Compatibility
-(void) layoutSubviews {
    [super layoutSubviews];
    [self setup];
}
-(void) setup {
    self.layer.cornerRadius = [self bounds].size.width/2;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 1.5f;
    self.clipsToBounds = YES;
}

@end
