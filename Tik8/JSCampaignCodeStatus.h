//
//  JSCampaignCodeStatus.h
//  Tik8
//
//  Created by Sina KH on 7/17/16.
//  Copyright © 2016 Pooya. All rights reserved.
//

#import "JSONModel.h"

@interface JSCampaignCodeStatus : JSStatus

@property (nonatomic,strong) NSString *discount_value;
@property (nonatomic,strong) NSString *discount_percent;

@end
