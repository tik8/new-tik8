//
//  MapViewController.m
//  Tik8
//
//  Created by Pooya on 2/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>

@interface MapViewController ()<GMSMapViewDelegate,UIActionSheetDelegate>
@property (nonatomic,strong) GMSMapView *mapView;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:35.689385
                                                            longitude:51.403956
                                                                 zoom:11];
    self.mapView = [GMSMapView mapWithFrame:self.view.frame camera:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.delegate = self;
    
    self.mapView.settings.myLocationButton = YES;
    
    
    [self.view addSubview:self.mapView ];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([self.locationInfo.latitude doubleValue], [self.locationInfo.longitude doubleValue]);
    marker.title =self.locationInfo.title;
    marker.snippet = self.locationInfo.address;
    //marker.icon = [UIImage imageNamed:@"map_marker_theatre"];
    marker.map = self.mapView;
    
    [self.mapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:[self.locationInfo.latitude doubleValue]
                                                                     longitude:[self.locationInfo.longitude doubleValue]
                                                                          zoom:14]];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)openMapPressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Choose:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Google Map",@"Apple Map", nil];
    [actionSheet showInView:self.navigationController.view];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(buttonIndex == 0){
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%f,%f&daddr=%@,%@&directionsmode=driving",self.mapView.myLocation.coordinate.latitude,self.mapView.myLocation.coordinate.longitude,self.locationInfo.latitude,self.locationInfo.longitude ]]];
    }else if(buttonIndex ==1){
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([self.locationInfo.latitude doubleValue], [self.locationInfo.longitude doubleValue]);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:self.locationInfo.title];
        
        // Set the directions mode to "Walking"
        // Can use MKLaunchOptionsDirectionsModeDriving instead
        NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
        // Get the "Current User Location" MKMapItem
        MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
        // Pass the current location and destination map items to the Maps app
        // Set the direction mode in the launchOptions dictionary
        [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                       launchOptions:launchOptions];
    }
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
