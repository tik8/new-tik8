//
//  HomeViewController.m
//  Tik8
//
//  Created by Pooya on 1/21/15.
//  Copyright (c) 2015 Pooya. All rights reserved.
//

#import "HomeViewController.h"
#import "JASidePanelController.h"
#import "JSEvent.h"
#import "EventTypeCollectionViewCell.h"
#import "JSEvent.h"

@interface HomeViewController() <UICollectionViewDelegate,UICollectionViewDataSource>
- (IBAction)menuButPressed;
- (void) loadEventTypes;
- (void) showEventTypes: (NSMutableArray<JSEventType *>*) allEventTypes;
@property (strong, nonatomic) IBOutlet UIImageView *imgBG;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic,strong) NSMutableArray * eventTypes;

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"صفحه‌ی اصلی";
    [UIView animateWithDuration:5.0 delay:0 options:(UIViewAnimationOptionRepeat|UIViewAnimationOptionAutoreverse) animations:^{
        CGRect rect = self.imgBG.frame;
        rect.origin.x += 15;
        self.imgBG.frame = rect;
    } completion:^(BOOL finished) {
        
    }];

    _collectionView.delegate    = self;
    _collectionView.dataSource  = self;
    // rotation
    _collectionView.transform = CGAffineTransformMakeScale(-1, 1);

    // event types to show will be stored here
    _eventTypes = [[NSMutableArray alloc] init];

    // show all cached event types
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"eventTypes"];
    NSMutableArray<JSEventType *>* allEventTypes = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    if(allEventTypes.count>0) {
        [self showEventTypes:allEventTypes];
    }

    [self loadEventTypes];

    [self setNavbarAttr];

}

-(void) loadEventTypes {
    if([[MyReachability reachabilityForInternetConnection] isReachable]){
        if(_eventTypes.count==0) {
            [SVProgressHUD showWithStatus:@"در حال بارگذاری رویداد ها"];
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

            // get and update event types
            if([ServerRequest checkEventTypes]) {
                [self showEventTypes: [Tik8DataProvider sharedTik8DataProvider].eventTypes];
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    if(_eventTypes.count==0) {
                        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
                            [TSMessage showNotificationInViewController:self title:nil subtitle:@"لطفاً اتصال دستگاه به اینترنت را بررسی نمایید." type:TSMessageNotificationTypeWarning];
                            [self performSelector:@selector(loadEventTypes) withObject:self afterDelay:5.0];
                        } else {
                            [self showErrorDialog];
                        }
                    }
                });
            }

        });
    }else{
        dispatch_async(dispatch_get_main_queue(), ^{
            if(_eventTypes.count==0) {
                if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
                    [TSMessage showNotificationInViewController:self title:nil subtitle:@"اتصال دستگاه به اینترنت را بررسی نمایید." type:TSMessageNotificationTypeWarning];
                    [self performSelector:@selector(loadEventTypes) withObject:self afterDelay:5.0];
                } else {
                    [self showErrorDialog];
                }
            }
        });
    }
}

-(void) showEventTypes: (NSMutableArray<JSEventType *>*) allEventTypes {
    _eventTypes = [[NSMutableArray alloc] init];
    for(int i=0; i<allEventTypes.count; i++) {
        if(allEventTypes[i].status) {
            [_eventTypes addObject:allEventTypes[i]];
        }
    }
    if(_eventTypes.count%3>0) {
        int mod = _eventTypes.count%3;
        for(int i=0; i<3-mod; i++) {
            [_eventTypes addObject:[[JSEventType alloc] init]];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        [_collectionView reloadData];
    });
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[UINavigationBar appearance] setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self setNavbarAttr];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)setNavbarAttr{
    [self.navigationController.navigationBar setBarTintColor:[Tik8DataProvider sharedTik8DataProvider].eventColor];
}

- (IBAction)menuButPressed {
    JASidePanelController *panel = (JASidePanelController *)self.parentViewController.parentViewController;
    [panel showRightPanelAnimated:YES];
}


#pragma mark - CollectionView datasource delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _eventTypes.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
        EventTypeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"item" forIndexPath:indexPath];

    JSEventType *jsEventType = _eventTypes[indexPath.row];

    cell.text.text = jsEventType.name;
    if([jsEventType.name length]>0) {
        [cell.image sd_setImageWithURL:[NSURL URLWithString:jsEventType.image_url] placeholderImage:[UIImage imageNamed:@"type_default"]];
    } else {
        [cell.image sd_setImageWithURL:[NSURL URLWithString:jsEventType.image_url] placeholderImage:[UIImage imageNamed:@"nothing"]];
    }

    cell.transform = CGAffineTransformMakeScale(-1, 1);

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    JSEventType *jsEventType = (JSEventType *)_eventTypes[indexPath.row];

    if (jsEventType.id>0) {
        [Tik8DataProvider sharedTik8DataProvider].eventType = jsEventType;
        [self performSegueWithIdentifier:@"go to events" sender:nil];
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];

        reusableview = headerView;
    }

    // rotatation
    reusableview.transform = CGAffineTransformMakeScale(-1, 1);

    return reusableview;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    int width = 0;
    int screenWidth = self.collectionView.frame.size.width;
    if(screenWidth%3==1 && indexPath.row%3==1) {
        width = screenWidth/3 +1;
    }
    else if(screenWidth%3==2 && indexPath.row%3!=1) {
        width = screenWidth/3 +1;
    } else {
        width = screenWidth/3;
    }
    return CGSizeMake(width, 135);
}

-(void) showErrorDialog {
    SCLALertViewButtonBuilder *retryButton = [SCLALertViewButtonBuilder new].title(@"تلاش مجدد")
    .actionBlock(^{
        [self loadEventTypes];
    });
    
    SCLAlertViewBuilder *builder = [SCLAlertViewBuilder new]
    .addButtonWithBuilder(retryButton)
    .shouldDismissOnTapOutside(NO);
    
    SCLAlertViewShowBuilder *showBuilder = [SCLAlertViewShowBuilder new]
    .style(Error)
    .title(@"خطا در اطلاعات دریافتی")
    .subTitle(@"لطفاً اتصال دستگاه به اینترنت را بررسی نمایید.")
    .duration(0);
    
    SCLAlertView* alertView = builder.alertView;
    alertView.customViewColor = [Tik8DataProvider sharedTik8DataProvider].eventColor;
    [alertView setTitleFontFamily:FontName withSize:14];
    [alertView setBodyTextFontFamily:FontName withSize:12];
    [alertView setButtonsTextFontFamily:FontName withSize:14];
    // change buttons' font
    UIView* subview = alertView.view.subviews[0].subviews.lastObject;
    if ([subview isKindOfClass:UIButton.class]) {
        UIButton* btn = (UIButton* ) subview;
        [btn.titleLabel setFont: [UIFont fontWithName:FontName size:btn.titleLabel.font.pointSize]];
    }
    
    [showBuilder showAlertView:alertView onViewController:self];
}

@end
